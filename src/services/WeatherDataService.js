import firebase from "../firebase";

import { getDatabase, ref, set } from "firebase/database";

class WeatherDataService {
  writeUserData(weather) {
    const db = getDatabase();
    set(ref(db,new Date()+":"), {
     locationName: weather.name,
   locationCountry:weather.sys.country ,
     temperature:Math.round(weather.main.temp)+"°c",
     weather: weather.weather[0].main 


    }).then((reponse) => {
     alert("Saved in firebase")
    }).catch((error) => {
      alert("Error occured")
    });
  }
}

export default new WeatherDataService();