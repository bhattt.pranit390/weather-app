webpackJsonp([0],{"+E39":function(e,t,n){e.exports=!n("S82l")(function(){return 7!=Object.defineProperty({},"a",{get:function(){return 7}}).a})},"+ZMJ":function(e,t,n){var r=n("lOnJ");e.exports=function(e,t,n){if(r(e),void 0===t)return e;switch(n){case 1:return function(n){return e.call(t,n)};case 2:return function(n,r){return e.call(t,n,r)};case 3:return function(n,r,i){return e.call(t,n,r,i)}}return function(){return e.apply(t,arguments)}}},"28tl":function(e,t,n){"use strict";(function(e){n.d(t,"a",function(){return d}),n.d(t,"b",function(){return b}),n.d(t,"c",function(){return D}),n.d(t,"d",function(){return i}),n.d(t,"e",function(){return o}),n.d(t,"f",function(){return a}),n.d(t,"g",function(){return c}),n.d(t,"h",function(){return E}),n.d(t,"i",function(){return f}),n.d(t,"j",function(){return h}),n.d(t,"k",function(){return A}),n.d(t,"l",function(){return j}),n.d(t,"m",function(){return L}),n.d(t,"n",function(){return O}),n.d(t,"o",function(){return x}),n.d(t,"p",function(){return _}),n.d(t,"q",function(){return v}),n.d(t,"r",function(){return m}),n.d(t,"s",function(){return S}),n.d(t,"t",function(){return w}),n.d(t,"u",function(){return N}),n.d(t,"v",function(){return P}),n.d(t,"w",function(){return I}),n.d(t,"x",function(){return M}),n.d(t,"y",function(){return $}),n.d(t,"z",function(){return T});
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const r={NODE_CLIENT:!1,NODE_ADMIN:!1,SDK_VERSION:"${JSCORE_VERSION}"},i=function(e,t){if(!e)throw o(t)},o=function(e){return new Error("Firebase Database ("+r.SDK_VERSION+") INTERNAL ASSERT FAILED: "+e)},s=function(e){const t=[];let n=0;for(let r=0;r<e.length;r++){let i=e.charCodeAt(r);i<128?t[n++]=i:i<2048?(t[n++]=i>>6|192,t[n++]=63&i|128):55296==(64512&i)&&r+1<e.length&&56320==(64512&e.charCodeAt(r+1))?(i=65536+((1023&i)<<10)+(1023&e.charCodeAt(++r)),t[n++]=i>>18|240,t[n++]=i>>12&63|128,t[n++]=i>>6&63|128,t[n++]=63&i|128):(t[n++]=i>>12|224,t[n++]=i>>6&63|128,t[n++]=63&i|128)}return t},a={byteToCharMap_:null,charToByteMap_:null,byteToCharMapWebSafe_:null,charToByteMapWebSafe_:null,ENCODED_VALS_BASE:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789",get ENCODED_VALS(){return this.ENCODED_VALS_BASE+"+/="},get ENCODED_VALS_WEBSAFE(){return this.ENCODED_VALS_BASE+"-_."},HAS_NATIVE_SUPPORT:"function"==typeof atob,encodeByteArray(e,t){if(!Array.isArray(e))throw Error("encodeByteArray takes an array as a parameter");this.init_();const n=t?this.byteToCharMapWebSafe_:this.byteToCharMap_,r=[];for(let t=0;t<e.length;t+=3){const i=e[t],o=t+1<e.length,s=o?e[t+1]:0,a=t+2<e.length,c=a?e[t+2]:0,l=i>>2,u=(3&i)<<4|s>>4;let h=(15&s)<<2|c>>6,d=63&c;a||(d=64,o||(h=64)),r.push(n[l],n[u],n[h],n[d])}return r.join("")},encodeString(e,t){return this.HAS_NATIVE_SUPPORT&&!t?btoa(e):this.encodeByteArray(s(e),t)},decodeString(e,t){return this.HAS_NATIVE_SUPPORT&&!t?atob(e):function(e){const t=[];let n=0,r=0;for(;n<e.length;){const i=e[n++];if(i<128)t[r++]=String.fromCharCode(i);else if(i>191&&i<224){const o=e[n++];t[r++]=String.fromCharCode((31&i)<<6|63&o)}else if(i>239&&i<365){const o=((7&i)<<18|(63&e[n++])<<12|(63&e[n++])<<6|63&e[n++])-65536;t[r++]=String.fromCharCode(55296+(o>>10)),t[r++]=String.fromCharCode(56320+(1023&o))}else{const o=e[n++],s=e[n++];t[r++]=String.fromCharCode((15&i)<<12|(63&o)<<6|63&s)}}return t.join("")}(this.decodeStringToByteArray(e,t))},decodeStringToByteArray(e,t){this.init_();const n=t?this.charToByteMapWebSafe_:this.charToByteMap_,r=[];for(let t=0;t<e.length;){const i=n[e.charAt(t++)],o=t<e.length?n[e.charAt(t)]:0,s=++t<e.length?n[e.charAt(t)]:64,a=++t<e.length?n[e.charAt(t)]:64;if(++t,null==i||null==o||null==s||null==a)throw Error();const c=i<<2|o>>4;if(r.push(c),64!==s){const e=o<<4&240|s>>2;if(r.push(e),64!==a){const e=s<<6&192|a;r.push(e)}}}return r},init_(){if(!this.byteToCharMap_){this.byteToCharMap_={},this.charToByteMap_={},this.byteToCharMapWebSafe_={},this.charToByteMapWebSafe_={};for(let e=0;e<this.ENCODED_VALS.length;e++)this.byteToCharMap_[e]=this.ENCODED_VALS.charAt(e),this.charToByteMap_[this.byteToCharMap_[e]]=e,this.byteToCharMapWebSafe_[e]=this.ENCODED_VALS_WEBSAFE.charAt(e),this.charToByteMapWebSafe_[this.byteToCharMapWebSafe_[e]]=e,e>=this.ENCODED_VALS_BASE.length&&(this.charToByteMap_[this.ENCODED_VALS_WEBSAFE.charAt(e)]=e,this.charToByteMapWebSafe_[this.ENCODED_VALS.charAt(e)]=e)}}},c=function(e){const t=s(e);return a.encodeByteArray(t,!0)},l=function(e){return c(e).replace(/\./g,"")},u=function(e){try{return a.decodeString(e,!0)}catch(e){console.error("base64Decode failed: ",e)}return null};
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
function h(e){return function e(t,n){if(!(n instanceof Object))return n;switch(n.constructor){case Date:const e=n;return new Date(e.getTime());case Object:void 0===t&&(t={});break;case Array:t=[];break;default:return n}for(const r in n)n.hasOwnProperty(r)&&"__proto__"!==r&&(t[r]=e(t[r],n[r]));return t}(void 0,e)}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class d{constructor(){this.reject=(()=>{}),this.resolve=(()=>{}),this.promise=new Promise((e,t)=>{this.resolve=e,this.reject=t})}wrapCallback(e){return(t,n)=>{t?this.reject(t):this.resolve(n),"function"==typeof e&&(this.promise.catch(()=>{}),1===e.length?e(t):e(t,n))}}}
/**
 * @license
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function f(e,t){if(e.uid)throw new Error('The "uid" field is no longer supported by mockUserToken. Please use "sub" instead for Firebase Auth User ID.');const n=t||"demo-project",r=e.iat||0,i=e.sub||e.user_id;if(!i)throw new Error("mockUserToken must contain 'sub' or 'user_id' field!");const o=Object.assign({iss:`https://securetoken.google.com/${n}`,aud:n,iat:r,exp:r+3600,auth_time:r,sub:i,user_id:i,firebase:{sign_in_provider:"custom",identities:{}}},e);return[l(JSON.stringify({alg:"none",type:"JWT"})),l(JSON.stringify(o)),""].join(".")}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function p(){return"undefined"!=typeof navigator&&"string"==typeof navigator.userAgent?navigator.userAgent:""}function _(){return"undefined"!=typeof window&&!!(window.cordova||window.phonegap||window.PhoneGap)&&/ios|iphone|ipod|ipad|android|blackberry|iemobile/i.test(p())}function m(){return"object"==typeof navigator&&"ReactNative"===navigator.product}function v(){return!0===r.NODE_CLIENT||!0===r.NODE_ADMIN}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const g="FirebaseError";class y extends Error{constructor(e,t,n){super(t),this.code=e,this.customData=n,this.name=g,Object.setPrototypeOf(this,y.prototype),Error.captureStackTrace&&Error.captureStackTrace(this,b.prototype.create)}}class b{constructor(e,t,n){this.service=e,this.serviceName=t,this.errors=n}create(e,...t){const n=t[0]||{},r=`${this.service}/${e}`,i=this.errors[e],o=i?function(e,t){return e.replace(C,(e,n)=>{const r=t[n];return null!=r?String(r):`<${n}?>`})}(i,n):"Error",s=`${this.serviceName}: ${o} (${r}).`;return new y(r,s,n)}}const C=/\{\$([^}]+)}/g;
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function w(e){return JSON.parse(e)}function T(e){return JSON.stringify(e)}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const k=function(e){let t={},n={},r={},i="";try{const o=e.split(".");t=w(u(o[0])||""),n=w(u(o[1])||""),i=o[2],r=n.d||{},delete n.d}catch(e){}return{header:t,claims:n,data:r,signature:i}},S=function(e){const t=k(e).claims;return!!t&&"object"==typeof t&&t.hasOwnProperty("iat")},O=function(e){const t=k(e).claims;return"object"==typeof t&&!0===t.admin};
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
function E(e,t){return Object.prototype.hasOwnProperty.call(e,t)}function I(e,t){return Object.prototype.hasOwnProperty.call(e,t)?e[t]:void 0}function x(e){for(const t in e)if(Object.prototype.hasOwnProperty.call(e,t))return!1;return!0}function N(e,t,n){const r={};for(const i in e)Object.prototype.hasOwnProperty.call(e,i)&&(r[i]=t.call(n,e[i],i,e));return r}function A(e,t){if(e===t)return!0;const n=Object.keys(e),r=Object.keys(t);for(const i of n){if(!r.includes(i))return!1;const n=e[i],o=t[i];if(R(n)&&R(o)){if(!A(n,o))return!1}else if(n!==o)return!1}for(const e of r)if(!n.includes(e))return!1;return!0}function R(e){return null!==e&&"object"==typeof e}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function P(e){const t=[];for(const[n,r]of Object.entries(e))Array.isArray(r)?r.forEach(e=>{t.push(encodeURIComponent(n)+"="+encodeURIComponent(e))}):t.push(encodeURIComponent(n)+"="+encodeURIComponent(r));return t.length?"&"+t.join("&"):""}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class D{constructor(){this.chain_=[],this.buf_=[],this.W_=[],this.pad_=[],this.inbuf_=0,this.total_=0,this.blockSize=64,this.pad_[0]=128;for(let e=1;e<this.blockSize;++e)this.pad_[e]=0;this.reset()}reset(){this.chain_[0]=1732584193,this.chain_[1]=4023233417,this.chain_[2]=2562383102,this.chain_[3]=271733878,this.chain_[4]=3285377520,this.inbuf_=0,this.total_=0}compress_(e,t){t||(t=0);const n=this.W_;if("string"==typeof e)for(let r=0;r<16;r++)n[r]=e.charCodeAt(t)<<24|e.charCodeAt(t+1)<<16|e.charCodeAt(t+2)<<8|e.charCodeAt(t+3),t+=4;else for(let r=0;r<16;r++)n[r]=e[t]<<24|e[t+1]<<16|e[t+2]<<8|e[t+3],t+=4;for(let e=16;e<80;e++){const t=n[e-3]^n[e-8]^n[e-14]^n[e-16];n[e]=4294967295&(t<<1|t>>>31)}let r,i,o=this.chain_[0],s=this.chain_[1],a=this.chain_[2],c=this.chain_[3],l=this.chain_[4];for(let e=0;e<80;e++){e<40?e<20?(r=c^s&(a^c),i=1518500249):(r=s^a^c,i=1859775393):e<60?(r=s&a|c&(s|a),i=2400959708):(r=s^a^c,i=3395469782);const t=(o<<5|o>>>27)+r+l+i+n[e]&4294967295;l=c,c=a,a=4294967295&(s<<30|s>>>2),s=o,o=t}this.chain_[0]=this.chain_[0]+o&4294967295,this.chain_[1]=this.chain_[1]+s&4294967295,this.chain_[2]=this.chain_[2]+a&4294967295,this.chain_[3]=this.chain_[3]+c&4294967295,this.chain_[4]=this.chain_[4]+l&4294967295}update(e,t){if(null==e)return;void 0===t&&(t=e.length);const n=t-this.blockSize;let r=0;const i=this.buf_;let o=this.inbuf_;for(;r<t;){if(0===o)for(;r<=n;)this.compress_(e,r),r+=this.blockSize;if("string"==typeof e){for(;r<t;)if(i[o]=e.charCodeAt(r),++r,++o===this.blockSize){this.compress_(i),o=0;break}}else for(;r<t;)if(i[o]=e[r],++r,++o===this.blockSize){this.compress_(i),o=0;break}}this.inbuf_=o,this.total_+=t}digest(){const e=[];let t=8*this.total_;this.inbuf_<56?this.update(this.pad_,56-this.inbuf_):this.update(this.pad_,this.blockSize-(this.inbuf_-56));for(let e=this.blockSize-1;e>=56;e--)this.buf_[e]=255&t,t/=256;this.compress_(this.buf_);let n=0;for(let t=0;t<5;t++)for(let r=24;r>=0;r-=8)e[n]=this.chain_[t]>>r&255,++n;return e}}function j(e,t){return`${e} failed: ${t} argument `}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const $=function(e){const t=[];let n=0;for(let r=0;r<e.length;r++){let o=e.charCodeAt(r);if(o>=55296&&o<=56319){const t=o-55296;i(++r<e.length,"Surrogate pair missing trail surrogate."),o=65536+(t<<10)+(e.charCodeAt(r)-56320)}o<128?t[n++]=o:o<2048?(t[n++]=o>>6|192,t[n++]=63&o|128):o<65536?(t[n++]=o>>12|224,t[n++]=o>>6&63|128,t[n++]=63&o|128):(t[n++]=o>>18|240,t[n++]=o>>12&63|128,t[n++]=o>>6&63|128,t[n++]=63&o|128)}return t},M=function(e){let t=0;for(let n=0;n<e.length;n++){const r=e.charCodeAt(n);r<128?t++:r<2048?t+=2:r>=55296&&r<=56319?(t+=4,n++):t+=3}return t};
/**
 * @license
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
function L(e){return e&&e._delegate?e._delegate:e}}).call(t,n("DuR2"))},"7+uW":function(e,t,n){"use strict";(function(e){
/*!
 * Vue.js v2.6.14
 * (c) 2014-2021 Evan You
 * Released under the MIT License.
 */
var n=Object.freeze({});function r(e){return void 0===e||null===e}function i(e){return void 0!==e&&null!==e}function o(e){return!0===e}function s(e){return"string"==typeof e||"number"==typeof e||"symbol"==typeof e||"boolean"==typeof e}function a(e){return null!==e&&"object"==typeof e}var c=Object.prototype.toString;function l(e){return"[object Object]"===c.call(e)}function u(e){return"[object RegExp]"===c.call(e)}function h(e){var t=parseFloat(String(e));return t>=0&&Math.floor(t)===t&&isFinite(e)}function d(e){return i(e)&&"function"==typeof e.then&&"function"==typeof e.catch}function f(e){return null==e?"":Array.isArray(e)||l(e)&&e.toString===c?JSON.stringify(e,null,2):String(e)}function p(e){var t=parseFloat(e);return isNaN(t)?e:t}function _(e,t){for(var n=Object.create(null),r=e.split(","),i=0;i<r.length;i++)n[r[i]]=!0;return t?function(e){return n[e.toLowerCase()]}:function(e){return n[e]}}var m=_("slot,component",!0),v=_("key,ref,slot,slot-scope,is");function g(e,t){if(e.length){var n=e.indexOf(t);if(n>-1)return e.splice(n,1)}}var y=Object.prototype.hasOwnProperty;function b(e,t){return y.call(e,t)}function C(e){var t=Object.create(null);return function(n){return t[n]||(t[n]=e(n))}}var w=/-(\w)/g,T=C(function(e){return e.replace(w,function(e,t){return t?t.toUpperCase():""})}),k=C(function(e){return e.charAt(0).toUpperCase()+e.slice(1)}),S=/\B([A-Z])/g,O=C(function(e){return e.replace(S,"-$1").toLowerCase()});var E=Function.prototype.bind?function(e,t){return e.bind(t)}:function(e,t){function n(n){var r=arguments.length;return r?r>1?e.apply(t,arguments):e.call(t,n):e.call(t)}return n._length=e.length,n};function I(e,t){t=t||0;for(var n=e.length-t,r=new Array(n);n--;)r[n]=e[n+t];return r}function x(e,t){for(var n in t)e[n]=t[n];return e}function N(e){for(var t={},n=0;n<e.length;n++)e[n]&&x(t,e[n]);return t}function A(e,t,n){}var R=function(e,t,n){return!1},P=function(e){return e};function D(e,t){if(e===t)return!0;var n=a(e),r=a(t);if(!n||!r)return!n&&!r&&String(e)===String(t);try{var i=Array.isArray(e),o=Array.isArray(t);if(i&&o)return e.length===t.length&&e.every(function(e,n){return D(e,t[n])});if(e instanceof Date&&t instanceof Date)return e.getTime()===t.getTime();if(i||o)return!1;var s=Object.keys(e),c=Object.keys(t);return s.length===c.length&&s.every(function(n){return D(e[n],t[n])})}catch(e){return!1}}function j(e,t){for(var n=0;n<e.length;n++)if(D(e[n],t))return n;return-1}function $(e){var t=!1;return function(){t||(t=!0,e.apply(this,arguments))}}var M="data-server-rendered",L=["component","directive","filter"],F=["beforeCreate","created","beforeMount","mounted","beforeUpdate","updated","beforeDestroy","destroyed","activated","deactivated","errorCaptured","serverPrefetch"],q={optionMergeStrategies:Object.create(null),silent:!1,productionTip:!1,devtools:!1,performance:!1,errorHandler:null,warnHandler:null,ignoredElements:[],keyCodes:Object.create(null),isReservedTag:R,isReservedAttr:R,isUnknownElement:R,getTagNamespace:A,parsePlatformTagName:P,mustUseProp:R,async:!0,_lifecycleHooks:F},U=/a-zA-Z\u00B7\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u037D\u037F-\u1FFF\u200C-\u200D\u203F-\u2040\u2070-\u218F\u2C00-\u2FEF\u3001-\uD7FF\uF900-\uFDCF\uFDF0-\uFFFD/;function W(e){var t=(e+"").charCodeAt(0);return 36===t||95===t}function H(e,t,n,r){Object.defineProperty(e,t,{value:n,enumerable:!!r,writable:!0,configurable:!0})}var z=new RegExp("[^"+U.source+".$_\\d]");var B,V="__proto__"in{},K="undefined"!=typeof window,G="undefined"!=typeof WXEnvironment&&!!WXEnvironment.platform,Y=G&&WXEnvironment.platform.toLowerCase(),J=K&&window.navigator.userAgent.toLowerCase(),Q=J&&/msie|trident/.test(J),X=J&&J.indexOf("msie 9.0")>0,Z=J&&J.indexOf("edge/")>0,ee=(J&&J.indexOf("android"),J&&/iphone|ipad|ipod|ios/.test(J)||"ios"===Y),te=(J&&/chrome\/\d+/.test(J),J&&/phantomjs/.test(J),J&&J.match(/firefox\/(\d+)/)),ne={}.watch,re=!1;if(K)try{var ie={};Object.defineProperty(ie,"passive",{get:function(){re=!0}}),window.addEventListener("test-passive",null,ie)}catch(e){}var oe=function(){return void 0===B&&(B=!K&&!G&&void 0!==e&&(e.process&&"server"===e.process.env.VUE_ENV)),B},se=K&&window.__VUE_DEVTOOLS_GLOBAL_HOOK__;function ae(e){return"function"==typeof e&&/native code/.test(e.toString())}var ce,le="undefined"!=typeof Symbol&&ae(Symbol)&&"undefined"!=typeof Reflect&&ae(Reflect.ownKeys);ce="undefined"!=typeof Set&&ae(Set)?Set:function(){function e(){this.set=Object.create(null)}return e.prototype.has=function(e){return!0===this.set[e]},e.prototype.add=function(e){this.set[e]=!0},e.prototype.clear=function(){this.set=Object.create(null)},e}();var ue=A,he=0,de=function(){this.id=he++,this.subs=[]};de.prototype.addSub=function(e){this.subs.push(e)},de.prototype.removeSub=function(e){g(this.subs,e)},de.prototype.depend=function(){de.target&&de.target.addDep(this)},de.prototype.notify=function(){var e=this.subs.slice();for(var t=0,n=e.length;t<n;t++)e[t].update()},de.target=null;var fe=[];function pe(e){fe.push(e),de.target=e}function _e(){fe.pop(),de.target=fe[fe.length-1]}var me=function(e,t,n,r,i,o,s,a){this.tag=e,this.data=t,this.children=n,this.text=r,this.elm=i,this.ns=void 0,this.context=o,this.fnContext=void 0,this.fnOptions=void 0,this.fnScopeId=void 0,this.key=t&&t.key,this.componentOptions=s,this.componentInstance=void 0,this.parent=void 0,this.raw=!1,this.isStatic=!1,this.isRootInsert=!0,this.isComment=!1,this.isCloned=!1,this.isOnce=!1,this.asyncFactory=a,this.asyncMeta=void 0,this.isAsyncPlaceholder=!1},ve={child:{configurable:!0}};ve.child.get=function(){return this.componentInstance},Object.defineProperties(me.prototype,ve);var ge=function(e){void 0===e&&(e="");var t=new me;return t.text=e,t.isComment=!0,t};function ye(e){return new me(void 0,void 0,void 0,String(e))}function be(e){var t=new me(e.tag,e.data,e.children&&e.children.slice(),e.text,e.elm,e.context,e.componentOptions,e.asyncFactory);return t.ns=e.ns,t.isStatic=e.isStatic,t.key=e.key,t.isComment=e.isComment,t.fnContext=e.fnContext,t.fnOptions=e.fnOptions,t.fnScopeId=e.fnScopeId,t.asyncMeta=e.asyncMeta,t.isCloned=!0,t}var Ce=Array.prototype,we=Object.create(Ce);["push","pop","shift","unshift","splice","sort","reverse"].forEach(function(e){var t=Ce[e];H(we,e,function(){for(var n=[],r=arguments.length;r--;)n[r]=arguments[r];var i,o=t.apply(this,n),s=this.__ob__;switch(e){case"push":case"unshift":i=n;break;case"splice":i=n.slice(2)}return i&&s.observeArray(i),s.dep.notify(),o})});var Te=Object.getOwnPropertyNames(we),ke=!0;function Se(e){ke=e}var Oe=function(e){var t;this.value=e,this.dep=new de,this.vmCount=0,H(e,"__ob__",this),Array.isArray(e)?(V?(t=we,e.__proto__=t):function(e,t,n){for(var r=0,i=n.length;r<i;r++){var o=n[r];H(e,o,t[o])}}(e,we,Te),this.observeArray(e)):this.walk(e)};function Ee(e,t){var n;if(a(e)&&!(e instanceof me))return b(e,"__ob__")&&e.__ob__ instanceof Oe?n=e.__ob__:ke&&!oe()&&(Array.isArray(e)||l(e))&&Object.isExtensible(e)&&!e._isVue&&(n=new Oe(e)),t&&n&&n.vmCount++,n}function Ie(e,t,n,r,i){var o=new de,s=Object.getOwnPropertyDescriptor(e,t);if(!s||!1!==s.configurable){var a=s&&s.get,c=s&&s.set;a&&!c||2!==arguments.length||(n=e[t]);var l=!i&&Ee(n);Object.defineProperty(e,t,{enumerable:!0,configurable:!0,get:function(){var t=a?a.call(e):n;return de.target&&(o.depend(),l&&(l.dep.depend(),Array.isArray(t)&&function e(t){for(var n=void 0,r=0,i=t.length;r<i;r++)(n=t[r])&&n.__ob__&&n.__ob__.dep.depend(),Array.isArray(n)&&e(n)}(t))),t},set:function(t){var r=a?a.call(e):n;t===r||t!=t&&r!=r||a&&!c||(c?c.call(e,t):n=t,l=!i&&Ee(t),o.notify())}})}}function xe(e,t,n){if(Array.isArray(e)&&h(t))return e.length=Math.max(e.length,t),e.splice(t,1,n),n;if(t in e&&!(t in Object.prototype))return e[t]=n,n;var r=e.__ob__;return e._isVue||r&&r.vmCount?n:r?(Ie(r.value,t,n),r.dep.notify(),n):(e[t]=n,n)}function Ne(e,t){if(Array.isArray(e)&&h(t))e.splice(t,1);else{var n=e.__ob__;e._isVue||n&&n.vmCount||b(e,t)&&(delete e[t],n&&n.dep.notify())}}Oe.prototype.walk=function(e){for(var t=Object.keys(e),n=0;n<t.length;n++)Ie(e,t[n])},Oe.prototype.observeArray=function(e){for(var t=0,n=e.length;t<n;t++)Ee(e[t])};var Ae=q.optionMergeStrategies;function Re(e,t){if(!t)return e;for(var n,r,i,o=le?Reflect.ownKeys(t):Object.keys(t),s=0;s<o.length;s++)"__ob__"!==(n=o[s])&&(r=e[n],i=t[n],b(e,n)?r!==i&&l(r)&&l(i)&&Re(r,i):xe(e,n,i));return e}function Pe(e,t,n){return n?function(){var r="function"==typeof t?t.call(n,n):t,i="function"==typeof e?e.call(n,n):e;return r?Re(r,i):i}:t?e?function(){return Re("function"==typeof t?t.call(this,this):t,"function"==typeof e?e.call(this,this):e)}:t:e}function De(e,t){var n=t?e?e.concat(t):Array.isArray(t)?t:[t]:e;return n?function(e){for(var t=[],n=0;n<e.length;n++)-1===t.indexOf(e[n])&&t.push(e[n]);return t}(n):n}function je(e,t,n,r){var i=Object.create(e||null);return t?x(i,t):i}Ae.data=function(e,t,n){return n?Pe(e,t,n):t&&"function"!=typeof t?e:Pe(e,t)},F.forEach(function(e){Ae[e]=De}),L.forEach(function(e){Ae[e+"s"]=je}),Ae.watch=function(e,t,n,r){if(e===ne&&(e=void 0),t===ne&&(t=void 0),!t)return Object.create(e||null);if(!e)return t;var i={};for(var o in x(i,e),t){var s=i[o],a=t[o];s&&!Array.isArray(s)&&(s=[s]),i[o]=s?s.concat(a):Array.isArray(a)?a:[a]}return i},Ae.props=Ae.methods=Ae.inject=Ae.computed=function(e,t,n,r){if(!e)return t;var i=Object.create(null);return x(i,e),t&&x(i,t),i},Ae.provide=Pe;var $e=function(e,t){return void 0===t?e:t};function Me(e,t,n){if("function"==typeof t&&(t=t.options),function(e,t){var n=e.props;if(n){var r,i,o={};if(Array.isArray(n))for(r=n.length;r--;)"string"==typeof(i=n[r])&&(o[T(i)]={type:null});else if(l(n))for(var s in n)i=n[s],o[T(s)]=l(i)?i:{type:i};e.props=o}}(t),function(e,t){var n=e.inject;if(n){var r=e.inject={};if(Array.isArray(n))for(var i=0;i<n.length;i++)r[n[i]]={from:n[i]};else if(l(n))for(var o in n){var s=n[o];r[o]=l(s)?x({from:o},s):{from:s}}}}(t),function(e){var t=e.directives;if(t)for(var n in t){var r=t[n];"function"==typeof r&&(t[n]={bind:r,update:r})}}(t),!t._base&&(t.extends&&(e=Me(e,t.extends,n)),t.mixins))for(var r=0,i=t.mixins.length;r<i;r++)e=Me(e,t.mixins[r],n);var o,s={};for(o in e)a(o);for(o in t)b(e,o)||a(o);function a(r){var i=Ae[r]||$e;s[r]=i(e[r],t[r],n,r)}return s}function Le(e,t,n,r){if("string"==typeof n){var i=e[t];if(b(i,n))return i[n];var o=T(n);if(b(i,o))return i[o];var s=k(o);return b(i,s)?i[s]:i[n]||i[o]||i[s]}}function Fe(e,t,n,r){var i=t[e],o=!b(n,e),s=n[e],a=He(Boolean,i.type);if(a>-1)if(o&&!b(i,"default"))s=!1;else if(""===s||s===O(e)){var c=He(String,i.type);(c<0||a<c)&&(s=!0)}if(void 0===s){s=function(e,t,n){if(!b(t,"default"))return;var r=t.default;0;if(e&&e.$options.propsData&&void 0===e.$options.propsData[n]&&void 0!==e._props[n])return e._props[n];return"function"==typeof r&&"Function"!==Ue(t.type)?r.call(e):r}(r,i,e);var l=ke;Se(!0),Ee(s),Se(l)}return s}var qe=/^\s*function (\w+)/;function Ue(e){var t=e&&e.toString().match(qe);return t?t[1]:""}function We(e,t){return Ue(e)===Ue(t)}function He(e,t){if(!Array.isArray(t))return We(t,e)?0:-1;for(var n=0,r=t.length;n<r;n++)if(We(t[n],e))return n;return-1}function ze(e,t,n){pe();try{if(t)for(var r=t;r=r.$parent;){var i=r.$options.errorCaptured;if(i)for(var o=0;o<i.length;o++)try{if(!1===i[o].call(r,e,t,n))return}catch(e){Ve(e,r,"errorCaptured hook")}}Ve(e,t,n)}finally{_e()}}function Be(e,t,n,r,i){var o;try{(o=n?e.apply(t,n):e.call(t))&&!o._isVue&&d(o)&&!o._handled&&(o.catch(function(e){return ze(e,r,i+" (Promise/async)")}),o._handled=!0)}catch(e){ze(e,r,i)}return o}function Ve(e,t,n){if(q.errorHandler)try{return q.errorHandler.call(null,e,t,n)}catch(t){t!==e&&Ke(t,null,"config.errorHandler")}Ke(e,t,n)}function Ke(e,t,n){if(!K&&!G||"undefined"==typeof console)throw e;console.error(e)}var Ge,Ye=!1,Je=[],Qe=!1;function Xe(){Qe=!1;var e=Je.slice(0);Je.length=0;for(var t=0;t<e.length;t++)e[t]()}if("undefined"!=typeof Promise&&ae(Promise)){var Ze=Promise.resolve();Ge=function(){Ze.then(Xe),ee&&setTimeout(A)},Ye=!0}else if(Q||"undefined"==typeof MutationObserver||!ae(MutationObserver)&&"[object MutationObserverConstructor]"!==MutationObserver.toString())Ge="undefined"!=typeof setImmediate&&ae(setImmediate)?function(){setImmediate(Xe)}:function(){setTimeout(Xe,0)};else{var et=1,tt=new MutationObserver(Xe),nt=document.createTextNode(String(et));tt.observe(nt,{characterData:!0}),Ge=function(){et=(et+1)%2,nt.data=String(et)},Ye=!0}function rt(e,t){var n;if(Je.push(function(){if(e)try{e.call(t)}catch(e){ze(e,t,"nextTick")}else n&&n(t)}),Qe||(Qe=!0,Ge()),!e&&"undefined"!=typeof Promise)return new Promise(function(e){n=e})}var it=new ce;function ot(e){!function e(t,n){var r,i;var o=Array.isArray(t);if(!o&&!a(t)||Object.isFrozen(t)||t instanceof me)return;if(t.__ob__){var s=t.__ob__.dep.id;if(n.has(s))return;n.add(s)}if(o)for(r=t.length;r--;)e(t[r],n);else for(i=Object.keys(t),r=i.length;r--;)e(t[i[r]],n)}(e,it),it.clear()}var st=C(function(e){var t="&"===e.charAt(0),n="~"===(e=t?e.slice(1):e).charAt(0),r="!"===(e=n?e.slice(1):e).charAt(0);return{name:e=r?e.slice(1):e,once:n,capture:r,passive:t}});function at(e,t){function n(){var e=arguments,r=n.fns;if(!Array.isArray(r))return Be(r,null,arguments,t,"v-on handler");for(var i=r.slice(),o=0;o<i.length;o++)Be(i[o],null,e,t,"v-on handler")}return n.fns=e,n}function ct(e,t,n,i,s,a){var c,l,u,h;for(c in e)l=e[c],u=t[c],h=st(c),r(l)||(r(u)?(r(l.fns)&&(l=e[c]=at(l,a)),o(h.once)&&(l=e[c]=s(h.name,l,h.capture)),n(h.name,l,h.capture,h.passive,h.params)):l!==u&&(u.fns=l,e[c]=u));for(c in t)r(e[c])&&i((h=st(c)).name,t[c],h.capture)}function lt(e,t,n){var s;e instanceof me&&(e=e.data.hook||(e.data.hook={}));var a=e[t];function c(){n.apply(this,arguments),g(s.fns,c)}r(a)?s=at([c]):i(a.fns)&&o(a.merged)?(s=a).fns.push(c):s=at([a,c]),s.merged=!0,e[t]=s}function ut(e,t,n,r,o){if(i(t)){if(b(t,n))return e[n]=t[n],o||delete t[n],!0;if(b(t,r))return e[n]=t[r],o||delete t[r],!0}return!1}function ht(e){return s(e)?[ye(e)]:Array.isArray(e)?function e(t,n){var a=[];var c,l,u,h;for(c=0;c<t.length;c++)r(l=t[c])||"boolean"==typeof l||(u=a.length-1,h=a[u],Array.isArray(l)?l.length>0&&(dt((l=e(l,(n||"")+"_"+c))[0])&&dt(h)&&(a[u]=ye(h.text+l[0].text),l.shift()),a.push.apply(a,l)):s(l)?dt(h)?a[u]=ye(h.text+l):""!==l&&a.push(ye(l)):dt(l)&&dt(h)?a[u]=ye(h.text+l.text):(o(t._isVList)&&i(l.tag)&&r(l.key)&&i(n)&&(l.key="__vlist"+n+"_"+c+"__"),a.push(l)));return a}(e):void 0}function dt(e){return i(e)&&i(e.text)&&!1===e.isComment}function ft(e,t){if(e){for(var n=Object.create(null),r=le?Reflect.ownKeys(e):Object.keys(e),i=0;i<r.length;i++){var o=r[i];if("__ob__"!==o){for(var s=e[o].from,a=t;a;){if(a._provided&&b(a._provided,s)){n[o]=a._provided[s];break}a=a.$parent}if(!a)if("default"in e[o]){var c=e[o].default;n[o]="function"==typeof c?c.call(t):c}else 0}}return n}}function pt(e,t){if(!e||!e.length)return{};for(var n={},r=0,i=e.length;r<i;r++){var o=e[r],s=o.data;if(s&&s.attrs&&s.attrs.slot&&delete s.attrs.slot,o.context!==t&&o.fnContext!==t||!s||null==s.slot)(n.default||(n.default=[])).push(o);else{var a=s.slot,c=n[a]||(n[a]=[]);"template"===o.tag?c.push.apply(c,o.children||[]):c.push(o)}}for(var l in n)n[l].every(_t)&&delete n[l];return n}function _t(e){return e.isComment&&!e.asyncFactory||" "===e.text}function mt(e){return e.isComment&&e.asyncFactory}function vt(e,t,r){var i,o=Object.keys(t).length>0,s=e?!!e.$stable:!o,a=e&&e.$key;if(e){if(e._normalized)return e._normalized;if(s&&r&&r!==n&&a===r.$key&&!o&&!r.$hasNormal)return r;for(var c in i={},e)e[c]&&"$"!==c[0]&&(i[c]=gt(t,c,e[c]))}else i={};for(var l in t)l in i||(i[l]=yt(t,l));return e&&Object.isExtensible(e)&&(e._normalized=i),H(i,"$stable",s),H(i,"$key",a),H(i,"$hasNormal",o),i}function gt(e,t,n){var r=function(){var e=arguments.length?n.apply(null,arguments):n({}),t=(e=e&&"object"==typeof e&&!Array.isArray(e)?[e]:ht(e))&&e[0];return e&&(!t||1===e.length&&t.isComment&&!mt(t))?void 0:e};return n.proxy&&Object.defineProperty(e,t,{get:r,enumerable:!0,configurable:!0}),r}function yt(e,t){return function(){return e[t]}}function bt(e,t){var n,r,o,s,c;if(Array.isArray(e)||"string"==typeof e)for(n=new Array(e.length),r=0,o=e.length;r<o;r++)n[r]=t(e[r],r);else if("number"==typeof e)for(n=new Array(e),r=0;r<e;r++)n[r]=t(r+1,r);else if(a(e))if(le&&e[Symbol.iterator]){n=[];for(var l=e[Symbol.iterator](),u=l.next();!u.done;)n.push(t(u.value,n.length)),u=l.next()}else for(s=Object.keys(e),n=new Array(s.length),r=0,o=s.length;r<o;r++)c=s[r],n[r]=t(e[c],c,r);return i(n)||(n=[]),n._isVList=!0,n}function Ct(e,t,n,r){var i,o=this.$scopedSlots[e];o?(n=n||{},r&&(n=x(x({},r),n)),i=o(n)||("function"==typeof t?t():t)):i=this.$slots[e]||("function"==typeof t?t():t);var s=n&&n.slot;return s?this.$createElement("template",{slot:s},i):i}function wt(e){return Le(this.$options,"filters",e)||P}function Tt(e,t){return Array.isArray(e)?-1===e.indexOf(t):e!==t}function kt(e,t,n,r,i){var o=q.keyCodes[t]||n;return i&&r&&!q.keyCodes[t]?Tt(i,r):o?Tt(o,e):r?O(r)!==t:void 0===e}function St(e,t,n,r,i){if(n)if(a(n)){var o;Array.isArray(n)&&(n=N(n));var s=function(s){if("class"===s||"style"===s||v(s))o=e;else{var a=e.attrs&&e.attrs.type;o=r||q.mustUseProp(t,a,s)?e.domProps||(e.domProps={}):e.attrs||(e.attrs={})}var c=T(s),l=O(s);c in o||l in o||(o[s]=n[s],i&&((e.on||(e.on={}))["update:"+s]=function(e){n[s]=e}))};for(var c in n)s(c)}else;return e}function Ot(e,t){var n=this._staticTrees||(this._staticTrees=[]),r=n[e];return r&&!t?r:(It(r=n[e]=this.$options.staticRenderFns[e].call(this._renderProxy,null,this),"__static__"+e,!1),r)}function Et(e,t,n){return It(e,"__once__"+t+(n?"_"+n:""),!0),e}function It(e,t,n){if(Array.isArray(e))for(var r=0;r<e.length;r++)e[r]&&"string"!=typeof e[r]&&xt(e[r],t+"_"+r,n);else xt(e,t,n)}function xt(e,t,n){e.isStatic=!0,e.key=t,e.isOnce=n}function Nt(e,t){if(t)if(l(t)){var n=e.on=e.on?x({},e.on):{};for(var r in t){var i=n[r],o=t[r];n[r]=i?[].concat(i,o):o}}else;return e}function At(e,t,n,r){t=t||{$stable:!n};for(var i=0;i<e.length;i++){var o=e[i];Array.isArray(o)?At(o,t,n):o&&(o.proxy&&(o.fn.proxy=!0),t[o.key]=o.fn)}return r&&(t.$key=r),t}function Rt(e,t){for(var n=0;n<t.length;n+=2){var r=t[n];"string"==typeof r&&r&&(e[t[n]]=t[n+1])}return e}function Pt(e,t){return"string"==typeof e?t+e:e}function Dt(e){e._o=Et,e._n=p,e._s=f,e._l=bt,e._t=Ct,e._q=D,e._i=j,e._m=Ot,e._f=wt,e._k=kt,e._b=St,e._v=ye,e._e=ge,e._u=At,e._g=Nt,e._d=Rt,e._p=Pt}function jt(e,t,r,i,s){var a,c=this,l=s.options;b(i,"_uid")?(a=Object.create(i))._original=i:(a=i,i=i._original);var u=o(l._compiled),h=!u;this.data=e,this.props=t,this.children=r,this.parent=i,this.listeners=e.on||n,this.injections=ft(l.inject,i),this.slots=function(){return c.$slots||vt(e.scopedSlots,c.$slots=pt(r,i)),c.$slots},Object.defineProperty(this,"scopedSlots",{enumerable:!0,get:function(){return vt(e.scopedSlots,this.slots())}}),u&&(this.$options=l,this.$slots=this.slots(),this.$scopedSlots=vt(e.scopedSlots,this.$slots)),l._scopeId?this._c=function(e,t,n,r){var o=zt(a,e,t,n,r,h);return o&&!Array.isArray(o)&&(o.fnScopeId=l._scopeId,o.fnContext=i),o}:this._c=function(e,t,n,r){return zt(a,e,t,n,r,h)}}function $t(e,t,n,r,i){var o=be(e);return o.fnContext=n,o.fnOptions=r,t.slot&&((o.data||(o.data={})).slot=t.slot),o}function Mt(e,t){for(var n in t)e[T(n)]=t[n]}Dt(jt.prototype);var Lt={init:function(e,t){if(e.componentInstance&&!e.componentInstance._isDestroyed&&e.data.keepAlive){var n=e;Lt.prepatch(n,n)}else{(e.componentInstance=function(e,t){var n={_isComponent:!0,_parentVnode:e,parent:t},r=e.data.inlineTemplate;i(r)&&(n.render=r.render,n.staticRenderFns=r.staticRenderFns);return new e.componentOptions.Ctor(n)}(e,Zt)).$mount(t?e.elm:void 0,t)}},prepatch:function(e,t){var r=t.componentOptions;!function(e,t,r,i,o){0;var s=i.data.scopedSlots,a=e.$scopedSlots,c=!!(s&&!s.$stable||a!==n&&!a.$stable||s&&e.$scopedSlots.$key!==s.$key||!s&&e.$scopedSlots.$key),l=!!(o||e.$options._renderChildren||c);e.$options._parentVnode=i,e.$vnode=i,e._vnode&&(e._vnode.parent=i);if(e.$options._renderChildren=o,e.$attrs=i.data.attrs||n,e.$listeners=r||n,t&&e.$options.props){Se(!1);for(var u=e._props,h=e.$options._propKeys||[],d=0;d<h.length;d++){var f=h[d],p=e.$options.props;u[f]=Fe(f,p,t,e)}Se(!0),e.$options.propsData=t}r=r||n;var _=e.$options._parentListeners;e.$options._parentListeners=r,Xt(e,r,_),l&&(e.$slots=pt(o,i.context),e.$forceUpdate());0}(t.componentInstance=e.componentInstance,r.propsData,r.listeners,t,r.children)},insert:function(e){var t,n=e.context,r=e.componentInstance;r._isMounted||(r._isMounted=!0,rn(r,"mounted")),e.data.keepAlive&&(n._isMounted?((t=r)._inactive=!1,sn.push(t)):nn(r,!0))},destroy:function(e){var t=e.componentInstance;t._isDestroyed||(e.data.keepAlive?function e(t,n){if(n&&(t._directInactive=!0,tn(t)))return;if(!t._inactive){t._inactive=!0;for(var r=0;r<t.$children.length;r++)e(t.$children[r]);rn(t,"deactivated")}}(t,!0):t.$destroy())}},Ft=Object.keys(Lt);function qt(e,t,s,c,l){if(!r(e)){var u=s.$options._base;if(a(e)&&(e=u.extend(e)),"function"==typeof e){var h;if(r(e.cid)&&void 0===(e=function(e,t){if(o(e.error)&&i(e.errorComp))return e.errorComp;if(i(e.resolved))return e.resolved;var n=Vt;n&&i(e.owners)&&-1===e.owners.indexOf(n)&&e.owners.push(n);if(o(e.loading)&&i(e.loadingComp))return e.loadingComp;if(n&&!i(e.owners)){var s=e.owners=[n],c=!0,l=null,u=null;n.$on("hook:destroyed",function(){return g(s,n)});var h=function(e){for(var t=0,n=s.length;t<n;t++)s[t].$forceUpdate();e&&(s.length=0,null!==l&&(clearTimeout(l),l=null),null!==u&&(clearTimeout(u),u=null))},f=$(function(n){e.resolved=Kt(n,t),c?s.length=0:h(!0)}),p=$(function(t){i(e.errorComp)&&(e.error=!0,h(!0))}),_=e(f,p);return a(_)&&(d(_)?r(e.resolved)&&_.then(f,p):d(_.component)&&(_.component.then(f,p),i(_.error)&&(e.errorComp=Kt(_.error,t)),i(_.loading)&&(e.loadingComp=Kt(_.loading,t),0===_.delay?e.loading=!0:l=setTimeout(function(){l=null,r(e.resolved)&&r(e.error)&&(e.loading=!0,h(!1))},_.delay||200)),i(_.timeout)&&(u=setTimeout(function(){u=null,r(e.resolved)&&p(null)},_.timeout)))),c=!1,e.loading?e.loadingComp:e.resolved}}(h=e,u)))return function(e,t,n,r,i){var o=ge();return o.asyncFactory=e,o.asyncMeta={data:t,context:n,children:r,tag:i},o}(h,t,s,c,l);t=t||{},On(e),i(t.model)&&function(e,t){var n=e.model&&e.model.prop||"value",r=e.model&&e.model.event||"input";(t.attrs||(t.attrs={}))[n]=t.model.value;var o=t.on||(t.on={}),s=o[r],a=t.model.callback;i(s)?(Array.isArray(s)?-1===s.indexOf(a):s!==a)&&(o[r]=[a].concat(s)):o[r]=a}(e.options,t);var f=function(e,t,n){var o=t.options.props;if(!r(o)){var s={},a=e.attrs,c=e.props;if(i(a)||i(c))for(var l in o){var u=O(l);ut(s,c,l,u,!0)||ut(s,a,l,u,!1)}return s}}(t,e);if(o(e.options.functional))return function(e,t,r,o,s){var a=e.options,c={},l=a.props;if(i(l))for(var u in l)c[u]=Fe(u,l,t||n);else i(r.attrs)&&Mt(c,r.attrs),i(r.props)&&Mt(c,r.props);var h=new jt(r,c,s,o,e),d=a.render.call(null,h._c,h);if(d instanceof me)return $t(d,r,h.parent,a);if(Array.isArray(d)){for(var f=ht(d)||[],p=new Array(f.length),_=0;_<f.length;_++)p[_]=$t(f[_],r,h.parent,a);return p}}(e,f,t,s,c);var p=t.on;if(t.on=t.nativeOn,o(e.options.abstract)){var _=t.slot;t={},_&&(t.slot=_)}!function(e){for(var t=e.hook||(e.hook={}),n=0;n<Ft.length;n++){var r=Ft[n],i=t[r],o=Lt[r];i===o||i&&i._merged||(t[r]=i?Ut(o,i):o)}}(t);var m=e.options.name||l;return new me("vue-component-"+e.cid+(m?"-"+m:""),t,void 0,void 0,void 0,s,{Ctor:e,propsData:f,listeners:p,tag:l,children:c},h)}}}function Ut(e,t){var n=function(n,r){e(n,r),t(n,r)};return n._merged=!0,n}var Wt=1,Ht=2;function zt(e,t,n,c,l,u){return(Array.isArray(n)||s(n))&&(l=c,c=n,n=void 0),o(u)&&(l=Ht),function(e,t,n,s,c){if(i(n)&&i(n.__ob__))return ge();i(n)&&i(n.is)&&(t=n.is);if(!t)return ge();0;Array.isArray(s)&&"function"==typeof s[0]&&((n=n||{}).scopedSlots={default:s[0]},s.length=0);c===Ht?s=ht(s):c===Wt&&(s=function(e){for(var t=0;t<e.length;t++)if(Array.isArray(e[t]))return Array.prototype.concat.apply([],e);return e}(s));var l,u;if("string"==typeof t){var h;u=e.$vnode&&e.$vnode.ns||q.getTagNamespace(t),l=q.isReservedTag(t)?new me(q.parsePlatformTagName(t),n,s,void 0,void 0,e):n&&n.pre||!i(h=Le(e.$options,"components",t))?new me(t,n,s,void 0,void 0,e):qt(h,n,e,s,t)}else l=qt(t,n,e,s);return Array.isArray(l)?l:i(l)?(i(u)&&function e(t,n,s){t.ns=n;"foreignObject"===t.tag&&(n=void 0,s=!0);if(i(t.children))for(var a=0,c=t.children.length;a<c;a++){var l=t.children[a];i(l.tag)&&(r(l.ns)||o(s)&&"svg"!==l.tag)&&e(l,n,s)}}(l,u),i(n)&&function(e){a(e.style)&&ot(e.style);a(e.class)&&ot(e.class)}(n),l):ge()}(e,t,n,c,l)}var Bt,Vt=null;function Kt(e,t){return(e.__esModule||le&&"Module"===e[Symbol.toStringTag])&&(e=e.default),a(e)?t.extend(e):e}function Gt(e){if(Array.isArray(e))for(var t=0;t<e.length;t++){var n=e[t];if(i(n)&&(i(n.componentOptions)||mt(n)))return n}}function Yt(e,t){Bt.$on(e,t)}function Jt(e,t){Bt.$off(e,t)}function Qt(e,t){var n=Bt;return function r(){null!==t.apply(null,arguments)&&n.$off(e,r)}}function Xt(e,t,n){Bt=e,ct(t,n||{},Yt,Jt,Qt,e),Bt=void 0}var Zt=null;function en(e){var t=Zt;return Zt=e,function(){Zt=t}}function tn(e){for(;e&&(e=e.$parent);)if(e._inactive)return!0;return!1}function nn(e,t){if(t){if(e._directInactive=!1,tn(e))return}else if(e._directInactive)return;if(e._inactive||null===e._inactive){e._inactive=!1;for(var n=0;n<e.$children.length;n++)nn(e.$children[n]);rn(e,"activated")}}function rn(e,t){pe();var n=e.$options[t],r=t+" hook";if(n)for(var i=0,o=n.length;i<o;i++)Be(n[i],e,null,e,r);e._hasHookEvent&&e.$emit("hook:"+t),_e()}var on=[],sn=[],an={},cn=!1,ln=!1,un=0;var hn=0,dn=Date.now;if(K&&!Q){var fn=window.performance;fn&&"function"==typeof fn.now&&dn()>document.createEvent("Event").timeStamp&&(dn=function(){return fn.now()})}function pn(){var e,t;for(hn=dn(),ln=!0,on.sort(function(e,t){return e.id-t.id}),un=0;un<on.length;un++)(e=on[un]).before&&e.before(),t=e.id,an[t]=null,e.run();var n=sn.slice(),r=on.slice();un=on.length=sn.length=0,an={},cn=ln=!1,function(e){for(var t=0;t<e.length;t++)e[t]._inactive=!0,nn(e[t],!0)}(n),function(e){var t=e.length;for(;t--;){var n=e[t],r=n.vm;r._watcher===n&&r._isMounted&&!r._isDestroyed&&rn(r,"updated")}}(r),se&&q.devtools&&se.emit("flush")}var _n=0,mn=function(e,t,n,r,i){this.vm=e,i&&(e._watcher=this),e._watchers.push(this),r?(this.deep=!!r.deep,this.user=!!r.user,this.lazy=!!r.lazy,this.sync=!!r.sync,this.before=r.before):this.deep=this.user=this.lazy=this.sync=!1,this.cb=n,this.id=++_n,this.active=!0,this.dirty=this.lazy,this.deps=[],this.newDeps=[],this.depIds=new ce,this.newDepIds=new ce,this.expression="","function"==typeof t?this.getter=t:(this.getter=function(e){if(!z.test(e)){var t=e.split(".");return function(e){for(var n=0;n<t.length;n++){if(!e)return;e=e[t[n]]}return e}}}(t),this.getter||(this.getter=A)),this.value=this.lazy?void 0:this.get()};mn.prototype.get=function(){var e;pe(this);var t=this.vm;try{e=this.getter.call(t,t)}catch(e){if(!this.user)throw e;ze(e,t,'getter for watcher "'+this.expression+'"')}finally{this.deep&&ot(e),_e(),this.cleanupDeps()}return e},mn.prototype.addDep=function(e){var t=e.id;this.newDepIds.has(t)||(this.newDepIds.add(t),this.newDeps.push(e),this.depIds.has(t)||e.addSub(this))},mn.prototype.cleanupDeps=function(){for(var e=this.deps.length;e--;){var t=this.deps[e];this.newDepIds.has(t.id)||t.removeSub(this)}var n=this.depIds;this.depIds=this.newDepIds,this.newDepIds=n,this.newDepIds.clear(),n=this.deps,this.deps=this.newDeps,this.newDeps=n,this.newDeps.length=0},mn.prototype.update=function(){this.lazy?this.dirty=!0:this.sync?this.run():function(e){var t=e.id;if(null==an[t]){if(an[t]=!0,ln){for(var n=on.length-1;n>un&&on[n].id>e.id;)n--;on.splice(n+1,0,e)}else on.push(e);cn||(cn=!0,rt(pn))}}(this)},mn.prototype.run=function(){if(this.active){var e=this.get();if(e!==this.value||a(e)||this.deep){var t=this.value;if(this.value=e,this.user){var n='callback for watcher "'+this.expression+'"';Be(this.cb,this.vm,[e,t],this.vm,n)}else this.cb.call(this.vm,e,t)}}},mn.prototype.evaluate=function(){this.value=this.get(),this.dirty=!1},mn.prototype.depend=function(){for(var e=this.deps.length;e--;)this.deps[e].depend()},mn.prototype.teardown=function(){if(this.active){this.vm._isBeingDestroyed||g(this.vm._watchers,this);for(var e=this.deps.length;e--;)this.deps[e].removeSub(this);this.active=!1}};var vn={enumerable:!0,configurable:!0,get:A,set:A};function gn(e,t,n){vn.get=function(){return this[t][n]},vn.set=function(e){this[t][n]=e},Object.defineProperty(e,n,vn)}function yn(e){e._watchers=[];var t=e.$options;t.props&&function(e,t){var n=e.$options.propsData||{},r=e._props={},i=e.$options._propKeys=[],o=!e.$parent;o||Se(!1);var s=function(o){i.push(o);var s=Fe(o,t,n,e);Ie(r,o,s),o in e||gn(e,"_props",o)};for(var a in t)s(a);Se(!0)}(e,t.props),t.methods&&function(e,t){e.$options.props;for(var n in t)e[n]="function"!=typeof t[n]?A:E(t[n],e)}(e,t.methods),t.data?function(e){var t=e.$options.data;l(t=e._data="function"==typeof t?function(e,t){pe();try{return e.call(t,t)}catch(e){return ze(e,t,"data()"),{}}finally{_e()}}(t,e):t||{})||(t={});var n=Object.keys(t),r=e.$options.props,i=(e.$options.methods,n.length);for(;i--;){var o=n[i];0,r&&b(r,o)||W(o)||gn(e,"_data",o)}Ee(t,!0)}(e):Ee(e._data={},!0),t.computed&&function(e,t){var n=e._computedWatchers=Object.create(null),r=oe();for(var i in t){var o=t[i],s="function"==typeof o?o:o.get;0,r||(n[i]=new mn(e,s||A,A,bn)),i in e||Cn(e,i,o)}}(e,t.computed),t.watch&&t.watch!==ne&&function(e,t){for(var n in t){var r=t[n];if(Array.isArray(r))for(var i=0;i<r.length;i++)kn(e,n,r[i]);else kn(e,n,r)}}(e,t.watch)}var bn={lazy:!0};function Cn(e,t,n){var r=!oe();"function"==typeof n?(vn.get=r?wn(t):Tn(n),vn.set=A):(vn.get=n.get?r&&!1!==n.cache?wn(t):Tn(n.get):A,vn.set=n.set||A),Object.defineProperty(e,t,vn)}function wn(e){return function(){var t=this._computedWatchers&&this._computedWatchers[e];if(t)return t.dirty&&t.evaluate(),de.target&&t.depend(),t.value}}function Tn(e){return function(){return e.call(this,this)}}function kn(e,t,n,r){return l(n)&&(r=n,n=n.handler),"string"==typeof n&&(n=e[n]),e.$watch(t,n,r)}var Sn=0;function On(e){var t=e.options;if(e.super){var n=On(e.super);if(n!==e.superOptions){e.superOptions=n;var r=function(e){var t,n=e.options,r=e.sealedOptions;for(var i in n)n[i]!==r[i]&&(t||(t={}),t[i]=n[i]);return t}(e);r&&x(e.extendOptions,r),(t=e.options=Me(n,e.extendOptions)).name&&(t.components[t.name]=e)}}return t}function En(e){this._init(e)}function In(e){e.cid=0;var t=1;e.extend=function(e){e=e||{};var n=this,r=n.cid,i=e._Ctor||(e._Ctor={});if(i[r])return i[r];var o=e.name||n.options.name;var s=function(e){this._init(e)};return(s.prototype=Object.create(n.prototype)).constructor=s,s.cid=t++,s.options=Me(n.options,e),s.super=n,s.options.props&&function(e){var t=e.options.props;for(var n in t)gn(e.prototype,"_props",n)}(s),s.options.computed&&function(e){var t=e.options.computed;for(var n in t)Cn(e.prototype,n,t[n])}(s),s.extend=n.extend,s.mixin=n.mixin,s.use=n.use,L.forEach(function(e){s[e]=n[e]}),o&&(s.options.components[o]=s),s.superOptions=n.options,s.extendOptions=e,s.sealedOptions=x({},s.options),i[r]=s,s}}function xn(e){return e&&(e.Ctor.options.name||e.tag)}function Nn(e,t){return Array.isArray(e)?e.indexOf(t)>-1:"string"==typeof e?e.split(",").indexOf(t)>-1:!!u(e)&&e.test(t)}function An(e,t){var n=e.cache,r=e.keys,i=e._vnode;for(var o in n){var s=n[o];if(s){var a=s.name;a&&!t(a)&&Rn(n,o,r,i)}}}function Rn(e,t,n,r){var i=e[t];!i||r&&i.tag===r.tag||i.componentInstance.$destroy(),e[t]=null,g(n,t)}!function(e){e.prototype._init=function(e){var t=this;t._uid=Sn++,t._isVue=!0,e&&e._isComponent?function(e,t){var n=e.$options=Object.create(e.constructor.options),r=t._parentVnode;n.parent=t.parent,n._parentVnode=r;var i=r.componentOptions;n.propsData=i.propsData,n._parentListeners=i.listeners,n._renderChildren=i.children,n._componentTag=i.tag,t.render&&(n.render=t.render,n.staticRenderFns=t.staticRenderFns)}(t,e):t.$options=Me(On(t.constructor),e||{},t),t._renderProxy=t,t._self=t,function(e){var t=e.$options,n=t.parent;if(n&&!t.abstract){for(;n.$options.abstract&&n.$parent;)n=n.$parent;n.$children.push(e)}e.$parent=n,e.$root=n?n.$root:e,e.$children=[],e.$refs={},e._watcher=null,e._inactive=null,e._directInactive=!1,e._isMounted=!1,e._isDestroyed=!1,e._isBeingDestroyed=!1}(t),function(e){e._events=Object.create(null),e._hasHookEvent=!1;var t=e.$options._parentListeners;t&&Xt(e,t)}(t),function(e){e._vnode=null,e._staticTrees=null;var t=e.$options,r=e.$vnode=t._parentVnode,i=r&&r.context;e.$slots=pt(t._renderChildren,i),e.$scopedSlots=n,e._c=function(t,n,r,i){return zt(e,t,n,r,i,!1)},e.$createElement=function(t,n,r,i){return zt(e,t,n,r,i,!0)};var o=r&&r.data;Ie(e,"$attrs",o&&o.attrs||n,null,!0),Ie(e,"$listeners",t._parentListeners||n,null,!0)}(t),rn(t,"beforeCreate"),function(e){var t=ft(e.$options.inject,e);t&&(Se(!1),Object.keys(t).forEach(function(n){Ie(e,n,t[n])}),Se(!0))}(t),yn(t),function(e){var t=e.$options.provide;t&&(e._provided="function"==typeof t?t.call(e):t)}(t),rn(t,"created"),t.$options.el&&t.$mount(t.$options.el)}}(En),function(e){var t={get:function(){return this._data}},n={get:function(){return this._props}};Object.defineProperty(e.prototype,"$data",t),Object.defineProperty(e.prototype,"$props",n),e.prototype.$set=xe,e.prototype.$delete=Ne,e.prototype.$watch=function(e,t,n){if(l(t))return kn(this,e,t,n);(n=n||{}).user=!0;var r=new mn(this,e,t,n);if(n.immediate){var i='callback for immediate watcher "'+r.expression+'"';pe(),Be(t,this,[r.value],this,i),_e()}return function(){r.teardown()}}}(En),function(e){var t=/^hook:/;e.prototype.$on=function(e,n){var r=this;if(Array.isArray(e))for(var i=0,o=e.length;i<o;i++)r.$on(e[i],n);else(r._events[e]||(r._events[e]=[])).push(n),t.test(e)&&(r._hasHookEvent=!0);return r},e.prototype.$once=function(e,t){var n=this;function r(){n.$off(e,r),t.apply(n,arguments)}return r.fn=t,n.$on(e,r),n},e.prototype.$off=function(e,t){var n=this;if(!arguments.length)return n._events=Object.create(null),n;if(Array.isArray(e)){for(var r=0,i=e.length;r<i;r++)n.$off(e[r],t);return n}var o,s=n._events[e];if(!s)return n;if(!t)return n._events[e]=null,n;for(var a=s.length;a--;)if((o=s[a])===t||o.fn===t){s.splice(a,1);break}return n},e.prototype.$emit=function(e){var t=this,n=t._events[e];if(n){n=n.length>1?I(n):n;for(var r=I(arguments,1),i='event handler for "'+e+'"',o=0,s=n.length;o<s;o++)Be(n[o],t,r,t,i)}return t}}(En),function(e){e.prototype._update=function(e,t){var n=this,r=n.$el,i=n._vnode,o=en(n);n._vnode=e,n.$el=i?n.__patch__(i,e):n.__patch__(n.$el,e,t,!1),o(),r&&(r.__vue__=null),n.$el&&(n.$el.__vue__=n),n.$vnode&&n.$parent&&n.$vnode===n.$parent._vnode&&(n.$parent.$el=n.$el)},e.prototype.$forceUpdate=function(){this._watcher&&this._watcher.update()},e.prototype.$destroy=function(){var e=this;if(!e._isBeingDestroyed){rn(e,"beforeDestroy"),e._isBeingDestroyed=!0;var t=e.$parent;!t||t._isBeingDestroyed||e.$options.abstract||g(t.$children,e),e._watcher&&e._watcher.teardown();for(var n=e._watchers.length;n--;)e._watchers[n].teardown();e._data.__ob__&&e._data.__ob__.vmCount--,e._isDestroyed=!0,e.__patch__(e._vnode,null),rn(e,"destroyed"),e.$off(),e.$el&&(e.$el.__vue__=null),e.$vnode&&(e.$vnode.parent=null)}}}(En),function(e){Dt(e.prototype),e.prototype.$nextTick=function(e){return rt(e,this)},e.prototype._render=function(){var e,t=this,n=t.$options,r=n.render,i=n._parentVnode;i&&(t.$scopedSlots=vt(i.data.scopedSlots,t.$slots,t.$scopedSlots)),t.$vnode=i;try{Vt=t,e=r.call(t._renderProxy,t.$createElement)}catch(n){ze(n,t,"render"),e=t._vnode}finally{Vt=null}return Array.isArray(e)&&1===e.length&&(e=e[0]),e instanceof me||(e=ge()),e.parent=i,e}}(En);var Pn=[String,RegExp,Array],Dn={KeepAlive:{name:"keep-alive",abstract:!0,props:{include:Pn,exclude:Pn,max:[String,Number]},methods:{cacheVNode:function(){var e=this.cache,t=this.keys,n=this.vnodeToCache,r=this.keyToCache;if(n){var i=n.tag,o=n.componentInstance,s=n.componentOptions;e[r]={name:xn(s),tag:i,componentInstance:o},t.push(r),this.max&&t.length>parseInt(this.max)&&Rn(e,t[0],t,this._vnode),this.vnodeToCache=null}}},created:function(){this.cache=Object.create(null),this.keys=[]},destroyed:function(){for(var e in this.cache)Rn(this.cache,e,this.keys)},mounted:function(){var e=this;this.cacheVNode(),this.$watch("include",function(t){An(e,function(e){return Nn(t,e)})}),this.$watch("exclude",function(t){An(e,function(e){return!Nn(t,e)})})},updated:function(){this.cacheVNode()},render:function(){var e=this.$slots.default,t=Gt(e),n=t&&t.componentOptions;if(n){var r=xn(n),i=this.include,o=this.exclude;if(i&&(!r||!Nn(i,r))||o&&r&&Nn(o,r))return t;var s=this.cache,a=this.keys,c=null==t.key?n.Ctor.cid+(n.tag?"::"+n.tag:""):t.key;s[c]?(t.componentInstance=s[c].componentInstance,g(a,c),a.push(c)):(this.vnodeToCache=t,this.keyToCache=c),t.data.keepAlive=!0}return t||e&&e[0]}}};!function(e){var t={get:function(){return q}};Object.defineProperty(e,"config",t),e.util={warn:ue,extend:x,mergeOptions:Me,defineReactive:Ie},e.set=xe,e.delete=Ne,e.nextTick=rt,e.observable=function(e){return Ee(e),e},e.options=Object.create(null),L.forEach(function(t){e.options[t+"s"]=Object.create(null)}),e.options._base=e,x(e.options.components,Dn),function(e){e.use=function(e){var t=this._installedPlugins||(this._installedPlugins=[]);if(t.indexOf(e)>-1)return this;var n=I(arguments,1);return n.unshift(this),"function"==typeof e.install?e.install.apply(e,n):"function"==typeof e&&e.apply(null,n),t.push(e),this}}(e),function(e){e.mixin=function(e){return this.options=Me(this.options,e),this}}(e),In(e),function(e){L.forEach(function(t){e[t]=function(e,n){return n?("component"===t&&l(n)&&(n.name=n.name||e,n=this.options._base.extend(n)),"directive"===t&&"function"==typeof n&&(n={bind:n,update:n}),this.options[t+"s"][e]=n,n):this.options[t+"s"][e]}})}(e)}(En),Object.defineProperty(En.prototype,"$isServer",{get:oe}),Object.defineProperty(En.prototype,"$ssrContext",{get:function(){return this.$vnode&&this.$vnode.ssrContext}}),Object.defineProperty(En,"FunctionalRenderContext",{value:jt}),En.version="2.6.14";var jn=_("style,class"),$n=_("input,textarea,option,select,progress"),Mn=function(e,t,n){return"value"===n&&$n(e)&&"button"!==t||"selected"===n&&"option"===e||"checked"===n&&"input"===e||"muted"===n&&"video"===e},Ln=_("contenteditable,draggable,spellcheck"),Fn=_("events,caret,typing,plaintext-only"),qn=function(e,t){return Bn(t)||"false"===t?"false":"contenteditable"===e&&Fn(t)?t:"true"},Un=_("allowfullscreen,async,autofocus,autoplay,checked,compact,controls,declare,default,defaultchecked,defaultmuted,defaultselected,defer,disabled,enabled,formnovalidate,hidden,indeterminate,inert,ismap,itemscope,loop,multiple,muted,nohref,noresize,noshade,novalidate,nowrap,open,pauseonexit,readonly,required,reversed,scoped,seamless,selected,sortable,truespeed,typemustmatch,visible"),Wn="http://www.w3.org/1999/xlink",Hn=function(e){return":"===e.charAt(5)&&"xlink"===e.slice(0,5)},zn=function(e){return Hn(e)?e.slice(6,e.length):""},Bn=function(e){return null==e||!1===e};function Vn(e){for(var t=e.data,n=e,r=e;i(r.componentInstance);)(r=r.componentInstance._vnode)&&r.data&&(t=Kn(r.data,t));for(;i(n=n.parent);)n&&n.data&&(t=Kn(t,n.data));return function(e,t){if(i(e)||i(t))return Gn(e,Yn(t));return""}(t.staticClass,t.class)}function Kn(e,t){return{staticClass:Gn(e.staticClass,t.staticClass),class:i(e.class)?[e.class,t.class]:t.class}}function Gn(e,t){return e?t?e+" "+t:e:t||""}function Yn(e){return Array.isArray(e)?function(e){for(var t,n="",r=0,o=e.length;r<o;r++)i(t=Yn(e[r]))&&""!==t&&(n&&(n+=" "),n+=t);return n}(e):a(e)?function(e){var t="";for(var n in e)e[n]&&(t&&(t+=" "),t+=n);return t}(e):"string"==typeof e?e:""}var Jn={svg:"http://www.w3.org/2000/svg",math:"http://www.w3.org/1998/Math/MathML"},Qn=_("html,body,base,head,link,meta,style,title,address,article,aside,footer,header,h1,h2,h3,h4,h5,h6,hgroup,nav,section,div,dd,dl,dt,figcaption,figure,picture,hr,img,li,main,ol,p,pre,ul,a,b,abbr,bdi,bdo,br,cite,code,data,dfn,em,i,kbd,mark,q,rp,rt,rtc,ruby,s,samp,small,span,strong,sub,sup,time,u,var,wbr,area,audio,map,track,video,embed,object,param,source,canvas,script,noscript,del,ins,caption,col,colgroup,table,thead,tbody,td,th,tr,button,datalist,fieldset,form,input,label,legend,meter,optgroup,option,output,progress,select,textarea,details,dialog,menu,menuitem,summary,content,element,shadow,template,blockquote,iframe,tfoot"),Xn=_("svg,animate,circle,clippath,cursor,defs,desc,ellipse,filter,font-face,foreignobject,g,glyph,image,line,marker,mask,missing-glyph,path,pattern,polygon,polyline,rect,switch,symbol,text,textpath,tspan,use,view",!0),Zn=function(e){return Qn(e)||Xn(e)};function er(e){return Xn(e)?"svg":"math"===e?"math":void 0}var tr=Object.create(null);var nr=_("text,number,password,search,email,tel,url");function rr(e){if("string"==typeof e){var t=document.querySelector(e);return t||document.createElement("div")}return e}var ir=Object.freeze({createElement:function(e,t){var n=document.createElement(e);return"select"!==e?n:(t.data&&t.data.attrs&&void 0!==t.data.attrs.multiple&&n.setAttribute("multiple","multiple"),n)},createElementNS:function(e,t){return document.createElementNS(Jn[e],t)},createTextNode:function(e){return document.createTextNode(e)},createComment:function(e){return document.createComment(e)},insertBefore:function(e,t,n){e.insertBefore(t,n)},removeChild:function(e,t){e.removeChild(t)},appendChild:function(e,t){e.appendChild(t)},parentNode:function(e){return e.parentNode},nextSibling:function(e){return e.nextSibling},tagName:function(e){return e.tagName},setTextContent:function(e,t){e.textContent=t},setStyleScope:function(e,t){e.setAttribute(t,"")}}),or={create:function(e,t){sr(t)},update:function(e,t){e.data.ref!==t.data.ref&&(sr(e,!0),sr(t))},destroy:function(e){sr(e,!0)}};function sr(e,t){var n=e.data.ref;if(i(n)){var r=e.context,o=e.componentInstance||e.elm,s=r.$refs;t?Array.isArray(s[n])?g(s[n],o):s[n]===o&&(s[n]=void 0):e.data.refInFor?Array.isArray(s[n])?s[n].indexOf(o)<0&&s[n].push(o):s[n]=[o]:s[n]=o}}var ar=new me("",{},[]),cr=["create","activate","update","remove","destroy"];function lr(e,t){return e.key===t.key&&e.asyncFactory===t.asyncFactory&&(e.tag===t.tag&&e.isComment===t.isComment&&i(e.data)===i(t.data)&&function(e,t){if("input"!==e.tag)return!0;var n,r=i(n=e.data)&&i(n=n.attrs)&&n.type,o=i(n=t.data)&&i(n=n.attrs)&&n.type;return r===o||nr(r)&&nr(o)}(e,t)||o(e.isAsyncPlaceholder)&&r(t.asyncFactory.error))}function ur(e,t,n){var r,o,s={};for(r=t;r<=n;++r)i(o=e[r].key)&&(s[o]=r);return s}var hr={create:dr,update:dr,destroy:function(e){dr(e,ar)}};function dr(e,t){(e.data.directives||t.data.directives)&&function(e,t){var n,r,i,o=e===ar,s=t===ar,a=pr(e.data.directives,e.context),c=pr(t.data.directives,t.context),l=[],u=[];for(n in c)r=a[n],i=c[n],r?(i.oldValue=r.value,i.oldArg=r.arg,mr(i,"update",t,e),i.def&&i.def.componentUpdated&&u.push(i)):(mr(i,"bind",t,e),i.def&&i.def.inserted&&l.push(i));if(l.length){var h=function(){for(var n=0;n<l.length;n++)mr(l[n],"inserted",t,e)};o?lt(t,"insert",h):h()}u.length&&lt(t,"postpatch",function(){for(var n=0;n<u.length;n++)mr(u[n],"componentUpdated",t,e)});if(!o)for(n in a)c[n]||mr(a[n],"unbind",e,e,s)}(e,t)}var fr=Object.create(null);function pr(e,t){var n,r,i=Object.create(null);if(!e)return i;for(n=0;n<e.length;n++)(r=e[n]).modifiers||(r.modifiers=fr),i[_r(r)]=r,r.def=Le(t.$options,"directives",r.name);return i}function _r(e){return e.rawName||e.name+"."+Object.keys(e.modifiers||{}).join(".")}function mr(e,t,n,r,i){var o=e.def&&e.def[t];if(o)try{o(n.elm,e,n,r,i)}catch(r){ze(r,n.context,"directive "+e.name+" "+t+" hook")}}var vr=[or,hr];function gr(e,t){var n=t.componentOptions;if(!(i(n)&&!1===n.Ctor.options.inheritAttrs||r(e.data.attrs)&&r(t.data.attrs))){var o,s,a=t.elm,c=e.data.attrs||{},l=t.data.attrs||{};for(o in i(l.__ob__)&&(l=t.data.attrs=x({},l)),l)s=l[o],c[o]!==s&&yr(a,o,s,t.data.pre);for(o in(Q||Z)&&l.value!==c.value&&yr(a,"value",l.value),c)r(l[o])&&(Hn(o)?a.removeAttributeNS(Wn,zn(o)):Ln(o)||a.removeAttribute(o))}}function yr(e,t,n,r){r||e.tagName.indexOf("-")>-1?br(e,t,n):Un(t)?Bn(n)?e.removeAttribute(t):(n="allowfullscreen"===t&&"EMBED"===e.tagName?"true":t,e.setAttribute(t,n)):Ln(t)?e.setAttribute(t,qn(t,n)):Hn(t)?Bn(n)?e.removeAttributeNS(Wn,zn(t)):e.setAttributeNS(Wn,t,n):br(e,t,n)}function br(e,t,n){if(Bn(n))e.removeAttribute(t);else{if(Q&&!X&&"TEXTAREA"===e.tagName&&"placeholder"===t&&""!==n&&!e.__ieph){var r=function(t){t.stopImmediatePropagation(),e.removeEventListener("input",r)};e.addEventListener("input",r),e.__ieph=!0}e.setAttribute(t,n)}}var Cr={create:gr,update:gr};function wr(e,t){var n=t.elm,o=t.data,s=e.data;if(!(r(o.staticClass)&&r(o.class)&&(r(s)||r(s.staticClass)&&r(s.class)))){var a=Vn(t),c=n._transitionClasses;i(c)&&(a=Gn(a,Yn(c))),a!==n._prevClass&&(n.setAttribute("class",a),n._prevClass=a)}}var Tr,kr,Sr,Or,Er,Ir,xr={create:wr,update:wr},Nr=/[\w).+\-_$\]]/;function Ar(e){var t,n,r,i,o,s=!1,a=!1,c=!1,l=!1,u=0,h=0,d=0,f=0;for(r=0;r<e.length;r++)if(n=t,t=e.charCodeAt(r),s)39===t&&92!==n&&(s=!1);else if(a)34===t&&92!==n&&(a=!1);else if(c)96===t&&92!==n&&(c=!1);else if(l)47===t&&92!==n&&(l=!1);else if(124!==t||124===e.charCodeAt(r+1)||124===e.charCodeAt(r-1)||u||h||d){switch(t){case 34:a=!0;break;case 39:s=!0;break;case 96:c=!0;break;case 40:d++;break;case 41:d--;break;case 91:h++;break;case 93:h--;break;case 123:u++;break;case 125:u--}if(47===t){for(var p=r-1,_=void 0;p>=0&&" "===(_=e.charAt(p));p--);_&&Nr.test(_)||(l=!0)}}else void 0===i?(f=r+1,i=e.slice(0,r).trim()):m();function m(){(o||(o=[])).push(e.slice(f,r).trim()),f=r+1}if(void 0===i?i=e.slice(0,r).trim():0!==f&&m(),o)for(r=0;r<o.length;r++)i=Rr(i,o[r]);return i}function Rr(e,t){var n=t.indexOf("(");if(n<0)return'_f("'+t+'")('+e+")";var r=t.slice(0,n),i=t.slice(n+1);return'_f("'+r+'")('+e+(")"!==i?","+i:i)}function Pr(e,t){console.error("[Vue compiler]: "+e)}function Dr(e,t){return e?e.map(function(e){return e[t]}).filter(function(e){return e}):[]}function jr(e,t,n,r,i){(e.props||(e.props=[])).push(Br({name:t,value:n,dynamic:i},r)),e.plain=!1}function $r(e,t,n,r,i){(i?e.dynamicAttrs||(e.dynamicAttrs=[]):e.attrs||(e.attrs=[])).push(Br({name:t,value:n,dynamic:i},r)),e.plain=!1}function Mr(e,t,n,r){e.attrsMap[t]=n,e.attrsList.push(Br({name:t,value:n},r))}function Lr(e,t,n,r,i,o,s,a){(e.directives||(e.directives=[])).push(Br({name:t,rawName:n,value:r,arg:i,isDynamicArg:o,modifiers:s},a)),e.plain=!1}function Fr(e,t,n){return n?"_p("+t+',"'+e+'")':e+t}function qr(e,t,r,i,o,s,a,c){var l;(i=i||n).right?c?t="("+t+")==='click'?'contextmenu':("+t+")":"click"===t&&(t="contextmenu",delete i.right):i.middle&&(c?t="("+t+")==='click'?'mouseup':("+t+")":"click"===t&&(t="mouseup")),i.capture&&(delete i.capture,t=Fr("!",t,c)),i.once&&(delete i.once,t=Fr("~",t,c)),i.passive&&(delete i.passive,t=Fr("&",t,c)),i.native?(delete i.native,l=e.nativeEvents||(e.nativeEvents={})):l=e.events||(e.events={});var u=Br({value:r.trim(),dynamic:c},a);i!==n&&(u.modifiers=i);var h=l[t];Array.isArray(h)?o?h.unshift(u):h.push(u):l[t]=h?o?[u,h]:[h,u]:u,e.plain=!1}function Ur(e,t){return e.rawAttrsMap[":"+t]||e.rawAttrsMap["v-bind:"+t]||e.rawAttrsMap[t]}function Wr(e,t,n){var r=Hr(e,":"+t)||Hr(e,"v-bind:"+t);if(null!=r)return Ar(r);if(!1!==n){var i=Hr(e,t);if(null!=i)return JSON.stringify(i)}}function Hr(e,t,n){var r;if(null!=(r=e.attrsMap[t]))for(var i=e.attrsList,o=0,s=i.length;o<s;o++)if(i[o].name===t){i.splice(o,1);break}return n&&delete e.attrsMap[t],r}function zr(e,t){for(var n=e.attrsList,r=0,i=n.length;r<i;r++){var o=n[r];if(t.test(o.name))return n.splice(r,1),o}}function Br(e,t){return t&&(null!=t.start&&(e.start=t.start),null!=t.end&&(e.end=t.end)),e}function Vr(e,t,n){var r=n||{},i=r.number,o="$$v";r.trim&&(o="(typeof $$v === 'string'? $$v.trim(): $$v)"),i&&(o="_n("+o+")");var s=Kr(t,o);e.model={value:"("+t+")",expression:JSON.stringify(t),callback:"function ($$v) {"+s+"}"}}function Kr(e,t){var n=function(e){if(e=e.trim(),Tr=e.length,e.indexOf("[")<0||e.lastIndexOf("]")<Tr-1)return(Or=e.lastIndexOf("."))>-1?{exp:e.slice(0,Or),key:'"'+e.slice(Or+1)+'"'}:{exp:e,key:null};kr=e,Or=Er=Ir=0;for(;!Yr();)Jr(Sr=Gr())?Xr(Sr):91===Sr&&Qr(Sr);return{exp:e.slice(0,Er),key:e.slice(Er+1,Ir)}}(e);return null===n.key?e+"="+t:"$set("+n.exp+", "+n.key+", "+t+")"}function Gr(){return kr.charCodeAt(++Or)}function Yr(){return Or>=Tr}function Jr(e){return 34===e||39===e}function Qr(e){var t=1;for(Er=Or;!Yr();)if(Jr(e=Gr()))Xr(e);else if(91===e&&t++,93===e&&t--,0===t){Ir=Or;break}}function Xr(e){for(var t=e;!Yr()&&(e=Gr())!==t;);}var Zr,ei="__r",ti="__c";function ni(e,t,n){var r=Zr;return function i(){null!==t.apply(null,arguments)&&oi(e,i,n,r)}}var ri=Ye&&!(te&&Number(te[1])<=53);function ii(e,t,n,r){if(ri){var i=hn,o=t;t=o._wrapper=function(e){if(e.target===e.currentTarget||e.timeStamp>=i||e.timeStamp<=0||e.target.ownerDocument!==document)return o.apply(this,arguments)}}Zr.addEventListener(e,t,re?{capture:n,passive:r}:n)}function oi(e,t,n,r){(r||Zr).removeEventListener(e,t._wrapper||t,n)}function si(e,t){if(!r(e.data.on)||!r(t.data.on)){var n=t.data.on||{},o=e.data.on||{};Zr=t.elm,function(e){if(i(e[ei])){var t=Q?"change":"input";e[t]=[].concat(e[ei],e[t]||[]),delete e[ei]}i(e[ti])&&(e.change=[].concat(e[ti],e.change||[]),delete e[ti])}(n),ct(n,o,ii,oi,ni,t.context),Zr=void 0}}var ai,ci={create:si,update:si};function li(e,t){if(!r(e.data.domProps)||!r(t.data.domProps)){var n,o,s=t.elm,a=e.data.domProps||{},c=t.data.domProps||{};for(n in i(c.__ob__)&&(c=t.data.domProps=x({},c)),a)n in c||(s[n]="");for(n in c){if(o=c[n],"textContent"===n||"innerHTML"===n){if(t.children&&(t.children.length=0),o===a[n])continue;1===s.childNodes.length&&s.removeChild(s.childNodes[0])}if("value"===n&&"PROGRESS"!==s.tagName){s._value=o;var l=r(o)?"":String(o);ui(s,l)&&(s.value=l)}else if("innerHTML"===n&&Xn(s.tagName)&&r(s.innerHTML)){(ai=ai||document.createElement("div")).innerHTML="<svg>"+o+"</svg>";for(var u=ai.firstChild;s.firstChild;)s.removeChild(s.firstChild);for(;u.firstChild;)s.appendChild(u.firstChild)}else if(o!==a[n])try{s[n]=o}catch(e){}}}}function ui(e,t){return!e.composing&&("OPTION"===e.tagName||function(e,t){var n=!0;try{n=document.activeElement!==e}catch(e){}return n&&e.value!==t}(e,t)||function(e,t){var n=e.value,r=e._vModifiers;if(i(r)){if(r.number)return p(n)!==p(t);if(r.trim)return n.trim()!==t.trim()}return n!==t}(e,t))}var hi={create:li,update:li},di=C(function(e){var t={},n=/:(.+)/;return e.split(/;(?![^(]*\))/g).forEach(function(e){if(e){var r=e.split(n);r.length>1&&(t[r[0].trim()]=r[1].trim())}}),t});function fi(e){var t=pi(e.style);return e.staticStyle?x(e.staticStyle,t):t}function pi(e){return Array.isArray(e)?N(e):"string"==typeof e?di(e):e}var _i,mi=/^--/,vi=/\s*!important$/,gi=function(e,t,n){if(mi.test(t))e.style.setProperty(t,n);else if(vi.test(n))e.style.setProperty(O(t),n.replace(vi,""),"important");else{var r=bi(t);if(Array.isArray(n))for(var i=0,o=n.length;i<o;i++)e.style[r]=n[i];else e.style[r]=n}},yi=["Webkit","Moz","ms"],bi=C(function(e){if(_i=_i||document.createElement("div").style,"filter"!==(e=T(e))&&e in _i)return e;for(var t=e.charAt(0).toUpperCase()+e.slice(1),n=0;n<yi.length;n++){var r=yi[n]+t;if(r in _i)return r}});function Ci(e,t){var n=t.data,o=e.data;if(!(r(n.staticStyle)&&r(n.style)&&r(o.staticStyle)&&r(o.style))){var s,a,c=t.elm,l=o.staticStyle,u=o.normalizedStyle||o.style||{},h=l||u,d=pi(t.data.style)||{};t.data.normalizedStyle=i(d.__ob__)?x({},d):d;var f=function(e,t){var n,r={};if(t)for(var i=e;i.componentInstance;)(i=i.componentInstance._vnode)&&i.data&&(n=fi(i.data))&&x(r,n);(n=fi(e.data))&&x(r,n);for(var o=e;o=o.parent;)o.data&&(n=fi(o.data))&&x(r,n);return r}(t,!0);for(a in h)r(f[a])&&gi(c,a,"");for(a in f)(s=f[a])!==h[a]&&gi(c,a,null==s?"":s)}}var wi={create:Ci,update:Ci},Ti=/\s+/;function ki(e,t){if(t&&(t=t.trim()))if(e.classList)t.indexOf(" ")>-1?t.split(Ti).forEach(function(t){return e.classList.add(t)}):e.classList.add(t);else{var n=" "+(e.getAttribute("class")||"")+" ";n.indexOf(" "+t+" ")<0&&e.setAttribute("class",(n+t).trim())}}function Si(e,t){if(t&&(t=t.trim()))if(e.classList)t.indexOf(" ")>-1?t.split(Ti).forEach(function(t){return e.classList.remove(t)}):e.classList.remove(t),e.classList.length||e.removeAttribute("class");else{for(var n=" "+(e.getAttribute("class")||"")+" ",r=" "+t+" ";n.indexOf(r)>=0;)n=n.replace(r," ");(n=n.trim())?e.setAttribute("class",n):e.removeAttribute("class")}}function Oi(e){if(e){if("object"==typeof e){var t={};return!1!==e.css&&x(t,Ei(e.name||"v")),x(t,e),t}return"string"==typeof e?Ei(e):void 0}}var Ei=C(function(e){return{enterClass:e+"-enter",enterToClass:e+"-enter-to",enterActiveClass:e+"-enter-active",leaveClass:e+"-leave",leaveToClass:e+"-leave-to",leaveActiveClass:e+"-leave-active"}}),Ii=K&&!X,xi="transition",Ni="animation",Ai="transition",Ri="transitionend",Pi="animation",Di="animationend";Ii&&(void 0===window.ontransitionend&&void 0!==window.onwebkittransitionend&&(Ai="WebkitTransition",Ri="webkitTransitionEnd"),void 0===window.onanimationend&&void 0!==window.onwebkitanimationend&&(Pi="WebkitAnimation",Di="webkitAnimationEnd"));var ji=K?window.requestAnimationFrame?window.requestAnimationFrame.bind(window):setTimeout:function(e){return e()};function $i(e){ji(function(){ji(e)})}function Mi(e,t){var n=e._transitionClasses||(e._transitionClasses=[]);n.indexOf(t)<0&&(n.push(t),ki(e,t))}function Li(e,t){e._transitionClasses&&g(e._transitionClasses,t),Si(e,t)}function Fi(e,t,n){var r=Ui(e,t),i=r.type,o=r.timeout,s=r.propCount;if(!i)return n();var a=i===xi?Ri:Di,c=0,l=function(){e.removeEventListener(a,u),n()},u=function(t){t.target===e&&++c>=s&&l()};setTimeout(function(){c<s&&l()},o+1),e.addEventListener(a,u)}var qi=/\b(transform|all)(,|$)/;function Ui(e,t){var n,r=window.getComputedStyle(e),i=(r[Ai+"Delay"]||"").split(", "),o=(r[Ai+"Duration"]||"").split(", "),s=Wi(i,o),a=(r[Pi+"Delay"]||"").split(", "),c=(r[Pi+"Duration"]||"").split(", "),l=Wi(a,c),u=0,h=0;return t===xi?s>0&&(n=xi,u=s,h=o.length):t===Ni?l>0&&(n=Ni,u=l,h=c.length):h=(n=(u=Math.max(s,l))>0?s>l?xi:Ni:null)?n===xi?o.length:c.length:0,{type:n,timeout:u,propCount:h,hasTransform:n===xi&&qi.test(r[Ai+"Property"])}}function Wi(e,t){for(;e.length<t.length;)e=e.concat(e);return Math.max.apply(null,t.map(function(t,n){return Hi(t)+Hi(e[n])}))}function Hi(e){return 1e3*Number(e.slice(0,-1).replace(",","."))}function zi(e,t){var n=e.elm;i(n._leaveCb)&&(n._leaveCb.cancelled=!0,n._leaveCb());var o=Oi(e.data.transition);if(!r(o)&&!i(n._enterCb)&&1===n.nodeType){for(var s=o.css,c=o.type,l=o.enterClass,u=o.enterToClass,h=o.enterActiveClass,d=o.appearClass,f=o.appearToClass,_=o.appearActiveClass,m=o.beforeEnter,v=o.enter,g=o.afterEnter,y=o.enterCancelled,b=o.beforeAppear,C=o.appear,w=o.afterAppear,T=o.appearCancelled,k=o.duration,S=Zt,O=Zt.$vnode;O&&O.parent;)S=O.context,O=O.parent;var E=!S._isMounted||!e.isRootInsert;if(!E||C||""===C){var I=E&&d?d:l,x=E&&_?_:h,N=E&&f?f:u,A=E&&b||m,R=E&&"function"==typeof C?C:v,P=E&&w||g,D=E&&T||y,j=p(a(k)?k.enter:k);0;var M=!1!==s&&!X,L=Ki(R),F=n._enterCb=$(function(){M&&(Li(n,N),Li(n,x)),F.cancelled?(M&&Li(n,I),D&&D(n)):P&&P(n),n._enterCb=null});e.data.show||lt(e,"insert",function(){var t=n.parentNode,r=t&&t._pending&&t._pending[e.key];r&&r.tag===e.tag&&r.elm._leaveCb&&r.elm._leaveCb(),R&&R(n,F)}),A&&A(n),M&&(Mi(n,I),Mi(n,x),$i(function(){Li(n,I),F.cancelled||(Mi(n,N),L||(Vi(j)?setTimeout(F,j):Fi(n,c,F)))})),e.data.show&&(t&&t(),R&&R(n,F)),M||L||F()}}}function Bi(e,t){var n=e.elm;i(n._enterCb)&&(n._enterCb.cancelled=!0,n._enterCb());var o=Oi(e.data.transition);if(r(o)||1!==n.nodeType)return t();if(!i(n._leaveCb)){var s=o.css,c=o.type,l=o.leaveClass,u=o.leaveToClass,h=o.leaveActiveClass,d=o.beforeLeave,f=o.leave,_=o.afterLeave,m=o.leaveCancelled,v=o.delayLeave,g=o.duration,y=!1!==s&&!X,b=Ki(f),C=p(a(g)?g.leave:g);0;var w=n._leaveCb=$(function(){n.parentNode&&n.parentNode._pending&&(n.parentNode._pending[e.key]=null),y&&(Li(n,u),Li(n,h)),w.cancelled?(y&&Li(n,l),m&&m(n)):(t(),_&&_(n)),n._leaveCb=null});v?v(T):T()}function T(){w.cancelled||(!e.data.show&&n.parentNode&&((n.parentNode._pending||(n.parentNode._pending={}))[e.key]=e),d&&d(n),y&&(Mi(n,l),Mi(n,h),$i(function(){Li(n,l),w.cancelled||(Mi(n,u),b||(Vi(C)?setTimeout(w,C):Fi(n,c,w)))})),f&&f(n,w),y||b||w())}}function Vi(e){return"number"==typeof e&&!isNaN(e)}function Ki(e){if(r(e))return!1;var t=e.fns;return i(t)?Ki(Array.isArray(t)?t[0]:t):(e._length||e.length)>1}function Gi(e,t){!0!==t.data.show&&zi(t)}var Yi=function(e){var t,n,a={},c=e.modules,l=e.nodeOps;for(t=0;t<cr.length;++t)for(a[cr[t]]=[],n=0;n<c.length;++n)i(c[n][cr[t]])&&a[cr[t]].push(c[n][cr[t]]);function u(e){var t=l.parentNode(e);i(t)&&l.removeChild(t,e)}function h(e,t,n,r,s,c,u){if(i(e.elm)&&i(c)&&(e=c[u]=be(e)),e.isRootInsert=!s,!function(e,t,n,r){var s=e.data;if(i(s)){var c=i(e.componentInstance)&&s.keepAlive;if(i(s=s.hook)&&i(s=s.init)&&s(e,!1),i(e.componentInstance))return d(e,t),f(n,e.elm,r),o(c)&&function(e,t,n,r){for(var o,s=e;s.componentInstance;)if(s=s.componentInstance._vnode,i(o=s.data)&&i(o=o.transition)){for(o=0;o<a.activate.length;++o)a.activate[o](ar,s);t.push(s);break}f(n,e.elm,r)}(e,t,n,r),!0}}(e,t,n,r)){var h=e.data,_=e.children,m=e.tag;i(m)?(e.elm=e.ns?l.createElementNS(e.ns,m):l.createElement(m,e),g(e),p(e,_,t),i(h)&&v(e,t),f(n,e.elm,r)):o(e.isComment)?(e.elm=l.createComment(e.text),f(n,e.elm,r)):(e.elm=l.createTextNode(e.text),f(n,e.elm,r))}}function d(e,t){i(e.data.pendingInsert)&&(t.push.apply(t,e.data.pendingInsert),e.data.pendingInsert=null),e.elm=e.componentInstance.$el,m(e)?(v(e,t),g(e)):(sr(e),t.push(e))}function f(e,t,n){i(e)&&(i(n)?l.parentNode(n)===e&&l.insertBefore(e,t,n):l.appendChild(e,t))}function p(e,t,n){if(Array.isArray(t))for(var r=0;r<t.length;++r)h(t[r],n,e.elm,null,!0,t,r);else s(e.text)&&l.appendChild(e.elm,l.createTextNode(String(e.text)))}function m(e){for(;e.componentInstance;)e=e.componentInstance._vnode;return i(e.tag)}function v(e,n){for(var r=0;r<a.create.length;++r)a.create[r](ar,e);i(t=e.data.hook)&&(i(t.create)&&t.create(ar,e),i(t.insert)&&n.push(e))}function g(e){var t;if(i(t=e.fnScopeId))l.setStyleScope(e.elm,t);else for(var n=e;n;)i(t=n.context)&&i(t=t.$options._scopeId)&&l.setStyleScope(e.elm,t),n=n.parent;i(t=Zt)&&t!==e.context&&t!==e.fnContext&&i(t=t.$options._scopeId)&&l.setStyleScope(e.elm,t)}function y(e,t,n,r,i,o){for(;r<=i;++r)h(n[r],o,e,t,!1,n,r)}function b(e){var t,n,r=e.data;if(i(r))for(i(t=r.hook)&&i(t=t.destroy)&&t(e),t=0;t<a.destroy.length;++t)a.destroy[t](e);if(i(t=e.children))for(n=0;n<e.children.length;++n)b(e.children[n])}function C(e,t,n){for(;t<=n;++t){var r=e[t];i(r)&&(i(r.tag)?(w(r),b(r)):u(r.elm))}}function w(e,t){if(i(t)||i(e.data)){var n,r=a.remove.length+1;for(i(t)?t.listeners+=r:t=function(e,t){function n(){0==--n.listeners&&u(e)}return n.listeners=t,n}(e.elm,r),i(n=e.componentInstance)&&i(n=n._vnode)&&i(n.data)&&w(n,t),n=0;n<a.remove.length;++n)a.remove[n](e,t);i(n=e.data.hook)&&i(n=n.remove)?n(e,t):t()}else u(e.elm)}function T(e,t,n,r){for(var o=n;o<r;o++){var s=t[o];if(i(s)&&lr(e,s))return o}}function k(e,t,n,s,c,u){if(e!==t){i(t.elm)&&i(s)&&(t=s[c]=be(t));var d=t.elm=e.elm;if(o(e.isAsyncPlaceholder))i(t.asyncFactory.resolved)?E(e.elm,t,n):t.isAsyncPlaceholder=!0;else if(o(t.isStatic)&&o(e.isStatic)&&t.key===e.key&&(o(t.isCloned)||o(t.isOnce)))t.componentInstance=e.componentInstance;else{var f,p=t.data;i(p)&&i(f=p.hook)&&i(f=f.prepatch)&&f(e,t);var _=e.children,v=t.children;if(i(p)&&m(t)){for(f=0;f<a.update.length;++f)a.update[f](e,t);i(f=p.hook)&&i(f=f.update)&&f(e,t)}r(t.text)?i(_)&&i(v)?_!==v&&function(e,t,n,o,s){for(var a,c,u,d=0,f=0,p=t.length-1,_=t[0],m=t[p],v=n.length-1,g=n[0],b=n[v],w=!s;d<=p&&f<=v;)r(_)?_=t[++d]:r(m)?m=t[--p]:lr(_,g)?(k(_,g,o,n,f),_=t[++d],g=n[++f]):lr(m,b)?(k(m,b,o,n,v),m=t[--p],b=n[--v]):lr(_,b)?(k(_,b,o,n,v),w&&l.insertBefore(e,_.elm,l.nextSibling(m.elm)),_=t[++d],b=n[--v]):lr(m,g)?(k(m,g,o,n,f),w&&l.insertBefore(e,m.elm,_.elm),m=t[--p],g=n[++f]):(r(a)&&(a=ur(t,d,p)),r(c=i(g.key)?a[g.key]:T(g,t,d,p))?h(g,o,e,_.elm,!1,n,f):lr(u=t[c],g)?(k(u,g,o,n,f),t[c]=void 0,w&&l.insertBefore(e,u.elm,_.elm)):h(g,o,e,_.elm,!1,n,f),g=n[++f]);d>p?y(e,r(n[v+1])?null:n[v+1].elm,n,f,v,o):f>v&&C(t,d,p)}(d,_,v,n,u):i(v)?(i(e.text)&&l.setTextContent(d,""),y(d,null,v,0,v.length-1,n)):i(_)?C(_,0,_.length-1):i(e.text)&&l.setTextContent(d,""):e.text!==t.text&&l.setTextContent(d,t.text),i(p)&&i(f=p.hook)&&i(f=f.postpatch)&&f(e,t)}}}function S(e,t,n){if(o(n)&&i(e.parent))e.parent.data.pendingInsert=t;else for(var r=0;r<t.length;++r)t[r].data.hook.insert(t[r])}var O=_("attrs,class,staticClass,staticStyle,key");function E(e,t,n,r){var s,a=t.tag,c=t.data,l=t.children;if(r=r||c&&c.pre,t.elm=e,o(t.isComment)&&i(t.asyncFactory))return t.isAsyncPlaceholder=!0,!0;if(i(c)&&(i(s=c.hook)&&i(s=s.init)&&s(t,!0),i(s=t.componentInstance)))return d(t,n),!0;if(i(a)){if(i(l))if(e.hasChildNodes())if(i(s=c)&&i(s=s.domProps)&&i(s=s.innerHTML)){if(s!==e.innerHTML)return!1}else{for(var u=!0,h=e.firstChild,f=0;f<l.length;f++){if(!h||!E(h,l[f],n,r)){u=!1;break}h=h.nextSibling}if(!u||h)return!1}else p(t,l,n);if(i(c)){var _=!1;for(var m in c)if(!O(m)){_=!0,v(t,n);break}!_&&c.class&&ot(c.class)}}else e.data!==t.text&&(e.data=t.text);return!0}return function(e,t,n,s){if(!r(t)){var c,u=!1,d=[];if(r(e))u=!0,h(t,d);else{var f=i(e.nodeType);if(!f&&lr(e,t))k(e,t,d,null,null,s);else{if(f){if(1===e.nodeType&&e.hasAttribute(M)&&(e.removeAttribute(M),n=!0),o(n)&&E(e,t,d))return S(t,d,!0),e;c=e,e=new me(l.tagName(c).toLowerCase(),{},[],void 0,c)}var p=e.elm,_=l.parentNode(p);if(h(t,d,p._leaveCb?null:_,l.nextSibling(p)),i(t.parent))for(var v=t.parent,g=m(t);v;){for(var y=0;y<a.destroy.length;++y)a.destroy[y](v);if(v.elm=t.elm,g){for(var w=0;w<a.create.length;++w)a.create[w](ar,v);var T=v.data.hook.insert;if(T.merged)for(var O=1;O<T.fns.length;O++)T.fns[O]()}else sr(v);v=v.parent}i(_)?C([e],0,0):i(e.tag)&&b(e)}}return S(t,d,u),t.elm}i(e)&&b(e)}}({nodeOps:ir,modules:[Cr,xr,ci,hi,wi,K?{create:Gi,activate:Gi,remove:function(e,t){!0!==e.data.show?Bi(e,t):t()}}:{}].concat(vr)});X&&document.addEventListener("selectionchange",function(){var e=document.activeElement;e&&e.vmodel&&ro(e,"input")});var Ji={inserted:function(e,t,n,r){"select"===n.tag?(r.elm&&!r.elm._vOptions?lt(n,"postpatch",function(){Ji.componentUpdated(e,t,n)}):Qi(e,t,n.context),e._vOptions=[].map.call(e.options,eo)):("textarea"===n.tag||nr(e.type))&&(e._vModifiers=t.modifiers,t.modifiers.lazy||(e.addEventListener("compositionstart",to),e.addEventListener("compositionend",no),e.addEventListener("change",no),X&&(e.vmodel=!0)))},componentUpdated:function(e,t,n){if("select"===n.tag){Qi(e,t,n.context);var r=e._vOptions,i=e._vOptions=[].map.call(e.options,eo);if(i.some(function(e,t){return!D(e,r[t])}))(e.multiple?t.value.some(function(e){return Zi(e,i)}):t.value!==t.oldValue&&Zi(t.value,i))&&ro(e,"change")}}};function Qi(e,t,n){Xi(e,t,n),(Q||Z)&&setTimeout(function(){Xi(e,t,n)},0)}function Xi(e,t,n){var r=t.value,i=e.multiple;if(!i||Array.isArray(r)){for(var o,s,a=0,c=e.options.length;a<c;a++)if(s=e.options[a],i)o=j(r,eo(s))>-1,s.selected!==o&&(s.selected=o);else if(D(eo(s),r))return void(e.selectedIndex!==a&&(e.selectedIndex=a));i||(e.selectedIndex=-1)}}function Zi(e,t){return t.every(function(t){return!D(t,e)})}function eo(e){return"_value"in e?e._value:e.value}function to(e){e.target.composing=!0}function no(e){e.target.composing&&(e.target.composing=!1,ro(e.target,"input"))}function ro(e,t){var n=document.createEvent("HTMLEvents");n.initEvent(t,!0,!0),e.dispatchEvent(n)}function io(e){return!e.componentInstance||e.data&&e.data.transition?e:io(e.componentInstance._vnode)}var oo={model:Ji,show:{bind:function(e,t,n){var r=t.value,i=(n=io(n)).data&&n.data.transition,o=e.__vOriginalDisplay="none"===e.style.display?"":e.style.display;r&&i?(n.data.show=!0,zi(n,function(){e.style.display=o})):e.style.display=r?o:"none"},update:function(e,t,n){var r=t.value;!r!=!t.oldValue&&((n=io(n)).data&&n.data.transition?(n.data.show=!0,r?zi(n,function(){e.style.display=e.__vOriginalDisplay}):Bi(n,function(){e.style.display="none"})):e.style.display=r?e.__vOriginalDisplay:"none")},unbind:function(e,t,n,r,i){i||(e.style.display=e.__vOriginalDisplay)}}},so={name:String,appear:Boolean,css:Boolean,mode:String,type:String,enterClass:String,leaveClass:String,enterToClass:String,leaveToClass:String,enterActiveClass:String,leaveActiveClass:String,appearClass:String,appearActiveClass:String,appearToClass:String,duration:[Number,String,Object]};function ao(e){var t=e&&e.componentOptions;return t&&t.Ctor.options.abstract?ao(Gt(t.children)):e}function co(e){var t={},n=e.$options;for(var r in n.propsData)t[r]=e[r];var i=n._parentListeners;for(var o in i)t[T(o)]=i[o];return t}function lo(e,t){if(/\d-keep-alive$/.test(t.tag))return e("keep-alive",{props:t.componentOptions.propsData})}var uo=function(e){return e.tag||mt(e)},ho=function(e){return"show"===e.name},fo={name:"transition",props:so,abstract:!0,render:function(e){var t=this,n=this.$slots.default;if(n&&(n=n.filter(uo)).length){0;var r=this.mode;0;var i=n[0];if(function(e){for(;e=e.parent;)if(e.data.transition)return!0}(this.$vnode))return i;var o=ao(i);if(!o)return i;if(this._leaving)return lo(e,i);var a="__transition-"+this._uid+"-";o.key=null==o.key?o.isComment?a+"comment":a+o.tag:s(o.key)?0===String(o.key).indexOf(a)?o.key:a+o.key:o.key;var c=(o.data||(o.data={})).transition=co(this),l=this._vnode,u=ao(l);if(o.data.directives&&o.data.directives.some(ho)&&(o.data.show=!0),u&&u.data&&!function(e,t){return t.key===e.key&&t.tag===e.tag}(o,u)&&!mt(u)&&(!u.componentInstance||!u.componentInstance._vnode.isComment)){var h=u.data.transition=x({},c);if("out-in"===r)return this._leaving=!0,lt(h,"afterLeave",function(){t._leaving=!1,t.$forceUpdate()}),lo(e,i);if("in-out"===r){if(mt(o))return l;var d,f=function(){d()};lt(c,"afterEnter",f),lt(c,"enterCancelled",f),lt(h,"delayLeave",function(e){d=e})}}return i}}},po=x({tag:String,moveClass:String},so);function _o(e){e.elm._moveCb&&e.elm._moveCb(),e.elm._enterCb&&e.elm._enterCb()}function mo(e){e.data.newPos=e.elm.getBoundingClientRect()}function vo(e){var t=e.data.pos,n=e.data.newPos,r=t.left-n.left,i=t.top-n.top;if(r||i){e.data.moved=!0;var o=e.elm.style;o.transform=o.WebkitTransform="translate("+r+"px,"+i+"px)",o.transitionDuration="0s"}}delete po.mode;var go={Transition:fo,TransitionGroup:{props:po,beforeMount:function(){var e=this,t=this._update;this._update=function(n,r){var i=en(e);e.__patch__(e._vnode,e.kept,!1,!0),e._vnode=e.kept,i(),t.call(e,n,r)}},render:function(e){for(var t=this.tag||this.$vnode.data.tag||"span",n=Object.create(null),r=this.prevChildren=this.children,i=this.$slots.default||[],o=this.children=[],s=co(this),a=0;a<i.length;a++){var c=i[a];if(c.tag)if(null!=c.key&&0!==String(c.key).indexOf("__vlist"))o.push(c),n[c.key]=c,(c.data||(c.data={})).transition=s;else;}if(r){for(var l=[],u=[],h=0;h<r.length;h++){var d=r[h];d.data.transition=s,d.data.pos=d.elm.getBoundingClientRect(),n[d.key]?l.push(d):u.push(d)}this.kept=e(t,null,l),this.removed=u}return e(t,null,o)},updated:function(){var e=this.prevChildren,t=this.moveClass||(this.name||"v")+"-move";e.length&&this.hasMove(e[0].elm,t)&&(e.forEach(_o),e.forEach(mo),e.forEach(vo),this._reflow=document.body.offsetHeight,e.forEach(function(e){if(e.data.moved){var n=e.elm,r=n.style;Mi(n,t),r.transform=r.WebkitTransform=r.transitionDuration="",n.addEventListener(Ri,n._moveCb=function e(r){r&&r.target!==n||r&&!/transform$/.test(r.propertyName)||(n.removeEventListener(Ri,e),n._moveCb=null,Li(n,t))})}}))},methods:{hasMove:function(e,t){if(!Ii)return!1;if(this._hasMove)return this._hasMove;var n=e.cloneNode();e._transitionClasses&&e._transitionClasses.forEach(function(e){Si(n,e)}),ki(n,t),n.style.display="none",this.$el.appendChild(n);var r=Ui(n);return this.$el.removeChild(n),this._hasMove=r.hasTransform}}}};En.config.mustUseProp=Mn,En.config.isReservedTag=Zn,En.config.isReservedAttr=jn,En.config.getTagNamespace=er,En.config.isUnknownElement=function(e){if(!K)return!0;if(Zn(e))return!1;if(e=e.toLowerCase(),null!=tr[e])return tr[e];var t=document.createElement(e);return e.indexOf("-")>-1?tr[e]=t.constructor===window.HTMLUnknownElement||t.constructor===window.HTMLElement:tr[e]=/HTMLUnknownElement/.test(t.toString())},x(En.options.directives,oo),x(En.options.components,go),En.prototype.__patch__=K?Yi:A,En.prototype.$mount=function(e,t){return function(e,t,n){return e.$el=t,e.$options.render||(e.$options.render=ge),rn(e,"beforeMount"),new mn(e,function(){e._update(e._render(),n)},A,{before:function(){e._isMounted&&!e._isDestroyed&&rn(e,"beforeUpdate")}},!0),n=!1,null==e.$vnode&&(e._isMounted=!0,rn(e,"mounted")),e}(this,e=e&&K?rr(e):void 0,t)},K&&setTimeout(function(){q.devtools&&se&&se.emit("init",En)},0);var yo=/\{\{((?:.|\r?\n)+?)\}\}/g,bo=/[-.*+?^${}()|[\]\/\\]/g,Co=C(function(e){var t=e[0].replace(bo,"\\$&"),n=e[1].replace(bo,"\\$&");return new RegExp(t+"((?:.|\\n)+?)"+n,"g")});function wo(e,t){var n=t?Co(t):yo;if(n.test(e)){for(var r,i,o,s=[],a=[],c=n.lastIndex=0;r=n.exec(e);){(i=r.index)>c&&(a.push(o=e.slice(c,i)),s.push(JSON.stringify(o)));var l=Ar(r[1].trim());s.push("_s("+l+")"),a.push({"@binding":l}),c=i+r[0].length}return c<e.length&&(a.push(o=e.slice(c)),s.push(JSON.stringify(o))),{expression:s.join("+"),tokens:a}}}var To={staticKeys:["staticClass"],transformNode:function(e,t){t.warn;var n=Hr(e,"class");n&&(e.staticClass=JSON.stringify(n));var r=Wr(e,"class",!1);r&&(e.classBinding=r)},genData:function(e){var t="";return e.staticClass&&(t+="staticClass:"+e.staticClass+","),e.classBinding&&(t+="class:"+e.classBinding+","),t}};var ko,So={staticKeys:["staticStyle"],transformNode:function(e,t){t.warn;var n=Hr(e,"style");n&&(e.staticStyle=JSON.stringify(di(n)));var r=Wr(e,"style",!1);r&&(e.styleBinding=r)},genData:function(e){var t="";return e.staticStyle&&(t+="staticStyle:"+e.staticStyle+","),e.styleBinding&&(t+="style:("+e.styleBinding+"),"),t}},Oo=function(e){return(ko=ko||document.createElement("div")).innerHTML=e,ko.textContent},Eo=_("area,base,br,col,embed,frame,hr,img,input,isindex,keygen,link,meta,param,source,track,wbr"),Io=_("colgroup,dd,dt,li,options,p,td,tfoot,th,thead,tr,source"),xo=_("address,article,aside,base,blockquote,body,caption,col,colgroup,dd,details,dialog,div,dl,dt,fieldset,figcaption,figure,footer,form,h1,h2,h3,h4,h5,h6,head,header,hgroup,hr,html,legend,li,menuitem,meta,optgroup,option,param,rp,rt,source,style,summary,tbody,td,tfoot,th,thead,title,tr,track"),No=/^\s*([^\s"'<>\/=]+)(?:\s*(=)\s*(?:"([^"]*)"+|'([^']*)'+|([^\s"'=<>`]+)))?/,Ao=/^\s*((?:v-[\w-]+:|@|:|#)\[[^=]+?\][^\s"'<>\/=]*)(?:\s*(=)\s*(?:"([^"]*)"+|'([^']*)'+|([^\s"'=<>`]+)))?/,Ro="[a-zA-Z_][\\-\\.0-9_a-zA-Z"+U.source+"]*",Po="((?:"+Ro+"\\:)?"+Ro+")",Do=new RegExp("^<"+Po),jo=/^\s*(\/?)>/,$o=new RegExp("^<\\/"+Po+"[^>]*>"),Mo=/^<!DOCTYPE [^>]+>/i,Lo=/^<!\--/,Fo=/^<!\[/,qo=_("script,style,textarea",!0),Uo={},Wo={"&lt;":"<","&gt;":">","&quot;":'"',"&amp;":"&","&#10;":"\n","&#9;":"\t","&#39;":"'"},Ho=/&(?:lt|gt|quot|amp|#39);/g,zo=/&(?:lt|gt|quot|amp|#39|#10|#9);/g,Bo=_("pre,textarea",!0),Vo=function(e,t){return e&&Bo(e)&&"\n"===t[0]};function Ko(e,t){var n=t?zo:Ho;return e.replace(n,function(e){return Wo[e]})}var Go,Yo,Jo,Qo,Xo,Zo,es,ts,ns=/^@|^v-on:/,rs=/^v-|^@|^:|^#/,is=/([\s\S]*?)\s+(?:in|of)\s+([\s\S]*)/,os=/,([^,\}\]]*)(?:,([^,\}\]]*))?$/,ss=/^\(|\)$/g,as=/^\[.*\]$/,cs=/:(.*)$/,ls=/^:|^\.|^v-bind:/,us=/\.[^.\]]+(?=[^\]]*$)/g,hs=/^v-slot(:|$)|^#/,ds=/[\r\n]/,fs=/[ \f\t\r\n]+/g,ps=C(Oo),_s="_empty_";function ms(e,t,n){return{type:1,tag:e,attrsList:t,attrsMap:function(e){for(var t={},n=0,r=e.length;n<r;n++)t[e[n].name]=e[n].value;return t}(t),rawAttrsMap:{},parent:n,children:[]}}function vs(e,t){Go=t.warn||Pr,Zo=t.isPreTag||R,es=t.mustUseProp||R,ts=t.getTagNamespace||R;var n=t.isReservedTag||R;(function(e){return!(!(e.component||e.attrsMap[":is"]||e.attrsMap["v-bind:is"])&&(e.attrsMap.is?n(e.attrsMap.is):n(e.tag)))}),Jo=Dr(t.modules,"transformNode"),Qo=Dr(t.modules,"preTransformNode"),Xo=Dr(t.modules,"postTransformNode"),Yo=t.delimiters;var r,i,o=[],s=!1!==t.preserveWhitespace,a=t.whitespace,c=!1,l=!1;function u(e){if(h(e),c||e.processed||(e=gs(e,t)),o.length||e===r||r.if&&(e.elseif||e.else)&&bs(r,{exp:e.elseif,block:e}),i&&!e.forbidden)if(e.elseif||e.else)s=e,(a=function(e){var t=e.length;for(;t--;){if(1===e[t].type)return e[t];e.pop()}}(i.children))&&a.if&&bs(a,{exp:s.elseif,block:s});else{if(e.slotScope){var n=e.slotTarget||'"default"';(i.scopedSlots||(i.scopedSlots={}))[n]=e}i.children.push(e),e.parent=i}var s,a;e.children=e.children.filter(function(e){return!e.slotScope}),h(e),e.pre&&(c=!1),Zo(e.tag)&&(l=!1);for(var u=0;u<Xo.length;u++)Xo[u](e,t)}function h(e){if(!l)for(var t;(t=e.children[e.children.length-1])&&3===t.type&&" "===t.text;)e.children.pop()}return function(e,t){for(var n,r,i=[],o=t.expectHTML,s=t.isUnaryTag||R,a=t.canBeLeftOpenTag||R,c=0;e;){if(n=e,r&&qo(r)){var l=0,u=r.toLowerCase(),h=Uo[u]||(Uo[u]=new RegExp("([\\s\\S]*?)(</"+u+"[^>]*>)","i")),d=e.replace(h,function(e,n,r){return l=r.length,qo(u)||"noscript"===u||(n=n.replace(/<!\--([\s\S]*?)-->/g,"$1").replace(/<!\[CDATA\[([\s\S]*?)]]>/g,"$1")),Vo(u,n)&&(n=n.slice(1)),t.chars&&t.chars(n),""});c+=e.length-d.length,e=d,O(u,c-l,c)}else{var f=e.indexOf("<");if(0===f){if(Lo.test(e)){var p=e.indexOf("--\x3e");if(p>=0){t.shouldKeepComment&&t.comment(e.substring(4,p),c,c+p+3),T(p+3);continue}}if(Fo.test(e)){var _=e.indexOf("]>");if(_>=0){T(_+2);continue}}var m=e.match(Mo);if(m){T(m[0].length);continue}var v=e.match($o);if(v){var g=c;T(v[0].length),O(v[1],g,c);continue}var y=k();if(y){S(y),Vo(y.tagName,e)&&T(1);continue}}var b=void 0,C=void 0,w=void 0;if(f>=0){for(C=e.slice(f);!($o.test(C)||Do.test(C)||Lo.test(C)||Fo.test(C)||(w=C.indexOf("<",1))<0);)f+=w,C=e.slice(f);b=e.substring(0,f)}f<0&&(b=e),b&&T(b.length),t.chars&&b&&t.chars(b,c-b.length,c)}if(e===n){t.chars&&t.chars(e);break}}function T(t){c+=t,e=e.substring(t)}function k(){var t=e.match(Do);if(t){var n,r,i={tagName:t[1],attrs:[],start:c};for(T(t[0].length);!(n=e.match(jo))&&(r=e.match(Ao)||e.match(No));)r.start=c,T(r[0].length),r.end=c,i.attrs.push(r);if(n)return i.unarySlash=n[1],T(n[0].length),i.end=c,i}}function S(e){var n=e.tagName,c=e.unarySlash;o&&("p"===r&&xo(n)&&O(r),a(n)&&r===n&&O(n));for(var l=s(n)||!!c,u=e.attrs.length,h=new Array(u),d=0;d<u;d++){var f=e.attrs[d],p=f[3]||f[4]||f[5]||"",_="a"===n&&"href"===f[1]?t.shouldDecodeNewlinesForHref:t.shouldDecodeNewlines;h[d]={name:f[1],value:Ko(p,_)}}l||(i.push({tag:n,lowerCasedTag:n.toLowerCase(),attrs:h,start:e.start,end:e.end}),r=n),t.start&&t.start(n,h,l,e.start,e.end)}function O(e,n,o){var s,a;if(null==n&&(n=c),null==o&&(o=c),e)for(a=e.toLowerCase(),s=i.length-1;s>=0&&i[s].lowerCasedTag!==a;s--);else s=0;if(s>=0){for(var l=i.length-1;l>=s;l--)t.end&&t.end(i[l].tag,n,o);i.length=s,r=s&&i[s-1].tag}else"br"===a?t.start&&t.start(e,[],!0,n,o):"p"===a&&(t.start&&t.start(e,[],!1,n,o),t.end&&t.end(e,n,o))}O()}(e,{warn:Go,expectHTML:t.expectHTML,isUnaryTag:t.isUnaryTag,canBeLeftOpenTag:t.canBeLeftOpenTag,shouldDecodeNewlines:t.shouldDecodeNewlines,shouldDecodeNewlinesForHref:t.shouldDecodeNewlinesForHref,shouldKeepComment:t.comments,outputSourceRange:t.outputSourceRange,start:function(e,n,s,a,h){var d=i&&i.ns||ts(e);Q&&"svg"===d&&(n=function(e){for(var t=[],n=0;n<e.length;n++){var r=e[n];Ts.test(r.name)||(r.name=r.name.replace(ks,""),t.push(r))}return t}(n));var f,p=ms(e,n,i);d&&(p.ns=d),"style"!==(f=p).tag&&("script"!==f.tag||f.attrsMap.type&&"text/javascript"!==f.attrsMap.type)||oe()||(p.forbidden=!0);for(var _=0;_<Qo.length;_++)p=Qo[_](p,t)||p;c||(!function(e){null!=Hr(e,"v-pre")&&(e.pre=!0)}(p),p.pre&&(c=!0)),Zo(p.tag)&&(l=!0),c?function(e){var t=e.attrsList,n=t.length;if(n)for(var r=e.attrs=new Array(n),i=0;i<n;i++)r[i]={name:t[i].name,value:JSON.stringify(t[i].value)},null!=t[i].start&&(r[i].start=t[i].start,r[i].end=t[i].end);else e.pre||(e.plain=!0)}(p):p.processed||(ys(p),function(e){var t=Hr(e,"v-if");if(t)e.if=t,bs(e,{exp:t,block:e});else{null!=Hr(e,"v-else")&&(e.else=!0);var n=Hr(e,"v-else-if");n&&(e.elseif=n)}}(p),function(e){null!=Hr(e,"v-once")&&(e.once=!0)}(p)),r||(r=p),s?u(p):(i=p,o.push(p))},end:function(e,t,n){var r=o[o.length-1];o.length-=1,i=o[o.length-1],u(r)},chars:function(e,t,n){if(i&&(!Q||"textarea"!==i.tag||i.attrsMap.placeholder!==e)){var r,o,u,h=i.children;if(e=l||e.trim()?"script"===(r=i).tag||"style"===r.tag?e:ps(e):h.length?a?"condense"===a&&ds.test(e)?"":" ":s?" ":"":"")l||"condense"!==a||(e=e.replace(fs," ")),!c&&" "!==e&&(o=wo(e,Yo))?u={type:2,expression:o.expression,tokens:o.tokens,text:e}:" "===e&&h.length&&" "===h[h.length-1].text||(u={type:3,text:e}),u&&h.push(u)}},comment:function(e,t,n){if(i){var r={type:3,text:e,isComment:!0};0,i.children.push(r)}}}),r}function gs(e,t){var n,r;!function(e){var t=Wr(e,"key");if(t){e.key=t}}(e),e.plain=!e.key&&!e.scopedSlots&&!e.attrsList.length,(r=Wr(n=e,"ref"))&&(n.ref=r,n.refInFor=function(e){for(var t=e;t;){if(void 0!==t.for)return!0;t=t.parent}return!1}(n)),function(e){var t;"template"===e.tag?(t=Hr(e,"scope"),e.slotScope=t||Hr(e,"slot-scope")):(t=Hr(e,"slot-scope"))&&(e.slotScope=t);var n=Wr(e,"slot");n&&(e.slotTarget='""'===n?'"default"':n,e.slotTargetDynamic=!(!e.attrsMap[":slot"]&&!e.attrsMap["v-bind:slot"]),"template"===e.tag||e.slotScope||$r(e,"slot",n,Ur(e,"slot")));if("template"===e.tag){var r=zr(e,hs);if(r){0;var i=Cs(r),o=i.name,s=i.dynamic;e.slotTarget=o,e.slotTargetDynamic=s,e.slotScope=r.value||_s}}else{var a=zr(e,hs);if(a){0;var c=e.scopedSlots||(e.scopedSlots={}),l=Cs(a),u=l.name,h=l.dynamic,d=c[u]=ms("template",[],e);d.slotTarget=u,d.slotTargetDynamic=h,d.children=e.children.filter(function(e){if(!e.slotScope)return e.parent=d,!0}),d.slotScope=a.value||_s,e.children=[],e.plain=!1}}}(e),function(e){"slot"===e.tag&&(e.slotName=Wr(e,"name"))}(e),function(e){var t;(t=Wr(e,"is"))&&(e.component=t);null!=Hr(e,"inline-template")&&(e.inlineTemplate=!0)}(e);for(var i=0;i<Jo.length;i++)e=Jo[i](e,t)||e;return function(e){var t,n,r,i,o,s,a,c,l=e.attrsList;for(t=0,n=l.length;t<n;t++){if(r=i=l[t].name,o=l[t].value,rs.test(r))if(e.hasBindings=!0,(s=ws(r.replace(rs,"")))&&(r=r.replace(us,"")),ls.test(r))r=r.replace(ls,""),o=Ar(o),(c=as.test(r))&&(r=r.slice(1,-1)),s&&(s.prop&&!c&&"innerHtml"===(r=T(r))&&(r="innerHTML"),s.camel&&!c&&(r=T(r)),s.sync&&(a=Kr(o,"$event"),c?qr(e,'"update:"+('+r+")",a,null,!1,0,l[t],!0):(qr(e,"update:"+T(r),a,null,!1,0,l[t]),O(r)!==T(r)&&qr(e,"update:"+O(r),a,null,!1,0,l[t])))),s&&s.prop||!e.component&&es(e.tag,e.attrsMap.type,r)?jr(e,r,o,l[t],c):$r(e,r,o,l[t],c);else if(ns.test(r))r=r.replace(ns,""),(c=as.test(r))&&(r=r.slice(1,-1)),qr(e,r,o,s,!1,0,l[t],c);else{var u=(r=r.replace(rs,"")).match(cs),h=u&&u[1];c=!1,h&&(r=r.slice(0,-(h.length+1)),as.test(h)&&(h=h.slice(1,-1),c=!0)),Lr(e,r,i,o,h,c,s,l[t])}else $r(e,r,JSON.stringify(o),l[t]),!e.component&&"muted"===r&&es(e.tag,e.attrsMap.type,r)&&jr(e,r,"true",l[t])}}(e),e}function ys(e){var t;if(t=Hr(e,"v-for")){var n=function(e){var t=e.match(is);if(!t)return;var n={};n.for=t[2].trim();var r=t[1].trim().replace(ss,""),i=r.match(os);i?(n.alias=r.replace(os,"").trim(),n.iterator1=i[1].trim(),i[2]&&(n.iterator2=i[2].trim())):n.alias=r;return n}(t);n&&x(e,n)}}function bs(e,t){e.ifConditions||(e.ifConditions=[]),e.ifConditions.push(t)}function Cs(e){var t=e.name.replace(hs,"");return t||"#"!==e.name[0]&&(t="default"),as.test(t)?{name:t.slice(1,-1),dynamic:!0}:{name:'"'+t+'"',dynamic:!1}}function ws(e){var t=e.match(us);if(t){var n={};return t.forEach(function(e){n[e.slice(1)]=!0}),n}}var Ts=/^xmlns:NS\d+/,ks=/^NS\d+:/;function Ss(e){return ms(e.tag,e.attrsList.slice(),e.parent)}var Os=[To,So,{preTransformNode:function(e,t){if("input"===e.tag){var n,r=e.attrsMap;if(!r["v-model"])return;if((r[":type"]||r["v-bind:type"])&&(n=Wr(e,"type")),r.type||n||!r["v-bind"]||(n="("+r["v-bind"]+").type"),n){var i=Hr(e,"v-if",!0),o=i?"&&("+i+")":"",s=null!=Hr(e,"v-else",!0),a=Hr(e,"v-else-if",!0),c=Ss(e);ys(c),Mr(c,"type","checkbox"),gs(c,t),c.processed=!0,c.if="("+n+")==='checkbox'"+o,bs(c,{exp:c.if,block:c});var l=Ss(e);Hr(l,"v-for",!0),Mr(l,"type","radio"),gs(l,t),bs(c,{exp:"("+n+")==='radio'"+o,block:l});var u=Ss(e);return Hr(u,"v-for",!0),Mr(u,":type",n),gs(u,t),bs(c,{exp:i,block:u}),s?c.else=!0:a&&(c.elseif=a),c}}}}];var Es,Is,xs={expectHTML:!0,modules:Os,directives:{model:function(e,t,n){n;var r=t.value,i=t.modifiers,o=e.tag,s=e.attrsMap.type;if(e.component)return Vr(e,r,i),!1;if("select"===o)!function(e,t,n){var r='var $$selectedVal = Array.prototype.filter.call($event.target.options,function(o){return o.selected}).map(function(o){var val = "_value" in o ? o._value : o.value;return '+(n&&n.number?"_n(val)":"val")+"});";r=r+" "+Kr(t,"$event.target.multiple ? $$selectedVal : $$selectedVal[0]"),qr(e,"change",r,null,!0)}(e,r,i);else if("input"===o&&"checkbox"===s)!function(e,t,n){var r=n&&n.number,i=Wr(e,"value")||"null",o=Wr(e,"true-value")||"true",s=Wr(e,"false-value")||"false";jr(e,"checked","Array.isArray("+t+")?_i("+t+","+i+")>-1"+("true"===o?":("+t+")":":_q("+t+","+o+")")),qr(e,"change","var $$a="+t+",$$el=$event.target,$$c=$$el.checked?("+o+"):("+s+");if(Array.isArray($$a)){var $$v="+(r?"_n("+i+")":i)+",$$i=_i($$a,$$v);if($$el.checked){$$i<0&&("+Kr(t,"$$a.concat([$$v])")+")}else{$$i>-1&&("+Kr(t,"$$a.slice(0,$$i).concat($$a.slice($$i+1))")+")}}else{"+Kr(t,"$$c")+"}",null,!0)}(e,r,i);else if("input"===o&&"radio"===s)!function(e,t,n){var r=n&&n.number,i=Wr(e,"value")||"null";jr(e,"checked","_q("+t+","+(i=r?"_n("+i+")":i)+")"),qr(e,"change",Kr(t,i),null,!0)}(e,r,i);else if("input"===o||"textarea"===o)!function(e,t,n){var r=e.attrsMap.type,i=n||{},o=i.lazy,s=i.number,a=i.trim,c=!o&&"range"!==r,l=o?"change":"range"===r?ei:"input",u="$event.target.value";a&&(u="$event.target.value.trim()"),s&&(u="_n("+u+")");var h=Kr(t,u);c&&(h="if($event.target.composing)return;"+h),jr(e,"value","("+t+")"),qr(e,l,h,null,!0),(a||s)&&qr(e,"blur","$forceUpdate()")}(e,r,i);else if(!q.isReservedTag(o))return Vr(e,r,i),!1;return!0},text:function(e,t){t.value&&jr(e,"textContent","_s("+t.value+")",t)},html:function(e,t){t.value&&jr(e,"innerHTML","_s("+t.value+")",t)}},isPreTag:function(e){return"pre"===e},isUnaryTag:Eo,mustUseProp:Mn,canBeLeftOpenTag:Io,isReservedTag:Zn,getTagNamespace:er,staticKeys:function(e){return e.reduce(function(e,t){return e.concat(t.staticKeys||[])},[]).join(",")}(Os)},Ns=C(function(e){return _("type,tag,attrsList,attrsMap,plain,parent,children,attrs,start,end,rawAttrsMap"+(e?","+e:""))});function As(e,t){e&&(Es=Ns(t.staticKeys||""),Is=t.isReservedTag||R,function e(t){t.static=function(e){if(2===e.type)return!1;if(3===e.type)return!0;return!(!e.pre&&(e.hasBindings||e.if||e.for||m(e.tag)||!Is(e.tag)||function(e){for(;e.parent;){if("template"!==(e=e.parent).tag)return!1;if(e.for)return!0}return!1}(e)||!Object.keys(e).every(Es)))}(t);if(1===t.type){if(!Is(t.tag)&&"slot"!==t.tag&&null==t.attrsMap["inline-template"])return;for(var n=0,r=t.children.length;n<r;n++){var i=t.children[n];e(i),i.static||(t.static=!1)}if(t.ifConditions)for(var o=1,s=t.ifConditions.length;o<s;o++){var a=t.ifConditions[o].block;e(a),a.static||(t.static=!1)}}}(e),function e(t,n){if(1===t.type){if((t.static||t.once)&&(t.staticInFor=n),t.static&&t.children.length&&(1!==t.children.length||3!==t.children[0].type))return void(t.staticRoot=!0);if(t.staticRoot=!1,t.children)for(var r=0,i=t.children.length;r<i;r++)e(t.children[r],n||!!t.for);if(t.ifConditions)for(var o=1,s=t.ifConditions.length;o<s;o++)e(t.ifConditions[o].block,n)}}(e,!1))}var Rs=/^([\w$_]+|\([^)]*?\))\s*=>|^function(?:\s+[\w$]+)?\s*\(/,Ps=/\([^)]*?\);*$/,Ds=/^[A-Za-z_$][\w$]*(?:\.[A-Za-z_$][\w$]*|\['[^']*?']|\["[^"]*?"]|\[\d+]|\[[A-Za-z_$][\w$]*])*$/,js={esc:27,tab:9,enter:13,space:32,up:38,left:37,right:39,down:40,delete:[8,46]},$s={esc:["Esc","Escape"],tab:"Tab",enter:"Enter",space:[" ","Spacebar"],up:["Up","ArrowUp"],left:["Left","ArrowLeft"],right:["Right","ArrowRight"],down:["Down","ArrowDown"],delete:["Backspace","Delete","Del"]},Ms=function(e){return"if("+e+")return null;"},Ls={stop:"$event.stopPropagation();",prevent:"$event.preventDefault();",self:Ms("$event.target !== $event.currentTarget"),ctrl:Ms("!$event.ctrlKey"),shift:Ms("!$event.shiftKey"),alt:Ms("!$event.altKey"),meta:Ms("!$event.metaKey"),left:Ms("'button' in $event && $event.button !== 0"),middle:Ms("'button' in $event && $event.button !== 1"),right:Ms("'button' in $event && $event.button !== 2")};function Fs(e,t){var n=t?"nativeOn:":"on:",r="",i="";for(var o in e){var s=qs(e[o]);e[o]&&e[o].dynamic?i+=o+","+s+",":r+='"'+o+'":'+s+","}return r="{"+r.slice(0,-1)+"}",i?n+"_d("+r+",["+i.slice(0,-1)+"])":n+r}function qs(e){if(!e)return"function(){}";if(Array.isArray(e))return"["+e.map(function(e){return qs(e)}).join(",")+"]";var t=Ds.test(e.value),n=Rs.test(e.value),r=Ds.test(e.value.replace(Ps,""));if(e.modifiers){var i="",o="",s=[];for(var a in e.modifiers)if(Ls[a])o+=Ls[a],js[a]&&s.push(a);else if("exact"===a){var c=e.modifiers;o+=Ms(["ctrl","shift","alt","meta"].filter(function(e){return!c[e]}).map(function(e){return"$event."+e+"Key"}).join("||"))}else s.push(a);return s.length&&(i+=function(e){return"if(!$event.type.indexOf('key')&&"+e.map(Us).join("&&")+")return null;"}(s)),o&&(i+=o),"function($event){"+i+(t?"return "+e.value+".apply(null, arguments)":n?"return ("+e.value+").apply(null, arguments)":r?"return "+e.value:e.value)+"}"}return t||n?e.value:"function($event){"+(r?"return "+e.value:e.value)+"}"}function Us(e){var t=parseInt(e,10);if(t)return"$event.keyCode!=="+t;var n=js[e],r=$s[e];return"_k($event.keyCode,"+JSON.stringify(e)+","+JSON.stringify(n)+",$event.key,"+JSON.stringify(r)+")"}var Ws={on:function(e,t){e.wrapListeners=function(e){return"_g("+e+","+t.value+")"}},bind:function(e,t){e.wrapData=function(n){return"_b("+n+",'"+e.tag+"',"+t.value+","+(t.modifiers&&t.modifiers.prop?"true":"false")+(t.modifiers&&t.modifiers.sync?",true":"")+")"}},cloak:A},Hs=function(e){this.options=e,this.warn=e.warn||Pr,this.transforms=Dr(e.modules,"transformCode"),this.dataGenFns=Dr(e.modules,"genData"),this.directives=x(x({},Ws),e.directives);var t=e.isReservedTag||R;this.maybeComponent=function(e){return!!e.component||!t(e.tag)},this.onceId=0,this.staticRenderFns=[],this.pre=!1};function zs(e,t){var n=new Hs(t);return{render:"with(this){return "+(e?"script"===e.tag?"null":Bs(e,n):'_c("div")')+"}",staticRenderFns:n.staticRenderFns}}function Bs(e,t){if(e.parent&&(e.pre=e.pre||e.parent.pre),e.staticRoot&&!e.staticProcessed)return Vs(e,t);if(e.once&&!e.onceProcessed)return Ks(e,t);if(e.for&&!e.forProcessed)return Ys(e,t);if(e.if&&!e.ifProcessed)return Gs(e,t);if("template"!==e.tag||e.slotTarget||t.pre){if("slot"===e.tag)return function(e,t){var n=e.slotName||'"default"',r=Zs(e,t),i="_t("+n+(r?",function(){return "+r+"}":""),o=e.attrs||e.dynamicAttrs?na((e.attrs||[]).concat(e.dynamicAttrs||[]).map(function(e){return{name:T(e.name),value:e.value,dynamic:e.dynamic}})):null,s=e.attrsMap["v-bind"];!o&&!s||r||(i+=",null");o&&(i+=","+o);s&&(i+=(o?"":",null")+","+s);return i+")"}(e,t);var n;if(e.component)n=function(e,t,n){var r=t.inlineTemplate?null:Zs(t,n,!0);return"_c("+e+","+Js(t,n)+(r?","+r:"")+")"}(e.component,e,t);else{var r;(!e.plain||e.pre&&t.maybeComponent(e))&&(r=Js(e,t));var i=e.inlineTemplate?null:Zs(e,t,!0);n="_c('"+e.tag+"'"+(r?","+r:"")+(i?","+i:"")+")"}for(var o=0;o<t.transforms.length;o++)n=t.transforms[o](e,n);return n}return Zs(e,t)||"void 0"}function Vs(e,t){e.staticProcessed=!0;var n=t.pre;return e.pre&&(t.pre=e.pre),t.staticRenderFns.push("with(this){return "+Bs(e,t)+"}"),t.pre=n,"_m("+(t.staticRenderFns.length-1)+(e.staticInFor?",true":"")+")"}function Ks(e,t){if(e.onceProcessed=!0,e.if&&!e.ifProcessed)return Gs(e,t);if(e.staticInFor){for(var n="",r=e.parent;r;){if(r.for){n=r.key;break}r=r.parent}return n?"_o("+Bs(e,t)+","+t.onceId+++","+n+")":Bs(e,t)}return Vs(e,t)}function Gs(e,t,n,r){return e.ifProcessed=!0,function e(t,n,r,i){if(!t.length)return i||"_e()";var o=t.shift();return o.exp?"("+o.exp+")?"+s(o.block)+":"+e(t,n,r,i):""+s(o.block);function s(e){return r?r(e,n):e.once?Ks(e,n):Bs(e,n)}}(e.ifConditions.slice(),t,n,r)}function Ys(e,t,n,r){var i=e.for,o=e.alias,s=e.iterator1?","+e.iterator1:"",a=e.iterator2?","+e.iterator2:"";return e.forProcessed=!0,(r||"_l")+"(("+i+"),function("+o+s+a+"){return "+(n||Bs)(e,t)+"})"}function Js(e,t){var n="{",r=function(e,t){var n=e.directives;if(!n)return;var r,i,o,s,a="directives:[",c=!1;for(r=0,i=n.length;r<i;r++){o=n[r],s=!0;var l=t.directives[o.name];l&&(s=!!l(e,o,t.warn)),s&&(c=!0,a+='{name:"'+o.name+'",rawName:"'+o.rawName+'"'+(o.value?",value:("+o.value+"),expression:"+JSON.stringify(o.value):"")+(o.arg?",arg:"+(o.isDynamicArg?o.arg:'"'+o.arg+'"'):"")+(o.modifiers?",modifiers:"+JSON.stringify(o.modifiers):"")+"},")}if(c)return a.slice(0,-1)+"]"}(e,t);r&&(n+=r+","),e.key&&(n+="key:"+e.key+","),e.ref&&(n+="ref:"+e.ref+","),e.refInFor&&(n+="refInFor:true,"),e.pre&&(n+="pre:true,"),e.component&&(n+='tag:"'+e.tag+'",');for(var i=0;i<t.dataGenFns.length;i++)n+=t.dataGenFns[i](e);if(e.attrs&&(n+="attrs:"+na(e.attrs)+","),e.props&&(n+="domProps:"+na(e.props)+","),e.events&&(n+=Fs(e.events,!1)+","),e.nativeEvents&&(n+=Fs(e.nativeEvents,!0)+","),e.slotTarget&&!e.slotScope&&(n+="slot:"+e.slotTarget+","),e.scopedSlots&&(n+=function(e,t,n){var r=e.for||Object.keys(t).some(function(e){var n=t[e];return n.slotTargetDynamic||n.if||n.for||Qs(n)}),i=!!e.if;if(!r)for(var o=e.parent;o;){if(o.slotScope&&o.slotScope!==_s||o.for){r=!0;break}o.if&&(i=!0),o=o.parent}var s=Object.keys(t).map(function(e){return Xs(t[e],n)}).join(",");return"scopedSlots:_u(["+s+"]"+(r?",null,true":"")+(!r&&i?",null,false,"+function(e){var t=5381,n=e.length;for(;n;)t=33*t^e.charCodeAt(--n);return t>>>0}(s):"")+")"}(e,e.scopedSlots,t)+","),e.model&&(n+="model:{value:"+e.model.value+",callback:"+e.model.callback+",expression:"+e.model.expression+"},"),e.inlineTemplate){var o=function(e,t){var n=e.children[0];0;if(n&&1===n.type){var r=zs(n,t.options);return"inlineTemplate:{render:function(){"+r.render+"},staticRenderFns:["+r.staticRenderFns.map(function(e){return"function(){"+e+"}"}).join(",")+"]}"}}(e,t);o&&(n+=o+",")}return n=n.replace(/,$/,"")+"}",e.dynamicAttrs&&(n="_b("+n+',"'+e.tag+'",'+na(e.dynamicAttrs)+")"),e.wrapData&&(n=e.wrapData(n)),e.wrapListeners&&(n=e.wrapListeners(n)),n}function Qs(e){return 1===e.type&&("slot"===e.tag||e.children.some(Qs))}function Xs(e,t){var n=e.attrsMap["slot-scope"];if(e.if&&!e.ifProcessed&&!n)return Gs(e,t,Xs,"null");if(e.for&&!e.forProcessed)return Ys(e,t,Xs);var r=e.slotScope===_s?"":String(e.slotScope),i="function("+r+"){return "+("template"===e.tag?e.if&&n?"("+e.if+")?"+(Zs(e,t)||"undefined")+":undefined":Zs(e,t)||"undefined":Bs(e,t))+"}",o=r?"":",proxy:true";return"{key:"+(e.slotTarget||'"default"')+",fn:"+i+o+"}"}function Zs(e,t,n,r,i){var o=e.children;if(o.length){var s=o[0];if(1===o.length&&s.for&&"template"!==s.tag&&"slot"!==s.tag){var a=n?t.maybeComponent(s)?",1":",0":"";return""+(r||Bs)(s,t)+a}var c=n?function(e,t){for(var n=0,r=0;r<e.length;r++){var i=e[r];if(1===i.type){if(ea(i)||i.ifConditions&&i.ifConditions.some(function(e){return ea(e.block)})){n=2;break}(t(i)||i.ifConditions&&i.ifConditions.some(function(e){return t(e.block)}))&&(n=1)}}return n}(o,t.maybeComponent):0,l=i||ta;return"["+o.map(function(e){return l(e,t)}).join(",")+"]"+(c?","+c:"")}}function ea(e){return void 0!==e.for||"template"===e.tag||"slot"===e.tag}function ta(e,t){return 1===e.type?Bs(e,t):3===e.type&&e.isComment?(r=e,"_e("+JSON.stringify(r.text)+")"):"_v("+(2===(n=e).type?n.expression:ra(JSON.stringify(n.text)))+")";var n,r}function na(e){for(var t="",n="",r=0;r<e.length;r++){var i=e[r],o=ra(i.value);i.dynamic?n+=i.name+","+o+",":t+='"'+i.name+'":'+o+","}return t="{"+t.slice(0,-1)+"}",n?"_d("+t+",["+n.slice(0,-1)+"])":t}function ra(e){return e.replace(/\u2028/g,"\\u2028").replace(/\u2029/g,"\\u2029")}new RegExp("\\b"+"do,if,for,let,new,try,var,case,else,with,await,break,catch,class,const,super,throw,while,yield,delete,export,import,return,switch,default,extends,finally,continue,debugger,function,arguments".split(",").join("\\b|\\b")+"\\b"),new RegExp("\\b"+"delete,typeof,void".split(",").join("\\s*\\([^\\)]*\\)|\\b")+"\\s*\\([^\\)]*\\)");function ia(e,t){try{return new Function(e)}catch(n){return t.push({err:n,code:e}),A}}function oa(e){var t=Object.create(null);return function(n,r,i){(r=x({},r)).warn;delete r.warn;var o=r.delimiters?String(r.delimiters)+n:n;if(t[o])return t[o];var s=e(n,r);var a={},c=[];return a.render=ia(s.render,c),a.staticRenderFns=s.staticRenderFns.map(function(e){return ia(e,c)}),t[o]=a}}var sa,aa,ca=(sa=function(e,t){var n=vs(e.trim(),t);!1!==t.optimize&&As(n,t);var r=zs(n,t);return{ast:n,render:r.render,staticRenderFns:r.staticRenderFns}},function(e){function t(t,n){var r=Object.create(e),i=[],o=[],s=function(e,t,n){(n?o:i).push(e)};if(n)for(var a in n.modules&&(r.modules=(e.modules||[]).concat(n.modules)),n.directives&&(r.directives=x(Object.create(e.directives||null),n.directives)),n)"modules"!==a&&"directives"!==a&&(r[a]=n[a]);r.warn=s;var c=sa(t.trim(),r);return c.errors=i,c.tips=o,c}return{compile:t,compileToFunctions:oa(t)}})(xs),la=(ca.compile,ca.compileToFunctions);function ua(e){return(aa=aa||document.createElement("div")).innerHTML=e?'<a href="\n"/>':'<div a="\n"/>',aa.innerHTML.indexOf("&#10;")>0}var ha=!!K&&ua(!1),da=!!K&&ua(!0),fa=C(function(e){var t=rr(e);return t&&t.innerHTML}),pa=En.prototype.$mount;En.prototype.$mount=function(e,t){if((e=e&&rr(e))===document.body||e===document.documentElement)return this;var n=this.$options;if(!n.render){var r=n.template;if(r)if("string"==typeof r)"#"===r.charAt(0)&&(r=fa(r));else{if(!r.nodeType)return this;r=r.innerHTML}else e&&(r=function(e){if(e.outerHTML)return e.outerHTML;var t=document.createElement("div");return t.appendChild(e.cloneNode(!0)),t.innerHTML}(e));if(r){0;var i=la(r,{outputSourceRange:!1,shouldDecodeNewlines:ha,shouldDecodeNewlinesForHref:da,delimiters:n.delimiters,comments:n.comments},this),o=i.render,s=i.staticRenderFns;n.render=o,n.staticRenderFns=s}}return pa.call(this,e,t)},En.compile=la,t.a=En}).call(t,n("DuR2"))},"77Pl":function(e,t,n){var r=n("EqjI");e.exports=function(e){if(!r(e))throw TypeError(e+" is not an object!");return e}},"7KvD":function(e,t){var n=e.exports="undefined"!=typeof window&&window.Math==Math?window:"undefined"!=typeof self&&self.Math==Math?self:Function("return this")();"number"==typeof __g&&(__g=n)},"9bBU":function(e,t,n){n("mClu");var r=n("FeBl").Object;e.exports=function(e,t,n){return r.defineProperty(e,t,n)}},C4MV:function(e,t,n){e.exports={default:n("9bBU"),__esModule:!0}},D2L2:function(e,t){var n={}.hasOwnProperty;e.exports=function(e,t){return n.call(e,t)}},DuR2:function(e,t){var n;n=function(){return this}();try{n=n||Function("return this")()||(0,eval)("this")}catch(e){"object"==typeof window&&(n=window)}e.exports=n},EqjI:function(e,t){e.exports=function(e){return"object"==typeof e?null!==e:"function"==typeof e}},FeBl:function(e,t){var n=e.exports={version:"2.6.12"};"number"==typeof __e&&(__e=n)},MmMw:function(e,t,n){var r=n("EqjI");e.exports=function(e,t){if(!r(e))return e;var n,i;if(t&&"function"==typeof(n=e.toString)&&!r(i=n.call(e)))return i;if("function"==typeof(n=e.valueOf)&&!r(i=n.call(e)))return i;if(!t&&"function"==typeof(n=e.toString)&&!r(i=n.call(e)))return i;throw TypeError("Can't convert object to primitive value")}},ON07:function(e,t,n){var r=n("EqjI"),i=n("7KvD").document,o=r(i)&&r(i.createElement);e.exports=function(e){return o?i.createElement(e):{}}},RUaj:function(e,t,n){"use strict";(function(e){n.d(t,"a",function(){return Hi}),n.d(t,"b",function(){return ji}),n.d(t,"c",function(){return Mi});var r=n("a7sP"),i=n("xq9A"),o=n("28tl"),s=n("gZMR");const a="@firebase/database",c="0.12.5";
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let l="";
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const u=function(e){try{if("undefined"!=typeof window&&void 0!==window[e]){const t=window[e];return t.setItem("firebase:sentinel","cache"),t.removeItem("firebase:sentinel"),new
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class{constructor(e){this.domStorage_=e,this.prefix_="firebase:"}set(e,t){null==t?this.domStorage_.removeItem(this.prefixedName_(e)):this.domStorage_.setItem(this.prefixedName_(e),Object(o.z)(t))}get(e){const t=this.domStorage_.getItem(this.prefixedName_(e));return null==t?null:Object(o.t)(t)}remove(e){this.domStorage_.removeItem(this.prefixedName_(e))}prefixedName_(e){return this.prefix_+e}toString(){return this.domStorage_.toString()}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */(t)}}catch(e){}return new class{constructor(){this.cache_={},this.isInMemoryStorage=!0}set(e,t){null==t?delete this.cache_[e]:this.cache_[e]=t}get(e){return Object(o.h)(this.cache_,e)?this.cache_[e]:null}remove(e){delete this.cache_[e]}}},h=u("localStorage"),d=u("sessionStorage"),f=new s.b("@firebase/database"),p=function(){let e=1;return function(){return e++}}(),_=function(e){const t=Object(o.y)(e),n=new o.c;n.update(t);const r=n.digest();return o.f.encodeByteArray(r)},m=function(...e){let t="";for(let n=0;n<e.length;n++){const r=e[n];Array.isArray(r)||r&&"object"==typeof r&&"number"==typeof r.length?t+=m.apply(null,r):t+="object"==typeof r?Object(o.z)(r):r,t+=" "}return t};let v=null,g=!0;const y=function(e,t){Object(o.d)(!t||!0===e||!1===e,"Can't turn on custom loggers persistently."),!0===e?(f.logLevel=s.a.VERBOSE,v=f.log.bind(f),t&&d.set("logging_enabled",!0)):"function"==typeof e?v=e:(v=null,d.remove("logging_enabled"))},b=function(...e){if(!0===g&&(g=!1,null===v&&!0===d.get("logging_enabled")&&y(!0)),v){const t=m.apply(null,e);v(t)}},C=function(e){return function(...t){b(e,...t)}},w=function(...e){const t="FIREBASE INTERNAL ERROR: "+m(...e);f.error(t)},T=function(...e){const t=`FIREBASE FATAL ERROR: ${m(...e)}`;throw f.error(t),new Error(t)},k=function(...e){const t="FIREBASE WARNING: "+m(...e);f.warn(t)},S=function(e){return"number"==typeof e&&(e!=e||e===Number.POSITIVE_INFINITY||e===Number.NEGATIVE_INFINITY)},O=function(e){if(Object(o.q)()||"complete"===document.readyState)e();else{let t=!1;const n=function(){document.body?t||(t=!0,e()):setTimeout(n,Math.floor(10))};document.addEventListener?(document.addEventListener("DOMContentLoaded",n,!1),window.addEventListener("load",n,!1)):document.attachEvent&&(document.attachEvent("onreadystatechange",()=>{"complete"===document.readyState&&n()}),window.attachEvent("onload",n))}},E="[MIN_NAME]",I="[MAX_NAME]",x=function(e,t){if(e===t)return 0;if(e===E||t===I)return-1;if(t===E||e===I)return 1;{const n=F(e),r=F(t);return null!==n?null!==r?n-r==0?e.length-t.length:n-r:-1:null!==r?1:e<t?-1:1}},N=function(e,t){return e===t?0:e<t?-1:1},A=function(e,t){if(t&&e in t)return t[e];throw new Error("Missing required key ("+e+") in object: "+Object(o.z)(t))},R=function(e){if("object"!=typeof e||null===e)return Object(o.z)(e);const t=[];for(const n in e)t.push(n);t.sort();let n="{";for(let r=0;r<t.length;r++)0!==r&&(n+=","),n+=Object(o.z)(t[r]),n+=":",n+=R(e[t[r]]);return n+="}"},P=function(e,t){const n=e.length;if(n<=t)return[e];const r=[];for(let i=0;i<n;i+=t)i+t>n?r.push(e.substring(i,n)):r.push(e.substring(i,i+t));return r};function D(e,t){for(const n in e)e.hasOwnProperty(n)&&t(n,e[n])}const j=function(e){Object(o.d)(!S(e),"Invalid JSON number");let t,n,r,i,s;0===e?(n=0,r=0,t=1/e==-1/0?1:0):(t=e<0,(e=Math.abs(e))>=Math.pow(2,-1022)?(n=(i=Math.min(Math.floor(Math.log(e)/Math.LN2),1023))+1023,r=Math.round(e*Math.pow(2,52-i)-Math.pow(2,52))):(n=0,r=Math.round(e/Math.pow(2,-1074))));const a=[];for(s=52;s;s-=1)a.push(r%2?1:0),r=Math.floor(r/2);for(s=11;s;s-=1)a.push(n%2?1:0),n=Math.floor(n/2);a.push(t?1:0),a.reverse();const c=a.join("");let l="";for(s=0;s<64;s+=8){let e=parseInt(c.substr(s,8),2).toString(16);1===e.length&&(e="0"+e),l+=e}return l.toLowerCase()},$=function(){return!("object"!=typeof window||!window.chrome||!window.chrome.extension||/^chrome/.test(window.location.href))},M=function(){return"object"==typeof Windows&&"object"==typeof Windows.UI};const L=new RegExp("^-?(0*)\\d{1,10}$"),F=function(e){if(L.test(e)){const t=Number(e);if(t>=-2147483648&&t<=2147483647)return t}return null},q=function(e){try{e()}catch(e){setTimeout(()=>{const t=e.stack||"";throw k("Exception was thrown by user callback.",t),e},Math.floor(0))}},U=function(){return("object"==typeof window&&window.navigator&&window.navigator.userAgent||"").search(/googlebot|google webmaster tools|bingbot|yahoo! slurp|baiduspider|yandexbot|duckduckbot/i)>=0},W=function(e,t){const n=setTimeout(e,t);return"object"==typeof n&&n.unref&&n.unref(),n};
/**
 * @license
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class H{constructor(e,t){this.appName_=e,this.appCheckProvider=t,this.appCheck=null===t||void 0===t?void 0:t.getImmediate({optional:!0}),this.appCheck||null===t||void 0===t||t.get().then(e=>this.appCheck=e)}getToken(e){return this.appCheck?this.appCheck.getToken(e):new Promise((t,n)=>{setTimeout(()=>{this.appCheck?this.getToken(e).then(t,n):t(null)},0)})}addTokenChangeListener(e){var t;null===(t=this.appCheckProvider)||void 0===t||t.get().then(t=>t.addTokenListener(e))}notifyForInvalidToken(){k(`Provided AppCheck credentials for the app named "${this.appName_}" `+"are invalid. This usually indicates your app was not initialized correctly.")}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class z{constructor(e,t,n){this.appName_=e,this.firebaseOptions_=t,this.authProvider_=n,this.auth_=null,this.auth_=n.getImmediate({optional:!0}),this.auth_||n.onInit(e=>this.auth_=e)}getToken(e){return this.auth_?this.auth_.getToken(e).catch(e=>e&&"auth/token-not-initialized"===e.code?(b("Got auth/token-not-initialized error.  Treating as null token."),null):Promise.reject(e)):new Promise((t,n)=>{setTimeout(()=>{this.auth_?this.getToken(e).then(t,n):t(null)},0)})}addTokenChangeListener(e){this.auth_?this.auth_.addAuthTokenListener(e):this.authProvider_.get().then(t=>t.addAuthTokenListener(e))}removeTokenChangeListener(e){this.authProvider_.get().then(t=>t.removeAuthTokenListener(e))}notifyForInvalidToken(){let e='Provided authentication credentials for the app named "'+this.appName_+'" are invalid. This usually indicates your app was not initialized correctly. ';"credential"in this.firebaseOptions_?e+='Make sure the "credential" property provided to initializeApp() is authorized to access the specified "databaseURL" and is from the correct project.':"serviceAccount"in this.firebaseOptions_?e+='Make sure the "serviceAccount" property provided to initializeApp() is authorized to access the specified "databaseURL" and is from the correct project.':e+='Make sure the "apiKey" and "databaseURL" properties provided to initializeApp() match the values provided for your app at https://console.firebase.google.com/.',k(e)}}class B{constructor(e){this.accessToken=e}getToken(e){return Promise.resolve({accessToken:this.accessToken})}addTokenChangeListener(e){e(this.accessToken)}removeTokenChangeListener(e){}notifyForInvalidToken(){}}B.OWNER="owner";
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const V="5",K="v",G="s",Y="r",J="f",Q=/(console\.firebase|firebase-console-\w+\.corp|firebase\.corp)\.google\.com/,X="ls",Z="p",ee="ac",te="websocket",ne="long_polling";
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class re{constructor(e,t,n,r,i=!1,o="",s=!1){this.secure=t,this.namespace=n,this.webSocketOnly=r,this.nodeAdmin=i,this.persistenceKey=o,this.includeNamespaceInQueryParams=s,this._host=e.toLowerCase(),this._domain=this._host.substr(this._host.indexOf(".")+1),this.internalHost=h.get("host:"+e)||this._host}isCacheableHost(){return"s-"===this.internalHost.substr(0,2)}isCustomHost(){return"firebaseio.com"!==this._domain&&"firebaseio-demo.com"!==this._domain}get host(){return this._host}set host(e){e!==this.internalHost&&(this.internalHost=e,this.isCacheableHost()&&h.set("host:"+this._host,this.internalHost))}toString(){let e=this.toURLString();return this.persistenceKey&&(e+="<"+this.persistenceKey+">"),e}toURLString(){const e=this.secure?"https://":"http://",t=this.includeNamespaceInQueryParams?`?ns=${this.namespace}`:"";return`${e}${this.host}/${t}`}}function ie(e,t,n){let r;if(Object(o.d)("string"==typeof t,"typeof type must == string"),Object(o.d)("object"==typeof n,"typeof params must == object"),t===te)r=(e.secure?"wss://":"ws://")+e.internalHost+"/.ws?";else{if(t!==ne)throw new Error("Unknown connection type: "+t);r=(e.secure?"https://":"http://")+e.internalHost+"/.lp?"}(function(e){return e.host!==e.internalHost||e.isCustomHost()||e.includeNamespaceInQueryParams})(e)&&(n.ns=e.namespace);const i=[];return D(n,(e,t)=>{i.push(e+"="+t)}),r+i.join("&")}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class oe{constructor(){this.counters_={}}incrementCounter(e,t=1){Object(o.h)(this.counters_,e)||(this.counters_[e]=0),this.counters_[e]+=t}get(){return Object(o.j)(this.counters_)}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const se={},ae={};function ce(e){const t=e.toString();return se[t]||(se[t]=new oe),se[t]}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class le{constructor(e){this.onMessage_=e,this.pendingResponses=[],this.currentResponseNum=0,this.closeAfterResponse=-1,this.onClose=null}closeAfter(e,t){this.closeAfterResponse=e,this.onClose=t,this.closeAfterResponse<this.currentResponseNum&&(this.onClose(),this.onClose=null)}handleResponse(e,t){for(this.pendingResponses[e]=t;this.pendingResponses[this.currentResponseNum];){const e=this.pendingResponses[this.currentResponseNum];delete this.pendingResponses[this.currentResponseNum];for(let t=0;t<e.length;++t)e[t]&&q(()=>{this.onMessage_(e[t])});if(this.currentResponseNum===this.closeAfterResponse){this.onClose&&(this.onClose(),this.onClose=null);break}this.currentResponseNum++}}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const ue="start",he="close",de="pLPCommand",fe="pRTLPCB",pe="id",_e="pw",me="ser",ve="cb",ge="seg",ye="ts",be="d",Ce="dframe",we=1870,Te=30,ke=we-Te,Se=25e3,Oe=3e4;class Ee{constructor(e,t,n,r,i,o,s){this.connId=e,this.repoInfo=t,this.applicationId=n,this.appCheckToken=r,this.authToken=i,this.transportSessionId=o,this.lastSessionId=s,this.bytesSent=0,this.bytesReceived=0,this.everConnected_=!1,this.log_=C(e),this.stats_=ce(t),this.urlFn=(e=>(this.appCheckToken&&(e[ee]=this.appCheckToken),ie(t,ne,e)))}open(e,t){this.curSegmentNum=0,this.onDisconnect_=t,this.myPacketOrderer=new le(e),this.isClosed_=!1,this.connectTimeoutTimer_=setTimeout(()=>{this.log_("Timed out trying to connect."),this.onClosed_(),this.connectTimeoutTimer_=null},Math.floor(Oe)),O(()=>{if(this.isClosed_)return;this.scriptTagHolder=new Ie((...e)=>{const[t,n,r,i,o]=e;if(this.incrementIncomingBytes_(e),this.scriptTagHolder)if(this.connectTimeoutTimer_&&(clearTimeout(this.connectTimeoutTimer_),this.connectTimeoutTimer_=null),this.everConnected_=!0,t===ue)this.id=n,this.password=r;else{if(t!==he)throw new Error("Unrecognized command received: "+t);n?(this.scriptTagHolder.sendNewPolls=!1,this.myPacketOrderer.closeAfter(n,()=>{this.onClosed_()})):this.onClosed_()}},(...e)=>{const[t,n]=e;this.incrementIncomingBytes_(e),this.myPacketOrderer.handleResponse(t,n)},()=>{this.onClosed_()},this.urlFn);const e={};e[ue]="t",e[me]=Math.floor(1e8*Math.random()),this.scriptTagHolder.uniqueCallbackIdentifier&&(e[ve]=this.scriptTagHolder.uniqueCallbackIdentifier),e[K]=V,this.transportSessionId&&(e[G]=this.transportSessionId),this.lastSessionId&&(e[X]=this.lastSessionId),this.applicationId&&(e[Z]=this.applicationId),this.appCheckToken&&(e[ee]=this.appCheckToken),"undefined"!=typeof location&&location.hostname&&Q.test(location.hostname)&&(e[Y]=J);const t=this.urlFn(e);this.log_("Connecting via long-poll to "+t),this.scriptTagHolder.addTag(t,()=>{})})}start(){this.scriptTagHolder.startLongPoll(this.id,this.password),this.addDisconnectPingFrame(this.id,this.password)}static forceAllow(){Ee.forceAllow_=!0}static forceDisallow(){Ee.forceDisallow_=!0}static isAvailable(){return!Object(o.q)()&&(!!Ee.forceAllow_||!Ee.forceDisallow_&&"undefined"!=typeof document&&null!=document.createElement&&!$()&&!M())}markConnectionHealthy(){}shutdown_(){this.isClosed_=!0,this.scriptTagHolder&&(this.scriptTagHolder.close(),this.scriptTagHolder=null),this.myDisconnFrame&&(document.body.removeChild(this.myDisconnFrame),this.myDisconnFrame=null),this.connectTimeoutTimer_&&(clearTimeout(this.connectTimeoutTimer_),this.connectTimeoutTimer_=null)}onClosed_(){this.isClosed_||(this.log_("Longpoll is closing itself"),this.shutdown_(),this.onDisconnect_&&(this.onDisconnect_(this.everConnected_),this.onDisconnect_=null))}close(){this.isClosed_||(this.log_("Longpoll is being closed."),this.shutdown_())}send(e){const t=Object(o.z)(e);this.bytesSent+=t.length,this.stats_.incrementCounter("bytes_sent",t.length);const n=Object(o.g)(t),r=P(n,ke);for(let e=0;e<r.length;e++)this.scriptTagHolder.enqueueSegment(this.curSegmentNum,r.length,r[e]),this.curSegmentNum++}addDisconnectPingFrame(e,t){if(Object(o.q)())return;this.myDisconnFrame=document.createElement("iframe");const n={};n[Ce]="t",n[pe]=e,n[_e]=t,this.myDisconnFrame.src=this.urlFn(n),this.myDisconnFrame.style.display="none",document.body.appendChild(this.myDisconnFrame)}incrementIncomingBytes_(e){const t=Object(o.z)(e).length;this.bytesReceived+=t,this.stats_.incrementCounter("bytes_received",t)}}class Ie{constructor(e,t,n,r){if(this.onDisconnect=n,this.urlFn=r,this.outstandingRequests=new Set,this.pendingSegs=[],this.currentSerial=Math.floor(1e8*Math.random()),this.sendNewPolls=!0,Object(o.q)())this.commandCB=e,this.onMessageCB=t;else{this.uniqueCallbackIdentifier=p(),window[de+this.uniqueCallbackIdentifier]=e,window[fe+this.uniqueCallbackIdentifier]=t,this.myIFrame=Ie.createIFrame_();let n="";if(this.myIFrame.src&&"javascript:"===this.myIFrame.src.substr(0,"javascript:".length)){n='<script>document.domain="'+document.domain+'";<\/script>'}const r="<html><body>"+n+"</body></html>";try{this.myIFrame.doc.open(),this.myIFrame.doc.write(r),this.myIFrame.doc.close()}catch(e){b("frame writing exception"),e.stack&&b(e.stack),b(e)}}}static createIFrame_(){const e=document.createElement("iframe");if(e.style.display="none",!document.body)throw"Document body has not initialized. Wait to initialize Firebase until after the document is ready.";document.body.appendChild(e);try{e.contentWindow.document||b("No IE domain setting required")}catch(t){const n=document.domain;e.src="javascript:void((function(){document.open();document.domain='"+n+"';document.close();})())"}return e.contentDocument?e.doc=e.contentDocument:e.contentWindow?e.doc=e.contentWindow.document:e.document&&(e.doc=e.document),e}close(){this.alive=!1,this.myIFrame&&(this.myIFrame.doc.body.innerHTML="",setTimeout(()=>{null!==this.myIFrame&&(document.body.removeChild(this.myIFrame),this.myIFrame=null)},Math.floor(0)));const e=this.onDisconnect;e&&(this.onDisconnect=null,e())}startLongPoll(e,t){for(this.myID=e,this.myPW=t,this.alive=!0;this.newRequest_(););}newRequest_(){if(this.alive&&this.sendNewPolls&&this.outstandingRequests.size<(this.pendingSegs.length>0?2:1)){this.currentSerial++;const e={};e[pe]=this.myID,e[_e]=this.myPW,e[me]=this.currentSerial;let t=this.urlFn(e),n="",r=0;for(;this.pendingSegs.length>0;){if(!(this.pendingSegs[0].d.length+Te+n.length<=we))break;{const e=this.pendingSegs.shift();n=n+"&"+ge+r+"="+e.seg+"&"+ye+r+"="+e.ts+"&"+be+r+"="+e.d,r++}}return t+=n,this.addLongPollTag_(t,this.currentSerial),!0}return!1}enqueueSegment(e,t,n){this.pendingSegs.push({seg:e,ts:t,d:n}),this.alive&&this.newRequest_()}addLongPollTag_(e,t){this.outstandingRequests.add(t);const n=()=>{this.outstandingRequests.delete(t),this.newRequest_()},r=setTimeout(n,Math.floor(Se));this.addTag(e,()=>{clearTimeout(r),n()})}addTag(e,t){Object(o.q)()?this.doNodeLongPoll(e,t):setTimeout(()=>{try{if(!this.sendNewPolls)return;const n=this.myIFrame.doc.createElement("script");n.type="text/javascript",n.async=!0,n.src=e,n.onload=n.onreadystatechange=function(){const e=n.readyState;e&&"loaded"!==e&&"complete"!==e||(n.onload=n.onreadystatechange=null,n.parentNode&&n.parentNode.removeChild(n),t())},n.onerror=(()=>{b("Long-poll script failed to load: "+e),this.sendNewPolls=!1,this.close()}),this.myIFrame.doc.body.appendChild(n)}catch(e){}},Math.floor(1))}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const xe=16384,Ne=45e3;let Ae=null;"undefined"!=typeof MozWebSocket?Ae=MozWebSocket:"undefined"!=typeof WebSocket&&(Ae=WebSocket);class Re{constructor(e,t,n,r,i,o,s){this.connId=e,this.applicationId=n,this.appCheckToken=r,this.authToken=i,this.keepaliveTimer=null,this.frames=null,this.totalFrames=0,this.bytesSent=0,this.bytesReceived=0,this.log_=C(this.connId),this.stats_=ce(t),this.connURL=Re.connectionURL_(t,o,s,r),this.nodeAdmin=t.nodeAdmin}static connectionURL_(e,t,n,r){const i={};return i[K]=V,!Object(o.q)()&&"undefined"!=typeof location&&location.hostname&&Q.test(location.hostname)&&(i[Y]=J),t&&(i[G]=t),n&&(i[X]=n),r&&(i[ee]=r),ie(e,te,i)}open(t,n){this.onDisconnect=n,this.onMessage=t,this.log_("Websocket connecting to "+this.connURL),this.everConnected_=!1,h.set("previous_websocket_failure",!0);try{if(Object(o.q)()){const t=this.nodeAdmin?"AdminNode":"Node",n={headers:{"User-Agent":`Firebase/${V}/${l}/${e.platform}/${t}`,"X-Firebase-GMPID":this.applicationId||""}};this.authToken&&(n.headers.Authorization=`Bearer ${this.authToken}`),this.appCheckToken&&(n.headers["X-Firebase-AppCheck"]=this.appCheckToken);const r=Object({NODE_ENV:"production"}),i=0===this.connURL.indexOf("wss://")?r.HTTPS_PROXY||r.https_proxy:r.HTTP_PROXY||r.http_proxy;i&&(n.proxy={origin:i}),this.mySock=new Ae(this.connURL,[],n)}else{const e={headers:{"X-Firebase-GMPID":this.applicationId||"","X-Firebase-AppCheck":this.appCheckToken||""}};this.mySock=new Ae(this.connURL,[],e)}}catch(e){this.log_("Error instantiating WebSocket.");const t=e.message||e.data;return t&&this.log_(t),void this.onClosed_()}this.mySock.onopen=(()=>{this.log_("Websocket connected."),this.everConnected_=!0}),this.mySock.onclose=(()=>{this.log_("Websocket connection was disconnected."),this.mySock=null,this.onClosed_()}),this.mySock.onmessage=(e=>{this.handleIncomingFrame(e)}),this.mySock.onerror=(e=>{this.log_("WebSocket error.  Closing connection.");const t=e.message||e.data;t&&this.log_(t),this.onClosed_()})}start(){}static forceDisallow(){Re.forceDisallow_=!0}static isAvailable(){let e=!1;if("undefined"!=typeof navigator&&navigator.userAgent){const t=/Android ([0-9]{0,}\.[0-9]{0,})/,n=navigator.userAgent.match(t);n&&n.length>1&&parseFloat(n[1])<4.4&&(e=!0)}return!e&&null!==Ae&&!Re.forceDisallow_}static previouslyFailed(){return h.isInMemoryStorage||!0===h.get("previous_websocket_failure")}markConnectionHealthy(){h.remove("previous_websocket_failure")}appendFrame_(e){if(this.frames.push(e),this.frames.length===this.totalFrames){const e=this.frames.join("");this.frames=null;const t=Object(o.t)(e);this.onMessage(t)}}handleNewFrameCount_(e){this.totalFrames=e,this.frames=[]}extractFrameCount_(e){if(Object(o.d)(null===this.frames,"We already have a frame buffer"),e.length<=6){const t=Number(e);if(!isNaN(t))return this.handleNewFrameCount_(t),null}return this.handleNewFrameCount_(1),e}handleIncomingFrame(e){if(null===this.mySock)return;const t=e.data;if(this.bytesReceived+=t.length,this.stats_.incrementCounter("bytes_received",t.length),this.resetKeepAlive(),null!==this.frames)this.appendFrame_(t);else{const e=this.extractFrameCount_(t);null!==e&&this.appendFrame_(e)}}send(e){this.resetKeepAlive();const t=Object(o.z)(e);this.bytesSent+=t.length,this.stats_.incrementCounter("bytes_sent",t.length);const n=P(t,xe);n.length>1&&this.sendString_(String(n.length));for(let e=0;e<n.length;e++)this.sendString_(n[e])}shutdown_(){this.isClosed_=!0,this.keepaliveTimer&&(clearInterval(this.keepaliveTimer),this.keepaliveTimer=null),this.mySock&&(this.mySock.close(),this.mySock=null)}onClosed_(){this.isClosed_||(this.log_("WebSocket is closing itself"),this.shutdown_(),this.onDisconnect&&(this.onDisconnect(this.everConnected_),this.onDisconnect=null))}close(){this.isClosed_||(this.log_("WebSocket is being closed"),this.shutdown_())}resetKeepAlive(){clearInterval(this.keepaliveTimer),this.keepaliveTimer=setInterval(()=>{this.mySock&&this.sendString_("0"),this.resetKeepAlive()},Math.floor(Ne))}sendString_(e){try{this.mySock.send(e)}catch(e){this.log_("Exception thrown from WebSocket.send():",e.message||e.data,"Closing connection."),setTimeout(this.onClosed_.bind(this),0)}}}Re.responsesRequiredToBeHealthy=2,Re.healthyTimeout=3e4;
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class Pe{constructor(e){this.initTransports_(e)}static get ALL_TRANSPORTS(){return[Ee,Re]}initTransports_(e){const t=Re&&Re.isAvailable();let n=t&&!Re.previouslyFailed();if(e.webSocketOnly&&(t||k("wss:// URL used, but browser isn't known to support websockets.  Trying anyway."),n=!0),n)this.transports_=[Re];else{const e=this.transports_=[];for(const t of Pe.ALL_TRANSPORTS)t&&t.isAvailable()&&e.push(t)}}initialTransport(){if(this.transports_.length>0)return this.transports_[0];throw new Error("No transports available")}upgradeTransport(){return this.transports_.length>1?this.transports_[1]:null}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const De=6e4,je=5e3,$e=10240,Me=102400,Le="t",Fe="d",qe="s",Ue="r",We="e",He="o",ze="a",Be="n",Ve="p",Ke="h";class Ge{constructor(e,t,n,r,i,o,s,a,c,l){this.id=e,this.repoInfo_=t,this.applicationId_=n,this.appCheckToken_=r,this.authToken_=i,this.onMessage_=o,this.onReady_=s,this.onDisconnect_=a,this.onKill_=c,this.lastSessionId=l,this.connectionCount=0,this.pendingDataMessages=[],this.state_=0,this.log_=C("c:"+this.id+":"),this.transportManager_=new Pe(t),this.log_("Connection created"),this.start_()}start_(){const e=this.transportManager_.initialTransport();this.conn_=new e(this.nextTransportId_(),this.repoInfo_,this.applicationId_,this.appCheckToken_,this.authToken_,null,this.lastSessionId),this.primaryResponsesRequired_=e.responsesRequiredToBeHealthy||0;const t=this.connReceiver_(this.conn_),n=this.disconnReceiver_(this.conn_);this.tx_=this.conn_,this.rx_=this.conn_,this.secondaryConn_=null,this.isHealthy_=!1,setTimeout(()=>{this.conn_&&this.conn_.open(t,n)},Math.floor(0));const r=e.healthyTimeout||0;r>0&&(this.healthyTimeout_=W(()=>{this.healthyTimeout_=null,this.isHealthy_||(this.conn_&&this.conn_.bytesReceived>Me?(this.log_("Connection exceeded healthy timeout but has received "+this.conn_.bytesReceived+" bytes.  Marking connection healthy."),this.isHealthy_=!0,this.conn_.markConnectionHealthy()):this.conn_&&this.conn_.bytesSent>$e?this.log_("Connection exceeded healthy timeout but has sent "+this.conn_.bytesSent+" bytes.  Leaving connection alive."):(this.log_("Closing unhealthy connection after timeout."),this.close()))},Math.floor(r)))}nextTransportId_(){return"c:"+this.id+":"+this.connectionCount++}disconnReceiver_(e){return t=>{e===this.conn_?this.onConnectionLost_(t):e===this.secondaryConn_?(this.log_("Secondary connection lost."),this.onSecondaryConnectionLost_()):this.log_("closing an old connection")}}connReceiver_(e){return t=>{2!==this.state_&&(e===this.rx_?this.onPrimaryMessageReceived_(t):e===this.secondaryConn_?this.onSecondaryMessageReceived_(t):this.log_("message on old connection"))}}sendRequest(e){const t={t:"d",d:e};this.sendData_(t)}tryCleanupConnection(){this.tx_===this.secondaryConn_&&this.rx_===this.secondaryConn_&&(this.log_("cleaning up and promoting a connection: "+this.secondaryConn_.connId),this.conn_=this.secondaryConn_,this.secondaryConn_=null)}onSecondaryControl_(e){if(Le in e){const t=e[Le];t===ze?this.upgradeIfSecondaryHealthy_():t===Ue?(this.log_("Got a reset on secondary, closing it"),this.secondaryConn_.close(),this.tx_!==this.secondaryConn_&&this.rx_!==this.secondaryConn_||this.close()):t===He&&(this.log_("got pong on secondary."),this.secondaryResponsesRequired_--,this.upgradeIfSecondaryHealthy_())}}onSecondaryMessageReceived_(e){const t=A("t",e),n=A("d",e);if("c"===t)this.onSecondaryControl_(n);else{if("d"!==t)throw new Error("Unknown protocol layer: "+t);this.pendingDataMessages.push(n)}}upgradeIfSecondaryHealthy_(){this.secondaryResponsesRequired_<=0?(this.log_("Secondary connection is healthy."),this.isHealthy_=!0,this.secondaryConn_.markConnectionHealthy(),this.proceedWithUpgrade_()):(this.log_("sending ping on secondary."),this.secondaryConn_.send({t:"c",d:{t:Ve,d:{}}}))}proceedWithUpgrade_(){this.secondaryConn_.start(),this.log_("sending client ack on secondary"),this.secondaryConn_.send({t:"c",d:{t:ze,d:{}}}),this.log_("Ending transmission on primary"),this.conn_.send({t:"c",d:{t:Be,d:{}}}),this.tx_=this.secondaryConn_,this.tryCleanupConnection()}onPrimaryMessageReceived_(e){const t=A("t",e),n=A("d",e);"c"===t?this.onControl_(n):"d"===t&&this.onDataMessage_(n)}onDataMessage_(e){this.onPrimaryResponse_(),this.onMessage_(e)}onPrimaryResponse_(){this.isHealthy_||(this.primaryResponsesRequired_--,this.primaryResponsesRequired_<=0&&(this.log_("Primary connection is healthy."),this.isHealthy_=!0,this.conn_.markConnectionHealthy()))}onControl_(e){const t=A(Le,e);if(Fe in e){const n=e[Fe];if(t===Ke)this.onHandshake_(n);else if(t===Be){this.log_("recvd end transmission on primary"),this.rx_=this.secondaryConn_;for(let e=0;e<this.pendingDataMessages.length;++e)this.onDataMessage_(this.pendingDataMessages[e]);this.pendingDataMessages=[],this.tryCleanupConnection()}else t===qe?this.onConnectionShutdown_(n):t===Ue?this.onReset_(n):t===We?w("Server Error: "+n):t===He?(this.log_("got pong on primary."),this.onPrimaryResponse_(),this.sendPingOnPrimaryIfNecessary_()):w("Unknown control packet command: "+t)}}onHandshake_(e){const t=e.ts,n=e.v,r=e.h;this.sessionId=e.s,this.repoInfo_.host=r,0===this.state_&&(this.conn_.start(),this.onConnectionEstablished_(this.conn_,t),V!==n&&k("Protocol version mismatch detected"),this.tryStartUpgrade_())}tryStartUpgrade_(){const e=this.transportManager_.upgradeTransport();e&&this.startUpgrade_(e)}startUpgrade_(e){this.secondaryConn_=new e(this.nextTransportId_(),this.repoInfo_,this.applicationId_,this.appCheckToken_,this.authToken_,this.sessionId),this.secondaryResponsesRequired_=e.responsesRequiredToBeHealthy||0;const t=this.connReceiver_(this.secondaryConn_),n=this.disconnReceiver_(this.secondaryConn_);this.secondaryConn_.open(t,n),W(()=>{this.secondaryConn_&&(this.log_("Timed out trying to upgrade."),this.secondaryConn_.close())},Math.floor(De))}onReset_(e){this.log_("Reset packet received.  New host: "+e),this.repoInfo_.host=e,1===this.state_?this.close():(this.closeConnections_(),this.start_())}onConnectionEstablished_(e,t){this.log_("Realtime connection established."),this.conn_=e,this.state_=1,this.onReady_&&(this.onReady_(t,this.sessionId),this.onReady_=null),0===this.primaryResponsesRequired_?(this.log_("Primary connection is healthy."),this.isHealthy_=!0):W(()=>{this.sendPingOnPrimaryIfNecessary_()},Math.floor(je))}sendPingOnPrimaryIfNecessary_(){this.isHealthy_||1!==this.state_||(this.log_("sending ping on primary."),this.sendData_({t:"c",d:{t:Ve,d:{}}}))}onSecondaryConnectionLost_(){const e=this.secondaryConn_;this.secondaryConn_=null,this.tx_!==e&&this.rx_!==e||this.close()}onConnectionLost_(e){this.conn_=null,e||0!==this.state_?1===this.state_&&this.log_("Realtime connection lost."):(this.log_("Realtime connection failed."),this.repoInfo_.isCacheableHost()&&(h.remove("host:"+this.repoInfo_.host),this.repoInfo_.internalHost=this.repoInfo_.host)),this.close()}onConnectionShutdown_(e){this.log_("Connection shutdown command received. Shutting down..."),this.onKill_&&(this.onKill_(e),this.onKill_=null),this.onDisconnect_=null,this.close()}sendData_(e){if(1!==this.state_)throw"Connection is not connected";this.tx_.send(e)}close(){2!==this.state_&&(this.log_("Closing realtime connection."),this.state_=2,this.closeConnections_(),this.onDisconnect_&&(this.onDisconnect_(),this.onDisconnect_=null))}closeConnections_(){this.log_("Shutting down all connections"),this.conn_&&(this.conn_.close(),this.conn_=null),this.secondaryConn_&&(this.secondaryConn_.close(),this.secondaryConn_=null),this.healthyTimeout_&&(clearTimeout(this.healthyTimeout_),this.healthyTimeout_=null)}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Ye{put(e,t,n,r){}merge(e,t,n,r){}refreshAuthToken(e){}refreshAppCheckToken(e){}onDisconnectPut(e,t,n){}onDisconnectMerge(e,t,n){}onDisconnectCancel(e,t){}reportStats(e){}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Je{constructor(e){this.allowedEvents_=e,this.listeners_={},Object(o.d)(Array.isArray(e)&&e.length>0,"Requires a non-empty array")}trigger(e,...t){if(Array.isArray(this.listeners_[e])){const n=[...this.listeners_[e]];for(let e=0;e<n.length;e++)n[e].callback.apply(n[e].context,t)}}on(e,t,n){this.validateEventType_(e),this.listeners_[e]=this.listeners_[e]||[],this.listeners_[e].push({callback:t,context:n});const r=this.getInitialEvent(e);r&&t.apply(n,r)}off(e,t,n){this.validateEventType_(e);const r=this.listeners_[e]||[];for(let e=0;e<r.length;e++)if(r[e].callback===t&&(!n||n===r[e].context))return void r.splice(e,1)}validateEventType_(e){Object(o.d)(this.allowedEvents_.find(t=>t===e),"Unknown event: "+e)}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Qe extends Je{constructor(){super(["online"]),this.online_=!0,"undefined"==typeof window||void 0===window.addEventListener||Object(o.p)()||(window.addEventListener("online",()=>{this.online_||(this.online_=!0,this.trigger("online",!0))},!1),window.addEventListener("offline",()=>{this.online_&&(this.online_=!1,this.trigger("online",!1))},!1))}static getInstance(){return new Qe}getInitialEvent(e){return Object(o.d)("online"===e,"Unknown event type: "+e),[this.online_]}currentlyOnline(){return this.online_}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const Xe=32,Ze=768;class et{constructor(e,t){if(void 0===t){this.pieces_=e.split("/");let t=0;for(let e=0;e<this.pieces_.length;e++)this.pieces_[e].length>0&&(this.pieces_[t]=this.pieces_[e],t++);this.pieces_.length=t,this.pieceNum_=0}else this.pieces_=e,this.pieceNum_=t}toString(){let e="";for(let t=this.pieceNum_;t<this.pieces_.length;t++)""!==this.pieces_[t]&&(e+="/"+this.pieces_[t]);return e||"/"}}function tt(){return new et("")}function nt(e){return e.pieceNum_>=e.pieces_.length?null:e.pieces_[e.pieceNum_]}function rt(e){return e.pieces_.length-e.pieceNum_}function it(e){let t=e.pieceNum_;return t<e.pieces_.length&&t++,new et(e.pieces_,t)}function ot(e){return e.pieceNum_<e.pieces_.length?e.pieces_[e.pieces_.length-1]:null}function st(e,t=0){return e.pieces_.slice(e.pieceNum_+t)}function at(e){if(e.pieceNum_>=e.pieces_.length)return null;const t=[];for(let n=e.pieceNum_;n<e.pieces_.length-1;n++)t.push(e.pieces_[n]);return new et(t,0)}function ct(e,t){const n=[];for(let t=e.pieceNum_;t<e.pieces_.length;t++)n.push(e.pieces_[t]);if(t instanceof et)for(let e=t.pieceNum_;e<t.pieces_.length;e++)n.push(t.pieces_[e]);else{const e=t.split("/");for(let t=0;t<e.length;t++)e[t].length>0&&n.push(e[t])}return new et(n,0)}function lt(e){return e.pieceNum_>=e.pieces_.length}function ut(e,t){const n=nt(e),r=nt(t);if(null===n)return t;if(n===r)return ut(it(e),it(t));throw new Error("INTERNAL ERROR: innerPath ("+t+") is not within outerPath ("+e+")")}function ht(e,t){if(rt(e)!==rt(t))return!1;for(let n=e.pieceNum_,r=t.pieceNum_;n<=e.pieces_.length;n++,r++)if(e.pieces_[n]!==t.pieces_[r])return!1;return!0}function dt(e,t){let n=e.pieceNum_,r=t.pieceNum_;if(rt(e)>rt(t))return!1;for(;n<e.pieces_.length;){if(e.pieces_[n]!==t.pieces_[r])return!1;++n,++r}return!0}function ft(e){if(e.byteLength_>Ze)throw new Error(e.errorPrefix_+"has a key path longer than "+Ze+" bytes ("+e.byteLength_+").");if(e.parts_.length>Xe)throw new Error(e.errorPrefix_+"path specified exceeds the maximum depth that can be written ("+Xe+") or object contains a cycle "+pt(e))}function pt(e){return 0===e.parts_.length?"":"in property '"+e.parts_.join(".")+"'"}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class _t extends Je{constructor(){let e,t;super(["visible"]),"undefined"!=typeof document&&void 0!==document.addEventListener&&(void 0!==document.hidden?(t="visibilitychange",e="hidden"):void 0!==document.mozHidden?(t="mozvisibilitychange",e="mozHidden"):void 0!==document.msHidden?(t="msvisibilitychange",e="msHidden"):void 0!==document.webkitHidden&&(t="webkitvisibilitychange",e="webkitHidden")),this.visible_=!0,t&&document.addEventListener(t,()=>{const t=!document[e];t!==this.visible_&&(this.visible_=t,this.trigger("visible",t))},!1)}static getInstance(){return new _t}getInitialEvent(e){return Object(o.d)("visible"===e,"Unknown event type: "+e),[this.visible_]}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const mt=1e3,vt=3e5,gt=3e3,yt=3e4,bt=1.3,Ct=3e4,wt="server_kill",Tt=3;class kt extends Ye{constructor(e,t,n,r,i,s,a,c){if(super(),this.repoInfo_=e,this.applicationId_=t,this.onDataUpdate_=n,this.onConnectStatus_=r,this.onServerInfoUpdate_=i,this.authTokenProvider_=s,this.appCheckTokenProvider_=a,this.authOverride_=c,this.id=kt.nextPersistentConnectionId_++,this.log_=C("p:"+this.id+":"),this.interruptReasons_={},this.listens=new Map,this.outstandingPuts_=[],this.outstandingGets_=[],this.outstandingPutCount_=0,this.outstandingGetCount_=0,this.onDisconnectRequestQueue_=[],this.connected_=!1,this.reconnectDelay_=mt,this.maxReconnectDelay_=vt,this.securityDebugCallback_=null,this.lastSessionId=null,this.establishConnectionTimer_=null,this.visible_=!1,this.requestCBHash_={},this.requestNumber_=0,this.realtime_=null,this.authToken_=null,this.appCheckToken_=null,this.forceTokenRefresh_=!1,this.invalidAuthTokenCount_=0,this.invalidAppCheckTokenCount_=0,this.firstConnection_=!0,this.lastConnectionAttemptTime_=null,this.lastConnectionEstablishedTime_=null,c&&!Object(o.q)())throw new Error("Auth override specified in options, but not supported on non Node.js platforms");_t.getInstance().on("visible",this.onVisible_,this),-1===e.host.indexOf("fblocal")&&Qe.getInstance().on("online",this.onOnline_,this)}sendRequest(e,t,n){const r=++this.requestNumber_,i={r:r,a:e,b:t};this.log_(Object(o.z)(i)),Object(o.d)(this.connected_,"sendRequest call when we're not connected not allowed."),this.realtime_.sendRequest(i),n&&(this.requestCBHash_[r]=n)}get(e){this.initConnection_();const t=new o.a,n={p:e._path.toString(),q:e._queryObject},r={action:"g",request:n,onComplete:e=>{const r=e.d;"ok"===e.s?(this.onDataUpdate_(n.p,r,!1,null),t.resolve(r)):t.reject(r)}};this.outstandingGets_.push(r),this.outstandingGetCount_++;const i=this.outstandingGets_.length-1;return this.connected_||setTimeout(()=>{const e=this.outstandingGets_[i];void 0!==e&&r===e&&(delete this.outstandingGets_[i],this.outstandingGetCount_--,0===this.outstandingGetCount_&&(this.outstandingGets_=[]),this.log_("get "+i+" timed out on connection"),t.reject(new Error("Client is offline.")))},gt),this.connected_&&this.sendGet_(i),t.promise}listen(e,t,n,r){this.initConnection_();const i=e._queryIdentifier,s=e._path.toString();this.log_("Listen called for "+s+" "+i),this.listens.has(s)||this.listens.set(s,new Map),Object(o.d)(e._queryParams.isDefault()||!e._queryParams.loadsAllData(),"listen() called for non-default but complete query"),Object(o.d)(!this.listens.get(s).has(i),"listen() called twice for same path/queryId.");const a={onComplete:r,hashFn:t,query:e,tag:n};this.listens.get(s).set(i,a),this.connected_&&this.sendListen_(a)}sendGet_(e){const t=this.outstandingGets_[e];this.sendRequest("g",t.request,n=>{delete this.outstandingGets_[e],this.outstandingGetCount_--,0===this.outstandingGetCount_&&(this.outstandingGets_=[]),t.onComplete&&t.onComplete(n)})}sendListen_(e){const t=e.query,n=t._path.toString(),r=t._queryIdentifier;this.log_("Listen on "+n+" for "+r);const i={p:n};e.tag&&(i.q=t._queryObject,i.t=e.tag),i.h=e.hashFn(),this.sendRequest("q",i,i=>{const o=i.d,s=i.s;kt.warnOnListenWarnings_(o,t),(this.listens.get(n)&&this.listens.get(n).get(r))===e&&(this.log_("listen response",i),"ok"!==s&&this.removeListen_(n,r),e.onComplete&&e.onComplete(s,o))})}static warnOnListenWarnings_(e,t){if(e&&"object"==typeof e&&Object(o.h)(e,"w")){const n=Object(o.w)(e,"w");if(Array.isArray(n)&&~n.indexOf("no_index")){const e='".indexOn": "'+t._queryParams.getIndex().toString()+'"',n=t._path.toString();k("Using an unspecified index. Your data will be downloaded and "+`filtered on the client. Consider adding ${e} at `+`${n} to your security rules for better performance.`)}}}refreshAuthToken(e){this.authToken_=e,this.log_("Auth token refreshed"),this.authToken_?this.tryAuth():this.connected_&&this.sendRequest("unauth",{},()=>{}),this.reduceReconnectDelayIfAdminCredential_(e)}reduceReconnectDelayIfAdminCredential_(e){(e&&40===e.length||Object(o.n)(e))&&(this.log_("Admin auth credential detected.  Reducing max reconnect time."),this.maxReconnectDelay_=yt)}refreshAppCheckToken(e){this.appCheckToken_=e,this.log_("App check token refreshed"),this.appCheckToken_?this.tryAppCheck():this.connected_&&this.sendRequest("unappeck",{},()=>{})}tryAuth(){if(this.connected_&&this.authToken_){const e=this.authToken_,t=Object(o.s)(e)?"auth":"gauth",n={cred:e};null===this.authOverride_?n.noauth=!0:"object"==typeof this.authOverride_&&(n.authvar=this.authOverride_),this.sendRequest(t,n,t=>{const n=t.s,r=t.d||"error";this.authToken_===e&&("ok"===n?this.invalidAuthTokenCount_=0:this.onAuthRevoked_(n,r))})}}tryAppCheck(){this.connected_&&this.appCheckToken_&&this.sendRequest("appcheck",{token:this.appCheckToken_},e=>{const t=e.s,n=e.d||"error";"ok"===t?this.invalidAppCheckTokenCount_=0:this.onAppCheckRevoked_(t,n)})}unlisten(e,t){const n=e._path.toString(),r=e._queryIdentifier;this.log_("Unlisten called for "+n+" "+r),Object(o.d)(e._queryParams.isDefault()||!e._queryParams.loadsAllData(),"unlisten() called for non-default but complete query"),this.removeListen_(n,r)&&this.connected_&&this.sendUnlisten_(n,r,e._queryObject,t)}sendUnlisten_(e,t,n,r){this.log_("Unlisten on "+e+" for "+t);const i={p:e};r&&(i.q=n,i.t=r),this.sendRequest("n",i)}onDisconnectPut(e,t,n){this.initConnection_(),this.connected_?this.sendOnDisconnect_("o",e,t,n):this.onDisconnectRequestQueue_.push({pathString:e,action:"o",data:t,onComplete:n})}onDisconnectMerge(e,t,n){this.initConnection_(),this.connected_?this.sendOnDisconnect_("om",e,t,n):this.onDisconnectRequestQueue_.push({pathString:e,action:"om",data:t,onComplete:n})}onDisconnectCancel(e,t){this.initConnection_(),this.connected_?this.sendOnDisconnect_("oc",e,null,t):this.onDisconnectRequestQueue_.push({pathString:e,action:"oc",data:null,onComplete:t})}sendOnDisconnect_(e,t,n,r){const i={p:t,d:n};this.log_("onDisconnect "+e,i),this.sendRequest(e,i,e=>{r&&setTimeout(()=>{r(e.s,e.d)},Math.floor(0))})}put(e,t,n,r){this.putInternal("p",e,t,n,r)}merge(e,t,n,r){this.putInternal("m",e,t,n,r)}putInternal(e,t,n,r,i){this.initConnection_();const o={p:t,d:n};void 0!==i&&(o.h=i),this.outstandingPuts_.push({action:e,request:o,onComplete:r}),this.outstandingPutCount_++;const s=this.outstandingPuts_.length-1;this.connected_?this.sendPut_(s):this.log_("Buffering put: "+t)}sendPut_(e){const t=this.outstandingPuts_[e].action,n=this.outstandingPuts_[e].request,r=this.outstandingPuts_[e].onComplete;this.outstandingPuts_[e].queued=this.connected_,this.sendRequest(t,n,n=>{this.log_(t+" response",n),delete this.outstandingPuts_[e],this.outstandingPutCount_--,0===this.outstandingPutCount_&&(this.outstandingPuts_=[]),r&&r(n.s,n.d)})}reportStats(e){if(this.connected_){const t={c:e};this.log_("reportStats",t),this.sendRequest("s",t,e=>{if("ok"!==e.s){const t=e.d;this.log_("reportStats","Error sending stats: "+t)}})}}onDataMessage_(e){if("r"in e){this.log_("from server: "+Object(o.z)(e));const t=e.r,n=this.requestCBHash_[t];n&&(delete this.requestCBHash_[t],n(e.b))}else{if("error"in e)throw"A server-side error has occurred: "+e.error;"a"in e&&this.onDataPush_(e.a,e.b)}}onDataPush_(e,t){this.log_("handleServerMessage",e,t),"d"===e?this.onDataUpdate_(t.p,t.d,!1,t.t):"m"===e?this.onDataUpdate_(t.p,t.d,!0,t.t):"c"===e?this.onListenRevoked_(t.p,t.q):"ac"===e?this.onAuthRevoked_(t.s,t.d):"apc"===e?this.onAppCheckRevoked_(t.s,t.d):"sd"===e?this.onSecurityDebugPacket_(t):w("Unrecognized action received from server: "+Object(o.z)(e)+"\nAre you using the latest client?")}onReady_(e,t){this.log_("connection ready"),this.connected_=!0,this.lastConnectionEstablishedTime_=(new Date).getTime(),this.handleTimestamp_(e),this.lastSessionId=t,this.firstConnection_&&this.sendConnectStats_(),this.restoreState_(),this.firstConnection_=!1,this.onConnectStatus_(!0)}scheduleConnect_(e){Object(o.d)(!this.realtime_,"Scheduling a connect when we're already connected/ing?"),this.establishConnectionTimer_&&clearTimeout(this.establishConnectionTimer_),this.establishConnectionTimer_=setTimeout(()=>{this.establishConnectionTimer_=null,this.establishConnection_()},Math.floor(e))}initConnection_(){!this.realtime_&&this.firstConnection_&&this.scheduleConnect_(0)}onVisible_(e){e&&!this.visible_&&this.reconnectDelay_===this.maxReconnectDelay_&&(this.log_("Window became visible.  Reducing delay."),this.reconnectDelay_=mt,this.realtime_||this.scheduleConnect_(0)),this.visible_=e}onOnline_(e){e?(this.log_("Browser went online."),this.reconnectDelay_=mt,this.realtime_||this.scheduleConnect_(0)):(this.log_("Browser went offline.  Killing connection."),this.realtime_&&this.realtime_.close())}onRealtimeDisconnect_(){if(this.log_("data client disconnected"),this.connected_=!1,this.realtime_=null,this.cancelSentTransactions_(),this.requestCBHash_={},this.shouldReconnect_()){if(this.visible_){if(this.lastConnectionEstablishedTime_){(new Date).getTime()-this.lastConnectionEstablishedTime_>Ct&&(this.reconnectDelay_=mt),this.lastConnectionEstablishedTime_=null}}else this.log_("Window isn't visible.  Delaying reconnect."),this.reconnectDelay_=this.maxReconnectDelay_,this.lastConnectionAttemptTime_=(new Date).getTime();const e=(new Date).getTime()-this.lastConnectionAttemptTime_;let t=Math.max(0,this.reconnectDelay_-e);t=Math.random()*t,this.log_("Trying to reconnect in "+t+"ms"),this.scheduleConnect_(t),this.reconnectDelay_=Math.min(this.maxReconnectDelay_,this.reconnectDelay_*bt)}this.onConnectStatus_(!1)}async establishConnection_(){if(this.shouldReconnect_()){this.log_("Making a connection attempt"),this.lastConnectionAttemptTime_=(new Date).getTime(),this.lastConnectionEstablishedTime_=null;const e=this.onDataMessage_.bind(this),t=this.onReady_.bind(this),n=this.onRealtimeDisconnect_.bind(this),r=this.id+":"+kt.nextConnectionId_++,i=this.lastSessionId;let s=!1,a=null;const c=function(){a?a.close():(s=!0,n())},l=function(e){Object(o.d)(a,"sendRequest call when we're not connected not allowed."),a.sendRequest(e)};this.realtime_={close:c,sendRequest:l};const u=this.forceTokenRefresh_;this.forceTokenRefresh_=!1;try{const[o,l]=await Promise.all([this.authTokenProvider_.getToken(u),this.appCheckTokenProvider_.getToken(u)]);s?b("getToken() completed but was canceled"):(b("getToken() completed. Creating connection."),this.authToken_=o&&o.accessToken,this.appCheckToken_=l&&l.token,a=new Ge(r,this.repoInfo_,this.applicationId_,this.appCheckToken_,this.authToken_,e,t,n,e=>{k(e+" ("+this.repoInfo_.toString()+")"),this.interrupt(wt)},i))}catch(e){this.log_("Failed to get token: "+e),s||(this.repoInfo_.nodeAdmin&&k(e),c())}}}interrupt(e){b("Interrupting connection for reason: "+e),this.interruptReasons_[e]=!0,this.realtime_?this.realtime_.close():(this.establishConnectionTimer_&&(clearTimeout(this.establishConnectionTimer_),this.establishConnectionTimer_=null),this.connected_&&this.onRealtimeDisconnect_())}resume(e){b("Resuming connection for reason: "+e),delete this.interruptReasons_[e],Object(o.o)(this.interruptReasons_)&&(this.reconnectDelay_=mt,this.realtime_||this.scheduleConnect_(0))}handleTimestamp_(e){const t=e-(new Date).getTime();this.onServerInfoUpdate_({serverTimeOffset:t})}cancelSentTransactions_(){for(let e=0;e<this.outstandingPuts_.length;e++){const t=this.outstandingPuts_[e];t&&"h"in t.request&&t.queued&&(t.onComplete&&t.onComplete("disconnect"),delete this.outstandingPuts_[e],this.outstandingPutCount_--)}0===this.outstandingPutCount_&&(this.outstandingPuts_=[])}onListenRevoked_(e,t){let n;n=t?t.map(e=>R(e)).join("$"):"default";const r=this.removeListen_(e,n);r&&r.onComplete&&r.onComplete("permission_denied")}removeListen_(e,t){const n=new et(e).toString();let r;if(this.listens.has(n)){const e=this.listens.get(n);r=e.get(t),e.delete(t),0===e.size&&this.listens.delete(n)}else r=void 0;return r}onAuthRevoked_(e,t){b("Auth token revoked: "+e+"/"+t),this.authToken_=null,this.forceTokenRefresh_=!0,this.realtime_.close(),"invalid_token"!==e&&"permission_denied"!==e||(this.invalidAuthTokenCount_++,this.invalidAuthTokenCount_>=Tt&&(this.reconnectDelay_=yt,this.authTokenProvider_.notifyForInvalidToken()))}onAppCheckRevoked_(e,t){b("App check token revoked: "+e+"/"+t),this.appCheckToken_=null,this.forceTokenRefresh_=!0,"invalid_token"!==e&&"permission_denied"!==e||(this.invalidAppCheckTokenCount_++,this.invalidAppCheckTokenCount_>=Tt&&this.appCheckTokenProvider_.notifyForInvalidToken())}onSecurityDebugPacket_(e){this.securityDebugCallback_?this.securityDebugCallback_(e):"msg"in e&&console.log("FIREBASE: "+e.msg.replace("\n","\nFIREBASE: "))}restoreState_(){this.tryAuth(),this.tryAppCheck();for(const e of this.listens.values())for(const t of e.values())this.sendListen_(t);for(let e=0;e<this.outstandingPuts_.length;e++)this.outstandingPuts_[e]&&this.sendPut_(e);for(;this.onDisconnectRequestQueue_.length;){const e=this.onDisconnectRequestQueue_.shift();this.sendOnDisconnect_(e.action,e.pathString,e.data,e.onComplete)}for(let e=0;e<this.outstandingGets_.length;e++)this.outstandingGets_[e]&&this.sendGet_(e)}sendConnectStats_(){const e={};let t="js";Object(o.q)()&&(t=this.repoInfo_.nodeAdmin?"admin_node":"node"),e["sdk."+t+"."+l.replace(/\./g,"-")]=1,Object(o.p)()?e["framework.cordova"]=1:Object(o.r)()&&(e["framework.reactnative"]=1),this.reportStats(e)}shouldReconnect_(){const e=Qe.getInstance().currentlyOnline();return Object(o.o)(this.interruptReasons_)&&e}}kt.nextPersistentConnectionId_=0,kt.nextConnectionId_=0;
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class St{constructor(e,t){this.name=e,this.node=t}static Wrap(e,t){return new St(e,t)}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Ot{getCompare(){return this.compare.bind(this)}indexedValueChanged(e,t){const n=new St(E,e),r=new St(E,t);return 0!==this.compare(n,r)}minPost(){return St.MIN}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */let Et;class It extends Ot{static get __EMPTY_NODE(){return Et}static set __EMPTY_NODE(e){Et=e}compare(e,t){return x(e.name,t.name)}isDefinedOn(e){throw Object(o.e)("KeyIndex.isDefinedOn not expected to be called.")}indexedValueChanged(e,t){return!1}minPost(){return St.MIN}maxPost(){return new St(I,Et)}makePost(e,t){return Object(o.d)("string"==typeof e,"KeyIndex indexValue must always be a string."),new St(e,Et)}toString(){return".key"}}const xt=new It;
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Nt{constructor(e,t,n,r,i=null){this.isReverse_=r,this.resultGenerator_=i,this.nodeStack_=[];let o=1;for(;!e.isEmpty();)if(e=e,o=t?n(e.key,t):1,r&&(o*=-1),o<0)e=this.isReverse_?e.left:e.right;else{if(0===o){this.nodeStack_.push(e);break}this.nodeStack_.push(e),e=this.isReverse_?e.right:e.left}}getNext(){if(0===this.nodeStack_.length)return null;let e,t=this.nodeStack_.pop();if(e=this.resultGenerator_?this.resultGenerator_(t.key,t.value):{key:t.key,value:t.value},this.isReverse_)for(t=t.left;!t.isEmpty();)this.nodeStack_.push(t),t=t.right;else for(t=t.right;!t.isEmpty();)this.nodeStack_.push(t),t=t.left;return e}hasNext(){return this.nodeStack_.length>0}peek(){if(0===this.nodeStack_.length)return null;const e=this.nodeStack_[this.nodeStack_.length-1];return this.resultGenerator_?this.resultGenerator_(e.key,e.value):{key:e.key,value:e.value}}}class At{constructor(e,t,n,r,i){this.key=e,this.value=t,this.color=null!=n?n:At.RED,this.left=null!=r?r:Rt.EMPTY_NODE,this.right=null!=i?i:Rt.EMPTY_NODE}copy(e,t,n,r,i){return new At(null!=e?e:this.key,null!=t?t:this.value,null!=n?n:this.color,null!=r?r:this.left,null!=i?i:this.right)}count(){return this.left.count()+1+this.right.count()}isEmpty(){return!1}inorderTraversal(e){return this.left.inorderTraversal(e)||!!e(this.key,this.value)||this.right.inorderTraversal(e)}reverseTraversal(e){return this.right.reverseTraversal(e)||e(this.key,this.value)||this.left.reverseTraversal(e)}min_(){return this.left.isEmpty()?this:this.left.min_()}minKey(){return this.min_().key}maxKey(){return this.right.isEmpty()?this.key:this.right.maxKey()}insert(e,t,n){let r=this;const i=n(e,r.key);return(r=i<0?r.copy(null,null,null,r.left.insert(e,t,n),null):0===i?r.copy(null,t,null,null,null):r.copy(null,null,null,null,r.right.insert(e,t,n))).fixUp_()}removeMin_(){if(this.left.isEmpty())return Rt.EMPTY_NODE;let e=this;return e.left.isRed_()||e.left.left.isRed_()||(e=e.moveRedLeft_()),(e=e.copy(null,null,null,e.left.removeMin_(),null)).fixUp_()}remove(e,t){let n,r;if(t(e,(n=this).key)<0)n.left.isEmpty()||n.left.isRed_()||n.left.left.isRed_()||(n=n.moveRedLeft_()),n=n.copy(null,null,null,n.left.remove(e,t),null);else{if(n.left.isRed_()&&(n=n.rotateRight_()),n.right.isEmpty()||n.right.isRed_()||n.right.left.isRed_()||(n=n.moveRedRight_()),0===t(e,n.key)){if(n.right.isEmpty())return Rt.EMPTY_NODE;r=n.right.min_(),n=n.copy(r.key,r.value,null,null,n.right.removeMin_())}n=n.copy(null,null,null,null,n.right.remove(e,t))}return n.fixUp_()}isRed_(){return this.color}fixUp_(){let e=this;return e.right.isRed_()&&!e.left.isRed_()&&(e=e.rotateLeft_()),e.left.isRed_()&&e.left.left.isRed_()&&(e=e.rotateRight_()),e.left.isRed_()&&e.right.isRed_()&&(e=e.colorFlip_()),e}moveRedLeft_(){let e=this.colorFlip_();return e.right.left.isRed_()&&(e=(e=(e=e.copy(null,null,null,null,e.right.rotateRight_())).rotateLeft_()).colorFlip_()),e}moveRedRight_(){let e=this.colorFlip_();return e.left.left.isRed_()&&(e=(e=e.rotateRight_()).colorFlip_()),e}rotateLeft_(){const e=this.copy(null,null,At.RED,null,this.right.left);return this.right.copy(null,null,this.color,e,null)}rotateRight_(){const e=this.copy(null,null,At.RED,this.left.right,null);return this.left.copy(null,null,this.color,null,e)}colorFlip_(){const e=this.left.copy(null,null,!this.left.color,null,null),t=this.right.copy(null,null,!this.right.color,null,null);return this.copy(null,null,!this.color,e,t)}checkMaxDepth_(){const e=this.check_();return Math.pow(2,e)<=this.count()+1}check_(){if(this.isRed_()&&this.left.isRed_())throw new Error("Red node has red child("+this.key+","+this.value+")");if(this.right.isRed_())throw new Error("Right child of ("+this.key+","+this.value+") is red");const e=this.left.check_();if(e!==this.right.check_())throw new Error("Black depths differ");return e+(this.isRed_()?0:1)}}At.RED=!0,At.BLACK=!1;class Rt{constructor(e,t=Rt.EMPTY_NODE){this.comparator_=e,this.root_=t}insert(e,t){return new Rt(this.comparator_,this.root_.insert(e,t,this.comparator_).copy(null,null,At.BLACK,null,null))}remove(e){return new Rt(this.comparator_,this.root_.remove(e,this.comparator_).copy(null,null,At.BLACK,null,null))}get(e){let t,n=this.root_;for(;!n.isEmpty();){if(0===(t=this.comparator_(e,n.key)))return n.value;t<0?n=n.left:t>0&&(n=n.right)}return null}getPredecessorKey(e){let t,n=this.root_,r=null;for(;!n.isEmpty();){if(0===(t=this.comparator_(e,n.key))){if(n.left.isEmpty())return r?r.key:null;for(n=n.left;!n.right.isEmpty();)n=n.right;return n.key}t<0?n=n.left:t>0&&(r=n,n=n.right)}throw new Error("Attempted to find predecessor key for a nonexistent key.  What gives?")}isEmpty(){return this.root_.isEmpty()}count(){return this.root_.count()}minKey(){return this.root_.minKey()}maxKey(){return this.root_.maxKey()}inorderTraversal(e){return this.root_.inorderTraversal(e)}reverseTraversal(e){return this.root_.reverseTraversal(e)}getIterator(e){return new Nt(this.root_,null,this.comparator_,!1,e)}getIteratorFrom(e,t){return new Nt(this.root_,e,this.comparator_,!1,t)}getReverseIteratorFrom(e,t){return new Nt(this.root_,e,this.comparator_,!0,t)}getReverseIterator(e){return new Nt(this.root_,null,this.comparator_,!0,e)}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
function Pt(e,t){return x(e.name,t.name)}function Dt(e,t){return x(e,t)}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */let jt;Rt.EMPTY_NODE=new class{copy(e,t,n,r,i){return this}insert(e,t,n){return new At(e,t,null)}remove(e,t){return this}count(){return 0}isEmpty(){return!0}inorderTraversal(e){return!1}reverseTraversal(e){return!1}minKey(){return null}maxKey(){return null}check_(){return 0}isRed_(){return!1}};const $t=function(e){return"number"==typeof e?"number:"+j(e):"string:"+e},Mt=function(e){if(e.isLeafNode()){const t=e.val();Object(o.d)("string"==typeof t||"number"==typeof t||"object"==typeof t&&Object(o.h)(t,".sv"),"Priority must be a string or number.")}else Object(o.d)(e===jt||e.isEmpty(),"priority of unexpected type.");Object(o.d)(e===jt||e.getPriority().isEmpty(),"Priority nodes can't have a priority of their own.")};
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let Lt,Ft,qt;class Ut{constructor(e,t=Ut.__childrenNodeConstructor.EMPTY_NODE){this.value_=e,this.priorityNode_=t,this.lazyHash_=null,Object(o.d)(void 0!==this.value_&&null!==this.value_,"LeafNode shouldn't be created with null/undefined value."),Mt(this.priorityNode_)}static set __childrenNodeConstructor(e){Lt=e}static get __childrenNodeConstructor(){return Lt}isLeafNode(){return!0}getPriority(){return this.priorityNode_}updatePriority(e){return new Ut(this.value_,e)}getImmediateChild(e){return".priority"===e?this.priorityNode_:Ut.__childrenNodeConstructor.EMPTY_NODE}getChild(e){return lt(e)?this:".priority"===nt(e)?this.priorityNode_:Ut.__childrenNodeConstructor.EMPTY_NODE}hasChild(){return!1}getPredecessorChildName(e,t){return null}updateImmediateChild(e,t){return".priority"===e?this.updatePriority(t):t.isEmpty()&&".priority"!==e?this:Ut.__childrenNodeConstructor.EMPTY_NODE.updateImmediateChild(e,t).updatePriority(this.priorityNode_)}updateChild(e,t){const n=nt(e);return null===n?t:t.isEmpty()&&".priority"!==n?this:(Object(o.d)(".priority"!==n||1===rt(e),".priority must be the last token in a path"),this.updateImmediateChild(n,Ut.__childrenNodeConstructor.EMPTY_NODE.updateChild(it(e),t)))}isEmpty(){return!1}numChildren(){return 0}forEachChild(e,t){return!1}val(e){return e&&!this.getPriority().isEmpty()?{".value":this.getValue(),".priority":this.getPriority().val()}:this.getValue()}hash(){if(null===this.lazyHash_){let e="";this.priorityNode_.isEmpty()||(e+="priority:"+$t(this.priorityNode_.val())+":");const t=typeof this.value_;e+=t+":",e+="number"===t?j(this.value_):this.value_,this.lazyHash_=_(e)}return this.lazyHash_}getValue(){return this.value_}compareTo(e){return e===Ut.__childrenNodeConstructor.EMPTY_NODE?1:e instanceof Ut.__childrenNodeConstructor?-1:(Object(o.d)(e.isLeafNode(),"Unknown node type"),this.compareToLeafNode_(e))}compareToLeafNode_(e){const t=typeof e.value_,n=typeof this.value_,r=Ut.VALUE_TYPE_ORDER.indexOf(t),i=Ut.VALUE_TYPE_ORDER.indexOf(n);return Object(o.d)(r>=0,"Unknown leaf type: "+t),Object(o.d)(i>=0,"Unknown leaf type: "+n),r===i?"object"===n?0:this.value_<e.value_?-1:this.value_===e.value_?0:1:i-r}withIndex(){return this}isIndexed(){return!0}equals(e){if(e===this)return!0;if(e.isLeafNode()){const t=e;return this.value_===t.value_&&this.priorityNode_.equals(t.priorityNode_)}return!1}}Ut.VALUE_TYPE_ORDER=["object","boolean","number","string"];const Wt=new class extends Ot{compare(e,t){const n=e.node.getPriority(),r=t.node.getPriority(),i=n.compareTo(r);return 0===i?x(e.name,t.name):i}isDefinedOn(e){return!e.getPriority().isEmpty()}indexedValueChanged(e,t){return!e.getPriority().equals(t.getPriority())}minPost(){return St.MIN}maxPost(){return new St(I,new Ut("[PRIORITY-POST]",qt))}makePost(e,t){const n=Ft(e);return new St(t,new Ut("[PRIORITY-POST]",n))}toString(){return".priority"}},Ht=Math.log(2);
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const zt=function(e,t,n,r){e.sort(t);const i=function(t,r){const o=r-t;let s,a;if(0===o)return null;if(1===o)return s=e[t],a=n?n(s):s,new At(a,s.node,At.BLACK,null,null);{const c=parseInt(o/2,10)+t,l=i(t,c),u=i(c+1,r);return s=e[c],a=n?n(s):s,new At(a,s.node,At.BLACK,l,u)}},o=function(t){let r=null,o=null,s=e.length;const a=function(t,r){const o=s-t,a=s;s-=t;const l=i(o+1,a),u=e[o],h=n?n(u):u;c(new At(h,u.node,r,null,l))},c=function(e){r?(r.left=e,r=e):(o=e,r=e)};for(let e=0;e<t.count;++e){const n=t.nextBitIsOne(),r=Math.pow(2,t.count-(e+1));n?a(r,At.BLACK):(a(r,At.BLACK),a(r,At.RED))}return o}(new class{constructor(e){this.count=(e=>parseInt(Math.log(e)/Ht,10))(e+1),this.current_=this.count-1;const t=(e=>parseInt(Array(e+1).join("1"),2))(this.count);this.bits_=e+1&t}nextBitIsOne(){const e=!(this.bits_&1<<this.current_);return this.current_--,e}}(e.length));return new Rt(r||t,o)};
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */let Bt;const Vt={};class Kt{constructor(e,t){this.indexes_=e,this.indexSet_=t}static get Default(){return Object(o.d)(Vt&&Wt,"ChildrenNode.ts has not been loaded"),Bt=Bt||new Kt({".priority":Vt},{".priority":Wt})}get(e){const t=Object(o.w)(this.indexes_,e);if(!t)throw new Error("No index defined for "+e);return t instanceof Rt?t:null}hasIndex(e){return Object(o.h)(this.indexSet_,e.toString())}addIndex(e,t){Object(o.d)(e!==xt,"KeyIndex always exists and isn't meant to be added to the IndexMap.");const n=[];let r=!1;const i=t.getIterator(St.Wrap);let s,a=i.getNext();for(;a;)r=r||e.isDefinedOn(a.node),n.push(a),a=i.getNext();s=r?zt(n,e.getCompare()):Vt;const c=e.toString(),l=Object.assign({},this.indexSet_);l[c]=e;const u=Object.assign({},this.indexes_);return u[c]=s,new Kt(u,l)}addToIndexes(e,t){const n=Object(o.u)(this.indexes_,(n,r)=>{const i=Object(o.w)(this.indexSet_,r);if(Object(o.d)(i,"Missing index implementation for "+r),n===Vt){if(i.isDefinedOn(e.node)){const n=[],r=t.getIterator(St.Wrap);let o=r.getNext();for(;o;)o.name!==e.name&&n.push(o),o=r.getNext();return n.push(e),zt(n,i.getCompare())}return Vt}{const r=t.get(e.name);let i=n;return r&&(i=i.remove(new St(e.name,r))),i.insert(e,e.node)}});return new Kt(n,this.indexSet_)}removeFromIndexes(e,t){const n=Object(o.u)(this.indexes_,n=>{if(n===Vt)return n;{const r=t.get(e.name);return r?n.remove(new St(e.name,r)):n}});return new Kt(n,this.indexSet_)}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */let Gt;class Yt{constructor(e,t,n){this.children_=e,this.priorityNode_=t,this.indexMap_=n,this.lazyHash_=null,this.priorityNode_&&Mt(this.priorityNode_),this.children_.isEmpty()&&Object(o.d)(!this.priorityNode_||this.priorityNode_.isEmpty(),"An empty node cannot have a priority")}static get EMPTY_NODE(){return Gt||(Gt=new Yt(new Rt(Dt),null,Kt.Default))}isLeafNode(){return!1}getPriority(){return this.priorityNode_||Gt}updatePriority(e){return this.children_.isEmpty()?this:new Yt(this.children_,e,this.indexMap_)}getImmediateChild(e){if(".priority"===e)return this.getPriority();{const t=this.children_.get(e);return null===t?Gt:t}}getChild(e){const t=nt(e);return null===t?this:this.getImmediateChild(t).getChild(it(e))}hasChild(e){return null!==this.children_.get(e)}updateImmediateChild(e,t){if(Object(o.d)(t,"We should always be passing snapshot nodes"),".priority"===e)return this.updatePriority(t);{const n=new St(e,t);let r,i;t.isEmpty()?(r=this.children_.remove(e),i=this.indexMap_.removeFromIndexes(n,this.children_)):(r=this.children_.insert(e,t),i=this.indexMap_.addToIndexes(n,this.children_));const o=r.isEmpty()?Gt:this.priorityNode_;return new Yt(r,o,i)}}updateChild(e,t){const n=nt(e);if(null===n)return t;{Object(o.d)(".priority"!==nt(e)||1===rt(e),".priority must be the last token in a path");const r=this.getImmediateChild(n).updateChild(it(e),t);return this.updateImmediateChild(n,r)}}isEmpty(){return this.children_.isEmpty()}numChildren(){return this.children_.count()}val(e){if(this.isEmpty())return null;const t={};let n=0,r=0,i=!0;if(this.forEachChild(Wt,(o,s)=>{t[o]=s.val(e),n++,i&&Yt.INTEGER_REGEXP_.test(o)?r=Math.max(r,Number(o)):i=!1}),!e&&i&&r<2*n){const e=[];for(const n in t)e[n]=t[n];return e}return e&&!this.getPriority().isEmpty()&&(t[".priority"]=this.getPriority().val()),t}hash(){if(null===this.lazyHash_){let e="";this.getPriority().isEmpty()||(e+="priority:"+$t(this.getPriority().val())+":"),this.forEachChild(Wt,(t,n)=>{const r=n.hash();""!==r&&(e+=":"+t+":"+r)}),this.lazyHash_=""===e?"":_(e)}return this.lazyHash_}getPredecessorChildName(e,t,n){const r=this.resolveIndex_(n);if(r){const n=r.getPredecessorKey(new St(e,t));return n?n.name:null}return this.children_.getPredecessorKey(e)}getFirstChildName(e){const t=this.resolveIndex_(e);if(t){const e=t.minKey();return e&&e.name}return this.children_.minKey()}getFirstChild(e){const t=this.getFirstChildName(e);return t?new St(t,this.children_.get(t)):null}getLastChildName(e){const t=this.resolveIndex_(e);if(t){const e=t.maxKey();return e&&e.name}return this.children_.maxKey()}getLastChild(e){const t=this.getLastChildName(e);return t?new St(t,this.children_.get(t)):null}forEachChild(e,t){const n=this.resolveIndex_(e);return n?n.inorderTraversal(e=>t(e.name,e.node)):this.children_.inorderTraversal(t)}getIterator(e){return this.getIteratorFrom(e.minPost(),e)}getIteratorFrom(e,t){const n=this.resolveIndex_(t);if(n)return n.getIteratorFrom(e,e=>e);{const n=this.children_.getIteratorFrom(e.name,St.Wrap);let r=n.peek();for(;null!=r&&t.compare(r,e)<0;)n.getNext(),r=n.peek();return n}}getReverseIterator(e){return this.getReverseIteratorFrom(e.maxPost(),e)}getReverseIteratorFrom(e,t){const n=this.resolveIndex_(t);if(n)return n.getReverseIteratorFrom(e,e=>e);{const n=this.children_.getReverseIteratorFrom(e.name,St.Wrap);let r=n.peek();for(;null!=r&&t.compare(r,e)>0;)n.getNext(),r=n.peek();return n}}compareTo(e){return this.isEmpty()?e.isEmpty()?0:-1:e.isLeafNode()||e.isEmpty()?1:e===Jt?-1:0}withIndex(e){if(e===xt||this.indexMap_.hasIndex(e))return this;{const t=this.indexMap_.addIndex(e,this.children_);return new Yt(this.children_,this.priorityNode_,t)}}isIndexed(e){return e===xt||this.indexMap_.hasIndex(e)}equals(e){if(e===this)return!0;if(e.isLeafNode())return!1;{const t=e;if(this.getPriority().equals(t.getPriority())){if(this.children_.count()===t.children_.count()){const e=this.getIterator(Wt),n=t.getIterator(Wt);let r=e.getNext(),i=n.getNext();for(;r&&i;){if(r.name!==i.name||!r.node.equals(i.node))return!1;r=e.getNext(),i=n.getNext()}return null===r&&null===i}return!1}return!1}}resolveIndex_(e){return e===xt?null:this.indexMap_.get(e.toString())}}Yt.INTEGER_REGEXP_=/^(0|[1-9]\d*)$/;const Jt=new class extends Yt{constructor(){super(new Rt(Dt),Yt.EMPTY_NODE,Kt.Default)}compareTo(e){return e===this?0:1}equals(e){return e===this}getPriority(){return this}getImmediateChild(e){return Yt.EMPTY_NODE}isEmpty(){return!1}};Object.defineProperties(St,{MIN:{value:new St(E,Yt.EMPTY_NODE)},MAX:{value:new St(I,Jt)}}),It.__EMPTY_NODE=Yt.EMPTY_NODE,Ut.__childrenNodeConstructor=Yt,jt=Jt,function(e){qt=e}(Jt);
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const Qt=!0;function Xt(e,t=null){if(null===e)return Yt.EMPTY_NODE;if("object"==typeof e&&".priority"in e&&(t=e[".priority"]),Object(o.d)(null===t||"string"==typeof t||"number"==typeof t||"object"==typeof t&&".sv"in t,"Invalid priority type found: "+typeof t),"object"==typeof e&&".value"in e&&null!==e[".value"]&&(e=e[".value"]),"object"!=typeof e||".sv"in e){return new Ut(e,Xt(t))}if(e instanceof Array||!Qt){let n=Yt.EMPTY_NODE;return D(e,(t,r)=>{if(Object(o.h)(e,t)&&"."!==t.substring(0,1)){const e=Xt(r);!e.isLeafNode()&&e.isEmpty()||(n=n.updateImmediateChild(t,e))}}),n.updatePriority(Xt(t))}{const n=[];let r=!1;if(D(e,(e,t)=>{if("."!==e.substring(0,1)){const i=Xt(t);i.isEmpty()||(r=r||!i.getPriority().isEmpty(),n.push(new St(e,i)))}}),0===n.length)return Yt.EMPTY_NODE;const i=zt(n,Pt,e=>e.name,Dt);if(r){const e=zt(n,Wt.getCompare());return new Yt(i,Xt(t),new Kt({".priority":e},{".priority":Wt}))}return new Yt(i,Xt(t),Kt.Default)}}!function(e){Ft=e}(Xt);
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class Zt extends Ot{constructor(e){super(),this.indexPath_=e,Object(o.d)(!lt(e)&&".priority"!==nt(e),"Can't create PathIndex with empty path or .priority key")}extractChild(e){return e.getChild(this.indexPath_)}isDefinedOn(e){return!e.getChild(this.indexPath_).isEmpty()}compare(e,t){const n=this.extractChild(e.node),r=this.extractChild(t.node),i=n.compareTo(r);return 0===i?x(e.name,t.name):i}makePost(e,t){const n=Xt(e),r=Yt.EMPTY_NODE.updateChild(this.indexPath_,n);return new St(t,r)}maxPost(){const e=Yt.EMPTY_NODE.updateChild(this.indexPath_,Jt);return new St(I,e)}toString(){return st(this.indexPath_,0).join("/")}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const en=new class extends Ot{compare(e,t){const n=e.node.compareTo(t.node);return 0===n?x(e.name,t.name):n}isDefinedOn(e){return!0}indexedValueChanged(e,t){return!e.equals(t)}minPost(){return St.MIN}maxPost(){return St.MAX}makePost(e,t){const n=Xt(e);return new St(t,n)}toString(){return".value"}},tn="-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz";
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */!function(){let e=0;const t=[]}();
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
function nn(e){return{type:"value",snapshotNode:e}}function rn(e,t){return{type:"child_added",snapshotNode:t,childName:e}}function on(e,t){return{type:"child_removed",snapshotNode:t,childName:e}}function sn(e,t,n){return{type:"child_changed",snapshotNode:t,childName:e,oldSnap:n}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class an{constructor(){this.limitSet_=!1,this.startSet_=!1,this.startNameSet_=!1,this.startAfterSet_=!1,this.endSet_=!1,this.endNameSet_=!1,this.endBeforeSet_=!1,this.limit_=0,this.viewFrom_="",this.indexStartValue_=null,this.indexStartName_="",this.indexEndValue_=null,this.indexEndName_="",this.index_=Wt}hasStart(){return this.startSet_}hasStartAfter(){return this.startAfterSet_}hasEndBefore(){return this.endBeforeSet_}isViewFromLeft(){return""===this.viewFrom_?this.startSet_:"l"===this.viewFrom_}getIndexStartValue(){return Object(o.d)(this.startSet_,"Only valid if start has been set"),this.indexStartValue_}getIndexStartName(){return Object(o.d)(this.startSet_,"Only valid if start has been set"),this.startNameSet_?this.indexStartName_:E}hasEnd(){return this.endSet_}getIndexEndValue(){return Object(o.d)(this.endSet_,"Only valid if end has been set"),this.indexEndValue_}getIndexEndName(){return Object(o.d)(this.endSet_,"Only valid if end has been set"),this.endNameSet_?this.indexEndName_:I}hasLimit(){return this.limitSet_}hasAnchoredLimit(){return this.limitSet_&&""!==this.viewFrom_}getLimit(){return Object(o.d)(this.limitSet_,"Only valid if limit has been set"),this.limit_}getIndex(){return this.index_}loadsAllData(){return!(this.startSet_||this.endSet_||this.limitSet_)}isDefault(){return this.loadsAllData()&&this.index_===Wt}copy(){const e=new an;return e.limitSet_=this.limitSet_,e.limit_=this.limit_,e.startSet_=this.startSet_,e.indexStartValue_=this.indexStartValue_,e.startNameSet_=this.startNameSet_,e.indexStartName_=this.indexStartName_,e.endSet_=this.endSet_,e.indexEndValue_=this.indexEndValue_,e.endNameSet_=this.endNameSet_,e.indexEndName_=this.indexEndName_,e.index_=this.index_,e.viewFrom_=this.viewFrom_,e}}function cn(e){const t={};if(e.isDefault())return t;let n;return e.index_===Wt?n="$priority":e.index_===en?n="$value":e.index_===xt?n="$key":(Object(o.d)(e.index_ instanceof Zt,"Unrecognized index type!"),n=e.index_.toString()),t.orderBy=Object(o.z)(n),e.startSet_&&(t.startAt=Object(o.z)(e.indexStartValue_),e.startNameSet_&&(t.startAt+=","+Object(o.z)(e.indexStartName_))),e.endSet_&&(t.endAt=Object(o.z)(e.indexEndValue_),e.endNameSet_&&(t.endAt+=","+Object(o.z)(e.indexEndName_))),e.limitSet_&&(e.isViewFromLeft()?t.limitToFirst=e.limit_:t.limitToLast=e.limit_),t}function ln(e){const t={};if(e.startSet_&&(t.sp=e.indexStartValue_,e.startNameSet_&&(t.sn=e.indexStartName_)),e.endSet_&&(t.ep=e.indexEndValue_,e.endNameSet_&&(t.en=e.indexEndName_)),e.limitSet_){t.l=e.limit_;let n=e.viewFrom_;""===n&&(n=e.isViewFromLeft()?"l":"r"),t.vf=n}return e.index_!==Wt&&(t.i=e.index_.toString()),t}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class un extends Ye{constructor(e,t,n,r){super(),this.repoInfo_=e,this.onDataUpdate_=t,this.authTokenProvider_=n,this.appCheckTokenProvider_=r,this.log_=C("p:rest:"),this.listens_={}}reportStats(e){throw new Error("Method not implemented.")}static getListenId_(e,t){return void 0!==t?"tag$"+t:(Object(o.d)(e._queryParams.isDefault(),"should have a tag if it's not a default query."),e._path.toString())}listen(e,t,n,r){const i=e._path.toString();this.log_("Listen called for "+i+" "+e._queryIdentifier);const s=un.getListenId_(e,n),a={};this.listens_[s]=a;const c=cn(e._queryParams);this.restRequest_(i+".json",c,(e,t)=>{let c=t;if(404===e&&(c=null,e=null),null===e&&this.onDataUpdate_(i,c,!1,n),Object(o.w)(this.listens_,s)===a){let t;r(t=e?401===e?"permission_denied":"rest_error:"+e:"ok",null)}})}unlisten(e,t){const n=un.getListenId_(e,t);delete this.listens_[n]}get(e){const t=cn(e._queryParams),n=e._path.toString(),r=new o.a;return this.restRequest_(n+".json",t,(e,t)=>{let i=t;404===e&&(i=null,e=null),null===e?(this.onDataUpdate_(n,i,!1,null),r.resolve(i)):r.reject(new Error(i))}),r.promise}refreshAuthToken(e){}restRequest_(e,t={},n){return t.format="export",Promise.all([this.authTokenProvider_.getToken(!1),this.appCheckTokenProvider_.getToken(!1)]).then(([r,i])=>{r&&r.accessToken&&(t.auth=r.accessToken),i&&i.token&&(t.ac=i.token);const s=(this.repoInfo_.secure?"https://":"http://")+this.repoInfo_.host+e+"?ns="+this.repoInfo_.namespace+Object(o.v)(t);this.log_("Sending REST request for "+s);const a=new XMLHttpRequest;a.onreadystatechange=(()=>{if(n&&4===a.readyState){this.log_("REST Response for "+s+" received. status:",a.status,"response:",a.responseText);let e=null;if(a.status>=200&&a.status<300){try{e=Object(o.t)(a.responseText)}catch(e){k("Failed to parse JSON response for "+s+": "+a.responseText)}n(null,e)}else 401!==a.status&&404!==a.status&&k("Got unsuccessful REST response for "+s+" Status: "+a.status),n(a.status);n=null}}),a.open("GET",s,!0),a.send()})}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class hn{constructor(){this.rootNode_=Yt.EMPTY_NODE}getNode(e){return this.rootNode_.getChild(e)}updateSnapshot(e,t){this.rootNode_=this.rootNode_.updateChild(e,t)}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function dn(){return{value:null,children:new Map}}function fn(e,t,n){if(lt(t))e.value=n,e.children.clear();else if(null!==e.value)e.value=e.value.updateChild(t,n);else{const r=nt(t);e.children.has(r)||e.children.set(r,dn()),fn(e.children.get(r),t=it(t),n)}}function pn(e,t,n){null!==e.value?n(t,e.value):function(e,t){e.children.forEach((e,n)=>{t(n,e)})}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */(e,(e,r)=>{pn(r,new et(t.toString()+"/"+e),n)})}class _n{constructor(e){this.collection_=e,this.last_=null}get(){const e=this.collection_.get(),t=Object.assign({},e);return this.last_&&D(this.last_,(e,n)=>{t[e]=t[e]-n}),this.last_=e,t}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const mn=1e4,vn=3e4,gn=3e5;class yn{constructor(e,t){this.server_=t,this.statsToReport_={},this.statsListener_=new _n(e);const n=mn+(vn-mn)*Math.random();W(this.reportStats_.bind(this),Math.floor(n))}reportStats_(){const e={};let t=!1;D(this.statsListener_.get(),(n,r)=>{r>0&&Object(o.h)(this.statsToReport_,n)&&(e[n]=r,t=!0)}),t&&this.server_.reportStats(e),W(this.reportStats_.bind(this),Math.floor(2*Math.random()*gn))}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */var bn;function Cn(e){return{fromUser:!1,fromServer:!0,queryId:e,tagged:!0}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */!function(e){e[e.OVERWRITE=0]="OVERWRITE",e[e.MERGE=1]="MERGE",e[e.ACK_USER_WRITE=2]="ACK_USER_WRITE",e[e.LISTEN_COMPLETE=3]="LISTEN_COMPLETE"}(bn||(bn={}));class wn{constructor(e,t,n){this.path=e,this.affectedTree=t,this.revert=n,this.type=bn.ACK_USER_WRITE,this.source={fromUser:!0,fromServer:!1,queryId:null,tagged:!1}}operationForChild(e){if(lt(this.path)){if(null!=this.affectedTree.value)return Object(o.d)(this.affectedTree.children.isEmpty(),"affectedTree should not have overlapping affected paths."),this;{const t=this.affectedTree.subtree(new et(e));return new wn(tt(),t,this.revert)}}return Object(o.d)(nt(this.path)===e,"operationForChild called for unrelated child."),new wn(it(this.path),this.affectedTree,this.revert)}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class Tn{constructor(e,t,n){this.source=e,this.path=t,this.snap=n,this.type=bn.OVERWRITE}operationForChild(e){return lt(this.path)?new Tn(this.source,tt(),this.snap.getImmediateChild(e)):new Tn(this.source,it(this.path),this.snap)}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class kn{constructor(e,t,n){this.source=e,this.path=t,this.children=n,this.type=bn.MERGE}operationForChild(e){if(lt(this.path)){const t=this.children.subtree(new et(e));return t.isEmpty()?null:t.value?new Tn(this.source,tt(),t.value):new kn(this.source,tt(),t)}return Object(o.d)(nt(this.path)===e,"Can't get a merge for a child not on the path of the operation"),new kn(this.source,it(this.path),this.children)}toString(){return"Operation("+this.path+": "+this.source.toString()+" merge: "+this.children.toString()+")"}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class Sn{constructor(e,t,n){this.node_=e,this.fullyInitialized_=t,this.filtered_=n}isFullyInitialized(){return this.fullyInitialized_}isFiltered(){return this.filtered_}isCompleteForPath(e){if(lt(e))return this.isFullyInitialized()&&!this.filtered_;const t=nt(e);return this.isCompleteForChild(t)}isCompleteForChild(e){return this.isFullyInitialized()&&!this.filtered_||this.node_.hasChild(e)}getNode(){return this.node_}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function On(e,t,n,r,i,s){const a=r.filter(e=>e.type===n);a.sort((t,n)=>(function(e,t,n){if(null==t.childName||null==n.childName)throw Object(o.e)("Should only compare child_ events.");const r=new St(t.childName,t.snapshotNode),i=new St(n.childName,n.snapshotNode);return e.index_.compare(r,i)}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */)(e,t,n)),a.forEach(n=>{const r=function(e,t,n){return"value"===t.type||"child_removed"===t.type?t:(t.prevName=n.getPredecessorChildName(t.childName,t.snapshotNode,e.index_),t)}(e,n,s);i.forEach(i=>{i.respondsTo(n.type)&&t.push(i.createEvent(r,e.query_))})})}function En(e,t){return{eventCache:e,serverCache:t}}function In(e,t,n,r){return En(new Sn(t,n,r),e.serverCache)}function xn(e,t,n,r){return En(e.eventCache,new Sn(t,n,r))}function Nn(e){return e.eventCache.isFullyInitialized()?e.eventCache.getNode():null}function An(e){return e.serverCache.isFullyInitialized()?e.serverCache.getNode():null}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */let Rn;const Pn=()=>(Rn||(Rn=new Rt(N)),Rn);class Dn{constructor(e,t=Pn()){this.value=e,this.children=t}static fromObject(e){let t=new Dn(null);return D(e,(e,n)=>{t=t.set(new et(e),n)}),t}isEmpty(){return null===this.value&&this.children.isEmpty()}findRootMostMatchingPathAndValue(e,t){if(null!=this.value&&t(this.value))return{path:tt(),value:this.value};if(lt(e))return null;{const n=nt(e),r=this.children.get(n);if(null!==r){const i=r.findRootMostMatchingPathAndValue(it(e),t);if(null!=i){return{path:ct(new et(n),i.path),value:i.value}}return null}return null}}findRootMostValueAndPath(e){return this.findRootMostMatchingPathAndValue(e,()=>!0)}subtree(e){if(lt(e))return this;{const t=nt(e),n=this.children.get(t);return null!==n?n.subtree(it(e)):new Dn(null)}}set(e,t){if(lt(e))return new Dn(t,this.children);{const n=nt(e),r=(this.children.get(n)||new Dn(null)).set(it(e),t),i=this.children.insert(n,r);return new Dn(this.value,i)}}remove(e){if(lt(e))return this.children.isEmpty()?new Dn(null):new Dn(null,this.children);{const t=nt(e),n=this.children.get(t);if(n){const r=n.remove(it(e));let i;return i=r.isEmpty()?this.children.remove(t):this.children.insert(t,r),null===this.value&&i.isEmpty()?new Dn(null):new Dn(this.value,i)}return this}}get(e){if(lt(e))return this.value;{const t=nt(e),n=this.children.get(t);return n?n.get(it(e)):null}}setTree(e,t){if(lt(e))return t;{const n=nt(e),r=(this.children.get(n)||new Dn(null)).setTree(it(e),t);let i;return i=r.isEmpty()?this.children.remove(n):this.children.insert(n,r),new Dn(this.value,i)}}fold(e){return this.fold_(tt(),e)}fold_(e,t){const n={};return this.children.inorderTraversal((r,i)=>{n[r]=i.fold_(ct(e,r),t)}),t(e,this.value,n)}findOnPath(e,t){return this.findOnPath_(e,tt(),t)}findOnPath_(e,t,n){const r=!!this.value&&n(t,this.value);if(r)return r;if(lt(e))return null;{const r=nt(e),i=this.children.get(r);return i?i.findOnPath_(it(e),ct(t,r),n):null}}foreachOnPath(e,t){return this.foreachOnPath_(e,tt(),t)}foreachOnPath_(e,t,n){if(lt(e))return this;{this.value&&n(t,this.value);const r=nt(e),i=this.children.get(r);return i?i.foreachOnPath_(it(e),ct(t,r),n):new Dn(null)}}foreach(e){this.foreach_(tt(),e)}foreach_(e,t){this.children.inorderTraversal((n,r)=>{r.foreach_(ct(e,n),t)}),this.value&&t(e,this.value)}foreachChild(e){this.children.inorderTraversal((t,n)=>{n.value&&e(t,n.value)})}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class jn{constructor(e){this.writeTree_=e}static empty(){return new jn(new Dn(null))}}function $n(e,t,n){if(lt(t))return new jn(new Dn(n));{const r=e.writeTree_.findRootMostValueAndPath(t);if(null!=r){const i=r.path;let o=r.value;const s=ut(i,t);return o=o.updateChild(s,n),new jn(e.writeTree_.set(i,o))}{const r=new Dn(n),i=e.writeTree_.setTree(t,r);return new jn(i)}}}function Mn(e,t,n){let r=e;return D(n,(e,n)=>{r=$n(r,ct(t,e),n)}),r}function Ln(e,t){if(lt(t))return jn.empty();{const n=e.writeTree_.setTree(t,new Dn(null));return new jn(n)}}function Fn(e,t){return null!=qn(e,t)}function qn(e,t){const n=e.writeTree_.findRootMostValueAndPath(t);return null!=n?e.writeTree_.get(n.path).getChild(ut(n.path,t)):null}function Un(e){const t=[],n=e.writeTree_.value;return null!=n?n.isLeafNode()||n.forEachChild(Wt,(e,n)=>{t.push(new St(e,n))}):e.writeTree_.children.inorderTraversal((e,n)=>{null!=n.value&&t.push(new St(e,n.value))}),t}function Wn(e,t){if(lt(t))return e;{const n=qn(e,t);return new jn(null!=n?new Dn(n):e.writeTree_.subtree(t))}}function Hn(e){return e.writeTree_.isEmpty()}function zn(e,t){return function e(t,n,r){if(null!=n.value)return r.updateChild(t,n.value);{let i=null;return n.children.inorderTraversal((n,s)=>{".priority"===n?(Object(o.d)(null!==s.value,"Priority writes must always be leaf nodes"),i=s.value):r=e(ct(t,n),s,r)}),r.getChild(t).isEmpty()||null===i||(r=r.updateChild(ct(t,".priority"),i)),r}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */(tt(),e.writeTree_,t)}function Bn(e,t){return ir(t,e)}function Vn(e,t){const n=e.allWrites.findIndex(e=>e.writeId===t);Object(o.d)(n>=0,"removeWrite called with nonexistent writeId.");const r=e.allWrites[n];e.allWrites.splice(n,1);let i=r.visible,s=!1,a=e.allWrites.length-1;for(;i&&a>=0;){const t=e.allWrites[a];t.visible&&(a>=n&&Kn(t,r.path)?i=!1:dt(r.path,t.path)&&(s=!0)),a--}if(i){if(s)return function(e){e.visibleWrites=Yn(e.allWrites,Gn,tt()),e.allWrites.length>0?e.lastWriteId=e.allWrites[e.allWrites.length-1].writeId:e.lastWriteId=-1}(e),!0;if(r.snap)e.visibleWrites=Ln(e.visibleWrites,r.path);else{D(r.children,t=>{e.visibleWrites=Ln(e.visibleWrites,ct(r.path,t))})}return!0}return!1}function Kn(e,t){if(e.snap)return dt(e.path,t);for(const n in e.children)if(e.children.hasOwnProperty(n)&&dt(ct(e.path,n),t))return!0;return!1}function Gn(e){return e.visible}function Yn(e,t,n){let r=jn.empty();for(let i=0;i<e.length;++i){const s=e[i];if(t(s)){const e=s.path;let t;if(s.snap)dt(n,e)?r=$n(r,t=ut(n,e),s.snap):dt(e,n)&&(t=ut(e,n),r=$n(r,tt(),s.snap.getChild(t)));else{if(!s.children)throw Object(o.e)("WriteRecord should have .snap or .children");if(dt(n,e))r=Mn(r,t=ut(n,e),s.children);else if(dt(e,n))if(lt(t=ut(e,n)))r=Mn(r,tt(),s.children);else{const e=Object(o.w)(s.children,nt(t));if(e){const n=e.getChild(it(t));r=$n(r,tt(),n)}}}}}return r}function Jn(e,t,n,r,i){if(r||i){const o=Wn(e.visibleWrites,t);if(!i&&Hn(o))return n;if(i||null!=n||Fn(o,tt())){const o=function(e){return(e.visible||i)&&(!r||!~r.indexOf(e.writeId))&&(dt(e.path,t)||dt(t,e.path))};return zn(Yn(e.allWrites,o,t),n||Yt.EMPTY_NODE)}return null}{const r=qn(e.visibleWrites,t);if(null!=r)return r;{const r=Wn(e.visibleWrites,t);if(Hn(r))return n;if(null!=n||Fn(r,tt())){return zn(r,n||Yt.EMPTY_NODE)}return null}}}function Qn(e,t,n,r){return Jn(e.writeTree,e.treePath,t,n,r)}function Xn(e,t){return function(e,t,n){let r=Yt.EMPTY_NODE;const i=qn(e.visibleWrites,t);if(i)return i.isLeafNode()||i.forEachChild(Wt,(e,t)=>{r=r.updateImmediateChild(e,t)}),r;if(n){const i=Wn(e.visibleWrites,t);return n.forEachChild(Wt,(e,t)=>{const n=zn(Wn(i,new et(e)),t);r=r.updateImmediateChild(e,n)}),Un(i).forEach(e=>{r=r.updateImmediateChild(e.name,e.node)}),r}return Un(Wn(e.visibleWrites,t)).forEach(e=>{r=r.updateImmediateChild(e.name,e.node)}),r}(e.writeTree,e.treePath,t)}function Zn(e,t,n,r){return function(e,t,n,r,i){Object(o.d)(r||i,"Either existingEventSnap or existingServerSnap must exist");const s=ct(t,n);if(Fn(e.visibleWrites,s))return null;{const t=Wn(e.visibleWrites,s);return Hn(t)?i.getChild(n):zn(t,i.getChild(n))}}(e.writeTree,e.treePath,t,n,r)}function er(e,t){return function(e,t){return qn(e.visibleWrites,t)}(e.writeTree,ct(e.treePath,t))}function tr(e,t,n,r,i,o){return function(e,t,n,r,i,o,s){let a;const c=Wn(e.visibleWrites,t),l=qn(c,tt());if(null!=l)a=l;else{if(null==n)return[];a=zn(c,n)}if((a=a.withIndex(s)).isEmpty()||a.isLeafNode())return[];{const e=[],t=s.getCompare(),n=o?a.getReverseIteratorFrom(r,s):a.getIteratorFrom(r,s);let c=n.getNext();for(;c&&e.length<i;)0!==t(c,r)&&e.push(c),c=n.getNext();return e}}(e.writeTree,e.treePath,t,n,r,i,o)}function nr(e,t,n){return function(e,t,n,r){const i=ct(t,n),o=qn(e.visibleWrites,i);if(null!=o)return o;if(r.isCompleteForChild(n))return zn(Wn(e.visibleWrites,i),r.getNode().getImmediateChild(n));return null}(e.writeTree,e.treePath,t,n)}function rr(e,t){return ir(ct(e.treePath,t),e.writeTree)}function ir(e,t){return{treePath:e,writeTree:t}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class or{constructor(){this.changeMap=new Map}trackChildChange(e){const t=e.type,n=e.childName;Object(o.d)("child_added"===t||"child_changed"===t||"child_removed"===t,"Only child changes supported for tracking"),Object(o.d)(".priority"!==n,"Only non-priority child changes can be tracked.");const r=this.changeMap.get(n);if(r){const i=r.type;if("child_added"===t&&"child_removed"===i)this.changeMap.set(n,sn(n,e.snapshotNode,r.snapshotNode));else if("child_removed"===t&&"child_added"===i)this.changeMap.delete(n);else if("child_removed"===t&&"child_changed"===i)this.changeMap.set(n,on(n,r.oldSnap));else if("child_changed"===t&&"child_added"===i)this.changeMap.set(n,rn(n,e.snapshotNode));else{if("child_changed"!==t||"child_changed"!==i)throw Object(o.e)("Illegal combination of changes: "+e+" occurred after "+r);this.changeMap.set(n,sn(n,e.snapshotNode,r.oldSnap))}}else this.changeMap.set(n,e)}getChanges(){return Array.from(this.changeMap.values())}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const sr=new class{getCompleteChild(e){return null}getChildAfterChild(e,t,n){return null}};class ar{constructor(e,t,n=null){this.writes_=e,this.viewCache_=t,this.optCompleteServerCache_=n}getCompleteChild(e){const t=this.viewCache_.eventCache;if(t.isCompleteForChild(e))return t.getNode().getImmediateChild(e);{const t=null!=this.optCompleteServerCache_?new Sn(this.optCompleteServerCache_,!0,!1):this.viewCache_.serverCache;return nr(this.writes_,e,t)}}getChildAfterChild(e,t,n){const r=null!=this.optCompleteServerCache_?this.optCompleteServerCache_:An(this.viewCache_),i=tr(this.writes_,r,t,1,n,e);return 0===i.length?null:i[0]}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */function cr(e,t,n,r,i){const s=new or;let a,c;if(n.type===bn.OVERWRITE){const l=n;l.source.fromUser?a=hr(e,t,l.path,l.snap,r,i,s):(Object(o.d)(l.source.fromServer,"Unknown source."),c=l.source.tagged||t.serverCache.isFiltered()&&!lt(l.path),a=ur(e,t,l.path,l.snap,r,i,c,s))}else if(n.type===bn.MERGE){const l=n;l.source.fromUser?a=function(e,t,n,r,i,o,s){let a=t;return r.foreach((r,c)=>{const l=ct(n,r);dr(t,nt(l))&&(a=hr(e,a,l,c,i,o,s))}),r.foreach((r,c)=>{const l=ct(n,r);dr(t,nt(l))||(a=hr(e,a,l,c,i,o,s))}),a}(e,t,l.path,l.children,r,i,s):(Object(o.d)(l.source.fromServer,"Unknown source."),c=l.source.tagged||t.serverCache.isFiltered(),a=pr(e,t,l.path,l.children,r,i,c,s))}else if(n.type===bn.ACK_USER_WRITE){const c=n;a=c.revert?function(e,t,n,r,i,s){let a;if(null!=er(r,n))return t;{const c=new ar(r,t,i),l=t.eventCache.getNode();let u;if(lt(n)||".priority"===nt(n)){let n;if(t.serverCache.isFullyInitialized())n=Qn(r,An(t));else{const e=t.serverCache.getNode();Object(o.d)(e instanceof Yt,"serverChildren would be complete if leaf node"),n=Xn(r,e)}n=n,u=e.filter.updateFullNode(l,n,s)}else{const i=nt(n);let o=nr(r,i,t.serverCache);null==o&&t.serverCache.isCompleteForChild(i)&&(o=l.getImmediateChild(i)),(u=null!=o?e.filter.updateChild(l,i,o,it(n),c,s):t.eventCache.getNode().hasChild(i)?e.filter.updateChild(l,i,Yt.EMPTY_NODE,it(n),c,s):l).isEmpty()&&t.serverCache.isFullyInitialized()&&(a=Qn(r,An(t))).isLeafNode()&&(u=e.filter.updateFullNode(u,a,s))}return a=t.serverCache.isFullyInitialized()||null!=er(r,tt()),In(t,u,a,e.filter.filtersNodes())}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */(e,t,c.path,r,i,s):function(e,t,n,r,i,o,s){if(null!=er(i,n))return t;const a=t.serverCache.isFiltered(),c=t.serverCache;if(null!=r.value){if(lt(n)&&c.isFullyInitialized()||c.isCompleteForPath(n))return ur(e,t,n,c.getNode().getChild(n),i,o,a,s);if(lt(n)){let r=new Dn(null);return c.getNode().forEachChild(xt,(e,t)=>{r=r.set(new et(e),t)}),pr(e,t,n,r,i,o,a,s)}return t}{let l=new Dn(null);return r.foreach((e,t)=>{const r=ct(n,e);c.isCompleteForPath(r)&&(l=l.set(e,c.getNode().getChild(r)))}),pr(e,t,n,l,i,o,a,s)}}(e,t,c.path,c.affectedTree,r,i,s)}else{if(n.type!==bn.LISTEN_COMPLETE)throw Object(o.e)("Unknown operation type: "+n.type);a=function(e,t,n,r,i){const o=t.serverCache,s=xn(t,o.getNode(),o.isFullyInitialized()||lt(n),o.isFiltered());return lr(e,s,n,r,sr,i)}(e,t,n.path,r,s)}const l=s.getChanges();return function(e,t,n){const r=t.eventCache;if(r.isFullyInitialized()){const i=r.getNode().isLeafNode()||r.getNode().isEmpty(),o=Nn(e);(n.length>0||!e.eventCache.isFullyInitialized()||i&&!r.getNode().equals(o)||!r.getNode().getPriority().equals(o.getPriority()))&&n.push(nn(Nn(t)))}}(t,a,l),{viewCache:a,changes:l}}function lr(e,t,n,r,i,s){const a=t.eventCache;if(null!=er(r,n))return t;{let c,l;if(lt(n))if(Object(o.d)(t.serverCache.isFullyInitialized(),"If change path is empty, we must have complete server data"),t.serverCache.isFiltered()){const n=An(t),i=Xn(r,n instanceof Yt?n:Yt.EMPTY_NODE);c=e.filter.updateFullNode(t.eventCache.getNode(),i,s)}else{const n=Qn(r,An(t));c=e.filter.updateFullNode(t.eventCache.getNode(),n,s)}else{const u=nt(n);if(".priority"===u){Object(o.d)(1===rt(n),"Can't have a priority with additional path components");const i=a.getNode(),s=Zn(r,n,i,l=t.serverCache.getNode());c=null!=s?e.filter.updatePriority(i,s):a.getNode()}else{const o=it(n);let h;if(a.isCompleteForChild(u)){l=t.serverCache.getNode();const e=Zn(r,n,a.getNode(),l);h=null!=e?a.getNode().getImmediateChild(u).updateChild(o,e):a.getNode().getImmediateChild(u)}else h=nr(r,u,t.serverCache);c=null!=h?e.filter.updateChild(a.getNode(),u,h,o,i,s):a.getNode()}}return In(t,c,a.isFullyInitialized()||lt(n),e.filter.filtersNodes())}}function ur(e,t,n,r,i,o,s,a){const c=t.serverCache;let l;const u=s?e.filter:e.filter.getIndexedFilter();if(lt(n))l=u.updateFullNode(c.getNode(),r,null);else if(u.filtersNodes()&&!c.isFiltered()){const e=c.getNode().updateChild(n,r);l=u.updateFullNode(c.getNode(),e,null)}else{const e=nt(n);if(!c.isCompleteForPath(n)&&rt(n)>1)return t;const i=it(n),o=c.getNode().getImmediateChild(e).updateChild(i,r);l=".priority"===e?u.updatePriority(c.getNode(),o):u.updateChild(c.getNode(),e,o,i,sr,null)}const h=xn(t,l,c.isFullyInitialized()||lt(n),u.filtersNodes());return lr(e,h,n,i,new ar(i,h,o),a)}function hr(e,t,n,r,i,o,s){const a=t.eventCache;let c,l;const u=new ar(i,t,o);if(lt(n))c=In(t,l=e.filter.updateFullNode(t.eventCache.getNode(),r,s),!0,e.filter.filtersNodes());else{const i=nt(n);if(".priority"===i)c=In(t,l=e.filter.updatePriority(t.eventCache.getNode(),r),a.isFullyInitialized(),a.isFiltered());else{const o=it(n),l=a.getNode().getImmediateChild(i);let h;if(lt(o))h=r;else{const e=u.getCompleteChild(i);h=null!=e?".priority"===ot(o)&&e.getChild(at(o)).isEmpty()?e:e.updateChild(o,r):Yt.EMPTY_NODE}if(l.equals(h))c=t;else{c=In(t,e.filter.updateChild(a.getNode(),i,h,o,u,s),a.isFullyInitialized(),e.filter.filtersNodes())}}}return c}function dr(e,t){return e.eventCache.isCompleteForChild(t)}function fr(e,t,n){return n.foreach((e,n)=>{t=t.updateChild(e,n)}),t}function pr(e,t,n,r,i,o,s,a){if(t.serverCache.getNode().isEmpty()&&!t.serverCache.isFullyInitialized())return t;let c,l=t;c=lt(n)?r:new Dn(null).setTree(n,r);const u=t.serverCache.getNode();return c.children.inorderTraversal((n,r)=>{if(u.hasChild(n)){const c=fr(0,t.serverCache.getNode().getImmediateChild(n),r);l=ur(e,l,new et(n),c,i,o,s,a)}}),c.children.inorderTraversal((n,r)=>{const c=!t.serverCache.isCompleteForChild(n)&&void 0===r.value;if(!u.hasChild(n)&&!c){const c=fr(0,t.serverCache.getNode().getImmediateChild(n),r);l=ur(e,l,new et(n),c,i,o,s,a)}}),l}function _r(e,t){const n=An(e.viewCache_);return n&&(e.query._queryParams.loadsAllData()||!lt(t)&&!n.getImmediateChild(nt(t)).isEmpty())?n.getChild(t):null}function mr(e,t,n,r){t.type===bn.MERGE&&null!==t.source.queryId&&(Object(o.d)(An(e.viewCache_),"We should always have a full cache before handling merges"),Object(o.d)(Nn(e.viewCache_),"Missing event cache, even though we have a server cache"));const i=e.viewCache_,s=cr(e.processor_,i,t,n,r);var a,c;return a=e.processor_,c=s.viewCache,Object(o.d)(c.eventCache.getNode().isIndexed(a.filter.getIndex()),"Event snap not indexed"),Object(o.d)(c.serverCache.getNode().isIndexed(a.filter.getIndex()),"Server snap not indexed"),Object(o.d)(s.viewCache.serverCache.isFullyInitialized()||!i.serverCache.isFullyInitialized(),"Once a server snap is complete, it should never go back"),e.viewCache_=s.viewCache,vr(e,s.changes,s.viewCache.eventCache.getNode(),null)}function vr(e,t,n,r){const i=r?[r]:e.eventRegistrations_;return function(e,t,n,r){const i=[],o=[];var s;
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */return t.forEach(t=>{"child_changed"===t.type&&e.index_.indexedValueChanged(t.oldSnap,t.snapshotNode)&&o.push((s=t.childName,{type:"child_moved",snapshotNode:t.snapshotNode,childName:s}))}),On(e,i,"child_removed",t,r,n),On(e,i,"child_added",t,r,n),On(e,i,"child_moved",o,r,n),On(e,i,"child_changed",t,r,n),On(e,i,"value",t,r,n),i}(e.eventGenerator_,t,n,i)}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */let gr,yr;function br(e,t,n,r){const i=t.source.queryId;if(null!==i){const s=e.views.get(i);return Object(o.d)(null!=s,"SyncTree gave us an op for an invalid query."),mr(s,t,n,r)}{let i=[];for(const o of e.views.values())i=i.concat(mr(o,t,n,r));return i}}function Cr(e,t){let n=null;for(const r of e.views.values())n=n||_r(r,t);return n}class wr{constructor(e){this.listenProvider_=e,this.syncPointTree_=new Dn(null),this.pendingWriteTree_={visibleWrites:jn.empty(),allWrites:[],lastWriteId:-1},this.tagToQueryMap=new Map,this.queryToTagMap=new Map}}function Tr(e,t,n,r,i){return function(e,t,n,r,i){Object(o.d)(r>e.lastWriteId,"Stacking an older write on top of newer ones"),void 0===i&&(i=!0),e.allWrites.push({path:t,snap:n,writeId:r,visible:i}),i&&(e.visibleWrites=$n(e.visibleWrites,t,n)),e.lastWriteId=r}(e.pendingWriteTree_,t,n,r,i),i?Er(e,new Tn({fromUser:!0,fromServer:!1,queryId:null,tagged:!1},t,n)):[]}function kr(e,t,n=!1){const r=function(e,t){for(let n=0;n<e.allWrites.length;n++){const r=e.allWrites[n];if(r.writeId===t)return r}return null}(e.pendingWriteTree_,t);if(Vn(e.pendingWriteTree_,t)){let t=new Dn(null);return null!=r.snap?t=t.set(tt(),!0):D(r.children,e=>{t=t.set(new et(e),!0)}),Er(e,new wn(r.path,t,n))}return[]}function Sr(e,t,n){return Er(e,new Tn({fromUser:!1,fromServer:!0,queryId:null,tagged:!1},t,n))}function Or(e,t,n){const r=e.pendingWriteTree_,i=e.syncPointTree_.findOnPath(t,(e,n)=>{const r=Cr(n,ut(e,t));if(r)return r});return Jn(r,t,i,n,!0)}function Er(e,t){return function e(t,n,r,i){if(lt(t.path))return function e(t,n,r,i){const o=n.get(tt());null==r&&null!=o&&(r=Cr(o,tt()));let s=[];n.children.inorderTraversal((n,o)=>{const a=r?r.getImmediateChild(n):null,c=rr(i,n),l=t.operationForChild(n);l&&(s=s.concat(e(l,o,a,c)))});o&&(s=s.concat(br(o,t,i,r)));return s}(t,n,r,i);{const o=n.get(tt());null==r&&null!=o&&(r=Cr(o,tt()));let s=[];const a=nt(t.path),c=t.operationForChild(a),l=n.children.get(a);if(l&&c){const t=r?r.getImmediateChild(a):null,n=rr(i,a);s=s.concat(e(c,l,t,n))}return o&&(s=s.concat(br(o,t,i,r))),s}}(t,e.syncPointTree_,null,Bn(e.pendingWriteTree_,tt()))}function Ir(e,t){return e.tagToQueryMap.get(t)}function xr(e){const t=e.indexOf("$");return Object(o.d)(-1!==t&&t<e.length-1,"Bad queryKey."),{queryId:e.substr(t+1),path:new et(e.substr(0,t))}}function Nr(e,t,n){const r=e.syncPointTree_.get(t);return Object(o.d)(r,"Missing sync point for query tag that we're tracking"),br(r,n,Bn(e.pendingWriteTree_,t),null)}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class Ar{constructor(e){this.node_=e}getImmediateChild(e){const t=this.node_.getImmediateChild(e);return new Ar(t)}node(){return this.node_}}class Rr{constructor(e,t){this.syncTree_=e,this.path_=t}getImmediateChild(e){const t=ct(this.path_,e);return new Rr(this.syncTree_,t)}node(){return Or(this.syncTree_,this.path_)}}const Pr=function(e){return(e=e||{}).timestamp=e.timestamp||(new Date).getTime(),e},Dr=function(e,t,n){return e&&"object"==typeof e?(Object(o.d)(".sv"in e,"Unexpected leaf node or priority contents"),"string"==typeof e[".sv"]?jr(e[".sv"],t,n):"object"==typeof e[".sv"]?$r(e[".sv"],t):void Object(o.d)(!1,"Unexpected server value: "+JSON.stringify(e,null,2))):e},jr=function(e,t,n){switch(e){case"timestamp":return n.timestamp;default:Object(o.d)(!1,"Unexpected server value: "+e)}},$r=function(e,t,n){e.hasOwnProperty("increment")||Object(o.d)(!1,"Unexpected server value: "+JSON.stringify(e,null,2));const r=e.increment;"number"!=typeof r&&Object(o.d)(!1,"Unexpected increment value: "+r);const i=t.node();if(Object(o.d)(null!==i&&void 0!==i,"Expected ChildrenNode.EMPTY_NODE for nulls"),!i.isLeafNode())return r;const s=i.getValue();return"number"!=typeof s?r:s+r},Mr=function(e,t,n,r){return Fr(t,new Rr(n,e),r)},Lr=function(e,t,n){return Fr(e,new Ar(t),n)};function Fr(e,t,n){const r=e.getPriority().val(),i=Dr(r,t.getImmediateChild(".priority"),n);let o;if(e.isLeafNode()){const r=e,o=Dr(r.getValue(),t,n);return o!==r.getValue()||i!==r.getPriority().val()?new Ut(o,Xt(i)):e}{const r=e;return o=r,i!==r.getPriority().val()&&(o=o.updatePriority(new Ut(i))),r.forEachChild(Wt,(e,r)=>{const i=Fr(r,t.getImmediateChild(e),n);i!==r&&(o=o.updateImmediateChild(e,i))}),o}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class qr{constructor(e="",t=null,n={children:{},childCount:0}){this.name=e,this.parent=t,this.node=n}}function Ur(e,t){let n=t instanceof et?t:new et(t),r=e,i=nt(n);for(;null!==i;){const e=Object(o.w)(r.node.children,i)||{children:{},childCount:0};r=new qr(i,r,e),i=nt(n=it(n))}return r}function Wr(e){return e.node.value}function Hr(e,t){e.node.value=t,Kr(e)}function zr(e){return e.node.childCount>0}function Br(e,t){D(e.node.children,(n,r)=>{t(new qr(n,e,r))})}function Vr(e){return new et(null===e.parent?e.name:Vr(e.parent)+"/"+e.name)}function Kr(e){null!==e.parent&&function(e,t,n){const r=function(e){return void 0===Wr(e)&&!zr(e)}(n),i=Object(o.h)(e.node.children,t);r&&i?(delete e.node.children[t],e.node.childCount--,Kr(e)):r||i||(e.node.children[t]=n.node,e.node.childCount++,Kr(e))}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */(e.parent,e.name,e)}const Gr=/[\[\].#$\/\u0000-\u001F\u007F]/,Yr=/[\[\].#$\u0000-\u001F\u007F]/,Jr=function(e){return"string"==typeof e&&0!==e.length&&!Gr.test(e)},Qr=function(e){return"string"==typeof e&&0!==e.length&&!Yr.test(e)},Xr=function(e,t,n,r){r&&void 0===t||Zr(Object(o.l)(e,"value"),t,n)},Zr=function(e,t,n){const r=n instanceof et?new class{constructor(e,t){this.errorPrefix_=t,this.parts_=st(e,0),this.byteLength_=Math.max(1,this.parts_.length);for(let e=0;e<this.parts_.length;e++)this.byteLength_+=Object(o.x)(this.parts_[e]);ft(this)}}(n,e):n;if(void 0===t)throw new Error(e+"contains undefined "+pt(r));if("function"==typeof t)throw new Error(e+"contains a function "+pt(r)+" with contents = "+t.toString());if(S(t))throw new Error(e+"contains "+t.toString()+" "+pt(r));if("string"==typeof t&&t.length>10485760/3&&Object(o.x)(t)>10485760)throw new Error(e+"contains a string greater than 10485760 utf8 bytes "+pt(r)+" ('"+t.substring(0,50)+"...')");if(t&&"object"==typeof t){let n=!1,a=!1;if(D(t,(t,c)=>{if(".value"===t)n=!0;else if(".priority"!==t&&".sv"!==t&&(a=!0,!Jr(t)))throw new Error(e+" contains an invalid key ("+t+") "+pt(r)+'.  Keys must be non-empty strings and can\'t contain ".", "#", "$", "/", "[", or "]"');s=t,(i=r).parts_.length>0&&(i.byteLength_+=1),i.parts_.push(s),i.byteLength_+=Object(o.x)(s),ft(i),Zr(e,c,r),function(e){const t=e.parts_.pop();e.byteLength_-=Object(o.x)(t),e.parts_.length>0&&(e.byteLength_-=1)}(r)}),n&&a)throw new Error(e+' contains ".value" child '+pt(r)+" in addition to actual children.")}var i,s},ei=function(e,t,n,r){if(!(r&&void 0===n||Qr(n)))throw new Error(Object(o.l)(e,t)+'was an invalid path = "'+n+'". Paths must be non-empty strings and can\'t contain ".", "#", "$", "[", or "]"')},ti=function(e,t,n,r){n&&(n=n.replace(/^\/*\.info(\/|$)/,"/")),ei(e,t,n,r)},ni=function(e,t){if(".info"===nt(t))throw new Error(e+" failed = Can't modify data under /.info/")},ri=function(e,t){const n=t.path.toString();if("string"!=typeof t.repoInfo.host||0===t.repoInfo.host.length||!Jr(t.repoInfo.namespace)&&"localhost"!==t.repoInfo.host.split(":")[0]||0!==n.length&&!function(e){return e&&(e=e.replace(/^\/*\.info(\/|$)/,"/")),Qr(e)}(n))throw new Error(Object(o.l)(e,"url")+'must be a valid firebase URL and the path can\'t contain ".", "#", "$", "[", or "]".')};
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class ii{constructor(){this.eventLists_=[],this.recursionDepth_=0}}function oi(e,t){let n=null;for(let r=0;r<t.length;r++){const i=t[r],o=i.getPath();null===n||ht(o,n.path)||(e.eventLists_.push(n),n=null),null===n&&(n={events:[],path:o}),n.events.push(i)}n&&e.eventLists_.push(n)}function si(e,t,n){oi(e,n),ai(e,e=>dt(e,t)||dt(t,e))}function ai(e,t){e.recursionDepth_++;let n=!0;for(let r=0;r<e.eventLists_.length;r++){const i=e.eventLists_[r];if(i){t(i.path)?(ci(e.eventLists_[r]),e.eventLists_[r]=null):n=!1}}n&&(e.eventLists_=[]),e.recursionDepth_--}function ci(e){for(let t=0;t<e.events.length;t++){const n=e.events[t];if(null!==n){e.events[t]=null;const r=n.getEventRunner();v&&b("event: "+n.toString()),q(r)}}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const li="repo_interrupt",ui=25;class hi{constructor(e,t,n,r){this.repoInfo_=e,this.forceRestClient_=t,this.authTokenProvider_=n,this.appCheckProvider_=r,this.dataUpdateCount=0,this.statsListener_=null,this.eventQueue_=new ii,this.nextWriteId_=1,this.interceptServerDataCallback_=null,this.onDisconnect_=dn(),this.transactionQueueTree_=new qr,this.persistentConnection_=null,this.key=this.repoInfo_.toURLString()}toString(){return(this.repoInfo_.secure?"https://":"http://")+this.repoInfo_.host}}function di(e,t,n){if(e.stats_=ce(e.repoInfo_),e.forceRestClient_||U())e.server_=new un(e.repoInfo_,(t,n,r,i)=>{_i(e,t,n,r,i)},e.authTokenProvider_,e.appCheckProvider_),setTimeout(()=>mi(e,!0),0);else{if(void 0!==n&&null!==n){if("object"!=typeof n)throw new Error("Only objects are supported for option databaseAuthVariableOverride");try{Object(o.z)(n)}catch(e){throw new Error("Invalid authOverride provided: "+e)}}e.persistentConnection_=new kt(e.repoInfo_,t,(t,n,r,i)=>{_i(e,t,n,r,i)},t=>{mi(e,t)},t=>{!function(e,t){D(t,(t,n)=>{vi(e,t,n)})}(e,t)},e.authTokenProvider_,e.appCheckProvider_,n),e.server_=e.persistentConnection_}e.authTokenProvider_.addTokenChangeListener(t=>{e.server_.refreshAuthToken(t)}),e.appCheckProvider_.addTokenChangeListener(t=>{e.server_.refreshAppCheckToken(t.token)}),e.statsReporter_=function(e,t){const n=e.toString();return ae[n]||(ae[n]=t()),ae[n]}(e.repoInfo_,()=>new yn(e.stats_,e.server_)),e.infoData_=new hn,e.infoSyncTree_=new wr({startListening:(t,n,r,i)=>{let o=[];const s=e.infoData_.getNode(t._path);return s.isEmpty()||(o=Sr(e.infoSyncTree_,t._path,s),setTimeout(()=>{i("ok")},0)),o},stopListening:()=>{}}),vi(e,"connected",!1),e.serverSyncTree_=new wr({startListening:(t,n,r,i)=>(e.server_.listen(t,r,n,(n,r)=>{const o=i(n,r);si(e.eventQueue_,t._path,o)}),[]),stopListening:(t,n)=>{e.server_.unlisten(t,n)}})}function fi(e){const t=e.infoData_.getNode(new et(".info/serverTimeOffset")).val()||0;return(new Date).getTime()+t}function pi(e){return Pr({timestamp:fi(e)})}function _i(e,t,n,r,i){e.dataUpdateCount++;const s=new et(t);n=e.interceptServerDataCallback_?e.interceptServerDataCallback_(t,n):n;let a=[];if(i)if(r){const t=Object(o.u)(n,e=>Xt(e));a=function(e,t,n,r){const i=Ir(e,r);if(i){const r=xr(i),o=r.path,s=r.queryId,a=ut(o,t),c=Dn.fromObject(n);return Nr(e,o,new kn(Cn(s),a,c))}return[]}(e.serverSyncTree_,s,t,i)}else{const t=Xt(n);a=function(e,t,n,r){const i=Ir(e,r);if(null!=i){const r=xr(i),o=r.path,s=r.queryId,a=ut(o,t);return Nr(e,o,new Tn(Cn(s),a,n))}return[]}(e.serverSyncTree_,s,t,i)}else if(r){const t=Object(o.u)(n,e=>Xt(e));a=function(e,t,n){const r=Dn.fromObject(n);return Er(e,new kn({fromUser:!1,fromServer:!0,queryId:null,tagged:!1},t,r))}(e.serverSyncTree_,s,t)}else{const t=Xt(n);a=Sr(e.serverSyncTree_,s,t)}let c=s;a.length>0&&(c=Si(e,s)),si(e.eventQueue_,c,a)}function mi(e,t){vi(e,"connected",t),!1===t&&function(e){Ci(e,"onDisconnectEvents");const t=pi(e),n=dn();pn(e.onDisconnect_,tt(),(r,i)=>{const o=Mr(r,i,e.serverSyncTree_,t);fn(n,r,o)});let r=[];pn(n,tt(),(t,n)=>{r=r.concat(Sr(e.serverSyncTree_,t,n));const i=xi(e,t);Si(e,i)}),e.onDisconnect_=dn(),si(e.eventQueue_,tt(),r)}(e)}function vi(e,t,n){const r=new et("/.info/"+t),i=Xt(n);e.infoData_.updateSnapshot(r,i);const o=Sr(e.infoSyncTree_,r,i);si(e.eventQueue_,r,o)}function gi(e){return e.nextWriteId_++}function yi(e,t,n,r,i){Ci(e,"set",{path:t.toString(),value:n,priority:r});const o=pi(e),s=Xt(n,r),a=Or(e.serverSyncTree_,t),c=Lr(s,a,o),l=gi(e),u=Tr(e.serverSyncTree_,t,c,l,!0);oi(e.eventQueue_,u),e.server_.put(t.toString(),s.val(!0),(n,r)=>{const o="ok"===n;o||k("set at "+t+" failed: "+n);const s=kr(e.serverSyncTree_,l,!o);si(e.eventQueue_,t,s),wi(e,i,n,r)});const h=xi(e,t);Si(e,h),si(e.eventQueue_,h,[])}function bi(e){e.persistentConnection_&&e.persistentConnection_.interrupt(li)}function Ci(e,...t){let n="";e.persistentConnection_&&(n=e.persistentConnection_.id+":"),b(n,...t)}function wi(e,t,n,r){t&&q(()=>{if("ok"===n)t(null);else{const e=(n||"error").toUpperCase();let i=e;r&&(i+=": "+r);const o=new Error(i);o.code=e,t(o)}})}function Ti(e,t,n){return Or(e.serverSyncTree_,t,n)||Yt.EMPTY_NODE}function ki(e,t=e.transactionQueueTree_){if(t||Ii(e,t),Wr(t)){const n=Ei(e,t);Object(o.d)(n.length>0,"Sending zero length transaction queue"),n.every(e=>0===e.status)&&function(e,t,n){const r=n.map(e=>e.currentWriteId),i=Ti(e,t,r);let s=i;const a=i.hash();for(let e=0;e<n.length;e++){const r=n[e];Object(o.d)(0===r.status,"tryToSendTransactionQueue_: items in queue should all be run."),r.status=1,r.retryCount++;const i=ut(t,r.path);s=s.updateChild(i,r.currentOutputSnapshotRaw)}const c=s.val(!0),l=t;e.server_.put(l.toString(),c,r=>{Ci(e,"transaction put response",{path:l.toString(),status:r});let i=[];if("ok"===r){const r=[];for(let t=0;t<n.length;t++)n[t].status=2,i=i.concat(kr(e.serverSyncTree_,n[t].currentWriteId)),n[t].onComplete&&r.push(()=>n[t].onComplete(null,!0,n[t].currentOutputSnapshotResolved)),n[t].unwatcher();Ii(e,Ur(e.transactionQueueTree_,t)),ki(e,e.transactionQueueTree_),si(e.eventQueue_,t,i);for(let e=0;e<r.length;e++)q(r[e])}else{if("datastale"===r)for(let e=0;e<n.length;e++)3===n[e].status?n[e].status=4:n[e].status=0;else{k("transaction at "+l.toString()+" failed: "+r);for(let e=0;e<n.length;e++)n[e].status=4,n[e].abortReason=r}Si(e,t)}},a)}(e,Vr(t),n)}else zr(t)&&Br(t,t=>{ki(e,t)})}function Si(e,t){const n=Oi(e,t),r=Vr(n);return function(e,t,n){if(0===t.length)return;const r=[];let i=[];const s=t.filter(e=>0===e.status).map(e=>e.currentWriteId);for(let c=0;c<t.length;c++){const l=t[c],u=ut(n,l.path);let h,d=!1;if(Object(o.d)(null!==u,"rerunTransactionsUnderNode_: relativePath should not be null."),4===l.status)d=!0,h=l.abortReason,i=i.concat(kr(e.serverSyncTree_,l.currentWriteId,!0));else if(0===l.status)if(l.retryCount>=ui)d=!0,h="maxretry",i=i.concat(kr(e.serverSyncTree_,l.currentWriteId,!0));else{const n=Ti(e,l.path,s);l.currentInputSnapshot=n;const r=t[c].update(n.val());if(void 0!==r){Zr("transaction failed: Data returned ",r,l.path);let t=Xt(r);const a="object"==typeof r&&null!=r&&Object(o.h)(r,".priority");a||(t=t.updatePriority(n.getPriority()));const c=l.currentWriteId,u=pi(e),h=Lr(t,n,u);l.currentOutputSnapshotRaw=t,l.currentOutputSnapshotResolved=h,l.currentWriteId=gi(e),s.splice(s.indexOf(c),1),i=(i=i.concat(Tr(e.serverSyncTree_,l.path,h,l.currentWriteId,l.applyLocally))).concat(kr(e.serverSyncTree_,c,!0))}else d=!0,h="nodata",i=i.concat(kr(e.serverSyncTree_,l.currentWriteId,!0))}si(e.eventQueue_,n,i),i=[],d&&(t[c].status=2,a=t[c].unwatcher,setTimeout(a,Math.floor(0)),t[c].onComplete&&("nodata"===h?r.push(()=>t[c].onComplete(null,!1,t[c].currentInputSnapshot)):r.push(()=>t[c].onComplete(new Error(h),!1,null))))}var a;Ii(e,e.transactionQueueTree_);for(let e=0;e<r.length;e++)q(r[e]);ki(e,e.transactionQueueTree_)}(e,Ei(e,n),r),r}function Oi(e,t){let n,r=e.transactionQueueTree_;for(n=nt(t);null!==n&&void 0===Wr(r);)r=Ur(r,n),n=nt(t=it(t));return r}function Ei(e,t){const n=[];return function e(t,n,r){const i=Wr(n);if(i)for(let e=0;e<i.length;e++)r.push(i[e]);Br(n,n=>{e(t,n,r)})}(e,t,n),n.sort((e,t)=>e.order-t.order),n}function Ii(e,t){const n=Wr(t);if(n){let e=0;for(let t=0;t<n.length;t++)2!==n[t].status&&(n[e]=n[t],e++);n.length=e,Hr(t,n.length>0?n:void 0)}Br(t,t=>{Ii(e,t)})}function xi(e,t){const n=Vr(Oi(e,t)),r=Ur(e.transactionQueueTree_,t);return function(e,t,n){let r=n?e:e.parent;for(;null!==r;){if(t(r))return!0;r=r.parent}}(r,t=>{Ni(e,t)}),Ni(e,r),function e(t,n,r,i){r&&!i&&n(t),Br(t,t=>{e(t,n,!0,i)}),r&&i&&n(t)}(r,t=>{Ni(e,t)}),n}function Ni(e,t){const n=Wr(t);if(n){const r=[];let i=[],s=-1;for(let t=0;t<n.length;t++)3===n[t].status||(1===n[t].status?(Object(o.d)(s===t-1,"All SENT items should be at beginning of queue."),s=t,n[t].status=3,n[t].abortReason="set"):(Object(o.d)(0===n[t].status,"Unexpected transaction status in abort"),n[t].unwatcher(),i=i.concat(kr(e.serverSyncTree_,n[t].currentWriteId,!0)),n[t].onComplete&&r.push(n[t].onComplete.bind(null,new Error("set"),!1,null))));-1===s?Hr(t,void 0):n.length=s+1,si(e.eventQueue_,Vr(t),i);for(let e=0;e<r.length;e++)q(r[e])}}
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const Ai=function(e,t){const n=Ri(e),r=n.namespace;"firebase.com"===n.domain&&T(n.host+" is no longer supported. Please use <YOUR FIREBASE>.firebaseio.com instead"),r&&"undefined"!==r||"localhost"===n.domain||T("Cannot parse Firebase url. Please use https://<YOUR FIREBASE>.firebaseio.com"),n.secure||"undefined"!=typeof window&&window.location&&window.location.protocol&&-1!==window.location.protocol.indexOf("https:")&&k("Insecure Firebase access from a secure page. Please use https in calls to new Firebase().");const i="ws"===n.scheme||"wss"===n.scheme;return{repoInfo:new re(n.host,n.secure,r,t,i,"",r!==n.subdomain),path:new et(n.pathString)}},Ri=function(e){let t="",n="",r="",i="",o="",s=!0,a="https",c=443;if("string"==typeof e){let l=e.indexOf("//");l>=0&&(a=e.substring(0,l-1),e=e.substring(l+2));let u=e.indexOf("/");-1===u&&(u=e.length);let h=e.indexOf("?");-1===h&&(h=e.length),t=e.substring(0,Math.min(u,h)),u<h&&(i=function(e){let t="";const n=e.split("/");for(let e=0;e<n.length;e++)if(n[e].length>0){let r=n[e];try{r=decodeURIComponent(r.replace(/\+/g," "))}catch(e){}t+="/"+r}return t}(e.substring(u,h)));const d=function(e){const t={};"?"===e.charAt(0)&&(e=e.substring(1));for(const n of e.split("&")){if(0===n.length)continue;const r=n.split("=");2===r.length?t[decodeURIComponent(r[0])]=decodeURIComponent(r[1]):k(`Invalid query segment '${n}' in query '${e}'`)}return t}(e.substring(Math.min(e.length,h)));(l=t.indexOf(":"))>=0?(s="https"===a||"wss"===a,c=parseInt(t.substring(l+1),10)):l=t.length;const f=t.slice(0,l);if("localhost"===f.toLowerCase())n="localhost";else if(f.split(".").length<=2)n=f;else{const e=t.indexOf(".");r=t.substring(0,e).toLowerCase(),n=t.substring(e+1),o=r}"ns"in d&&(o=d.ns)}return{host:t,port:c,domain:n,subdomain:r,secure:s,scheme:a,pathString:i,namespace:o}};
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class Pi{constructor(e,t,n,r){this._repo=e,this._path=t,this._queryParams=n,this._orderByCalled=r}get key(){return lt(this._path)?null:ot(this._path)}get ref(){return new Di(this._repo,this._path)}get _queryIdentifier(){const e=ln(this._queryParams),t=R(e);return"{}"===t?"default":t}get _queryObject(){return ln(this._queryParams)}isEqual(e){if(!((e=Object(o.m)(e))instanceof Pi))return!1;const t=this._repo===e._repo,n=ht(this._path,e._path),r=this._queryIdentifier===e._queryIdentifier;return t&&n&&r}toJSON(){return this.toString()}toString(){return this._repo.toString()+function(e){let t="";for(let n=e.pieceNum_;n<e.pieces_.length;n++)""!==e.pieces_[n]&&(t+="/"+encodeURIComponent(String(e.pieces_[n])));return t||"/"}(this._path)}}class Di extends Pi{constructor(e,t){super(e,t,new an,!1)}get parent(){const e=at(this._path);return null===e?null:new Di(this._repo,e)}get root(){let e=this;for(;null!==e.parent;)e=e.parent;return e}}function ji(e,t){return(e=Object(o.m)(e))._checkNotDeleted("ref"),void 0!==t?$i(e._root,t):e._root}function $i(e,t){return null===nt((e=Object(o.m)(e))._path)?ti("child","path",t,!1):ei("child","path",t,!1),new Di(e._repo,ct(e._path,t))}function Mi(e,t){e=Object(o.m)(e),ni("set",e._path),Xr("set",t,e._path,!1);const n=new o.a;return yi(e._repo,e._path,t,null,n.wrapCallback(()=>{})),n.promise}!function(e){Object(o.d)(!gr,"__referenceConstructor has already been defined"),gr=e}(Di),function(e){Object(o.d)(!yr,"__referenceConstructor has already been defined"),yr=e}(Di);
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const Li="FIREBASE_DATABASE_EMULATOR_HOST",Fi={};let qi=!1;function Ui(t,n,r,i,o){let s=i||t.options.databaseURL;void 0===s&&(t.options.projectId||T("Can't determine Firebase Database URL. Be sure to include  a Project ID when calling firebase.initializeApp()."),b("Using default host for project ",t.options.projectId),s=`${t.options.projectId}-default-rtdb.firebaseio.com`);let a,c=Ai(s,o),l=c.repoInfo,u=void 0;void 0!==e&&(u=Object({NODE_ENV:"production"})[Li]),u?(a=!0,s=`http://${u}?ns=${l.namespace}`,l=(c=Ai(s,o)).repoInfo):a=!c.repoInfo.secure;const h=o&&a?new B(B.OWNER):new z(t.name,t.options,n);ri("Invalid Firebase Database URL",c),lt(c.path)||T("Database URL must point to the root of a Firebase Database (not including a child path).");const d=function(e,t,n,r){let i=Fi[t.name];i||(i={},Fi[t.name]=i);let o=i[e.toURLString()];o&&T("Database initialized multiple times. Please make sure the format of the database URL matches with each database() call.");return o=new hi(e,qi,n,r),i[e.toURLString()]=o,o}(l,t,h,new H(t.name,r));return new Wi(d,t)}class Wi{constructor(e,t){this._repoInternal=e,this.app=t,this.type="database",this._instanceStarted=!1}get _repo(){return this._instanceStarted||(di(this._repoInternal,this.app.options.appId,this.app.options.databaseAuthVariableOverride),this._instanceStarted=!0),this._repoInternal}get _root(){return this._rootInternal||(this._rootInternal=new Di(this._repo,tt())),this._rootInternal}_delete(){return null!==this._rootInternal&&(!function(e,t){const n=Fi[t];n&&n[e.key]===e||T(`Database ${t}(${e.repoInfo_}) has already been deleted.`),bi(e),delete n[e.key]}(this._repo,this.app.name),this._repoInternal=null,this._rootInternal=null),Promise.resolve()}_checkNotDeleted(e){null===this._rootInternal&&T("Cannot call "+e+" on a deleted database.")}}function Hi(e=Object(r.d)(),t){return Object(r.b)(e,"database").getImmediate({identifier:t})}kt.prototype.simpleListen=function(e,t){this.sendRequest("q",{p:e},t)},kt.prototype.echo=function(e,t){this.sendRequest("echo",{d:e},t)};
/**
 * @license
 * Copyright 2021 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var zi;
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */!function(e){l=e}(r.a),Object(r.c)(new i.a("database",(e,{instanceIdentifier:t})=>Ui(e.getProvider("app").getImmediate(),e.getProvider("auth-internal"),e.getProvider("app-check-internal"),t),"PUBLIC").setMultipleInstances(!0)),Object(r.f)(a,c,zi),Object(r.f)(a,c,"esm2017")}).call(t,n("W2nU"))},S82l:function(e,t){e.exports=function(e){try{return!!e()}catch(e){return!0}}},SfB7:function(e,t,n){e.exports=!n("+E39")&&!n("S82l")(function(){return 7!=Object.defineProperty(n("ON07")("div"),"a",{get:function(){return 7}}).a})},"VU/8":function(e,t){e.exports=function(e,t,n,r,i,o){var s,a=e=e||{},c=typeof e.default;"object"!==c&&"function"!==c||(s=e,a=e.default);var l,u="function"==typeof a?a.options:a;if(t&&(u.render=t.render,u.staticRenderFns=t.staticRenderFns,u._compiled=!0),n&&(u.functional=!0),i&&(u._scopeId=i),o?(l=function(e){(e=e||this.$vnode&&this.$vnode.ssrContext||this.parent&&this.parent.$vnode&&this.parent.$vnode.ssrContext)||"undefined"==typeof __VUE_SSR_CONTEXT__||(e=__VUE_SSR_CONTEXT__),r&&r.call(this,e),e&&e._registeredComponents&&e._registeredComponents.add(o)},u._ssrRegister=l):r&&(l=r),l){var h=u.functional,d=h?u.render:u.beforeCreate;h?(u._injectStyles=l,u.render=function(e,t){return l.call(t),d(e,t)}):u.beforeCreate=d?[].concat(d,l):[l]}return{esModule:s,exports:a,options:u}}},W2nU:function(e,t){var n,r,i=e.exports={};function o(){throw new Error("setTimeout has not been defined")}function s(){throw new Error("clearTimeout has not been defined")}function a(e){if(n===setTimeout)return setTimeout(e,0);if((n===o||!n)&&setTimeout)return n=setTimeout,setTimeout(e,0);try{return n(e,0)}catch(t){try{return n.call(null,e,0)}catch(t){return n.call(this,e,0)}}}!function(){try{n="function"==typeof setTimeout?setTimeout:o}catch(e){n=o}try{r="function"==typeof clearTimeout?clearTimeout:s}catch(e){r=s}}();var c,l=[],u=!1,h=-1;function d(){u&&c&&(u=!1,c.length?l=c.concat(l):h=-1,l.length&&f())}function f(){if(!u){var e=a(d);u=!0;for(var t=l.length;t;){for(c=l,l=[];++h<t;)c&&c[h].run();h=-1,t=l.length}c=null,u=!1,function(e){if(r===clearTimeout)return clearTimeout(e);if((r===s||!r)&&clearTimeout)return r=clearTimeout,clearTimeout(e);try{r(e)}catch(t){try{return r.call(null,e)}catch(t){return r.call(this,e)}}}(e)}}function p(e,t){this.fun=e,this.array=t}function _(){}i.nextTick=function(e){var t=new Array(arguments.length-1);if(arguments.length>1)for(var n=1;n<arguments.length;n++)t[n-1]=arguments[n];l.push(new p(e,t)),1!==l.length||u||a(f)},p.prototype.run=function(){this.fun.apply(null,this.array)},i.title="browser",i.browser=!0,i.env={},i.argv=[],i.version="",i.versions={},i.on=_,i.addListener=_,i.once=_,i.off=_,i.removeListener=_,i.removeAllListeners=_,i.emit=_,i.prependListener=_,i.prependOnceListener=_,i.listeners=function(e){return[]},i.binding=function(e){throw new Error("process.binding is not supported")},i.cwd=function(){return"/"},i.chdir=function(e){throw new Error("process.chdir is not supported")},i.umask=function(){return 0}},X8DO:function(e,t){e.exports=function(e,t){return{enumerable:!(1&e),configurable:!(2&e),writable:!(4&e),value:t}}},Zrlr:function(e,t,n){"use strict";t.__esModule=!0,t.default=function(e,t){if(!(e instanceof t))throw new TypeError("Cannot call a class as a function")}},a7sP:function(e,t,n){"use strict";n.d(t,"a",function(){return b}),n.d(t,"b",function(){return m}),n.d(t,"c",function(){return _}),n.d(t,"d",function(){return w}),n.d(t,"e",function(){return C}),n.d(t,"f",function(){return T});var r=n("xq9A"),i=n("gZMR"),o=n("28tl");
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class s{constructor(e){this.container=e}getPlatformInfoString(){return this.container.getProviders().map(e=>{if(function(e){const t=e.getComponent();return"VERSION"===(null===t||void 0===t?void 0:t.type)}(e)){const t=e.getImmediate();return`${t.library}/${t.version}`}return null}).filter(e=>e).join(" ")}}const a="@firebase/app",c="0.7.14",l=new i.b("@firebase/app"),u="[DEFAULT]",h={[a]:"fire-core","@firebase/app-compat":"fire-core-compat","@firebase/analytics":"fire-analytics","@firebase/analytics-compat":"fire-analytics-compat","@firebase/app-check":"fire-app-check","@firebase/app-check-compat":"fire-app-check-compat","@firebase/auth":"fire-auth","@firebase/auth-compat":"fire-auth-compat","@firebase/database":"fire-rtdb","@firebase/database-compat":"fire-rtdb-compat","@firebase/functions":"fire-fn","@firebase/functions-compat":"fire-fn-compat","@firebase/installations":"fire-iid","@firebase/installations-compat":"fire-iid-compat","@firebase/messaging":"fire-fcm","@firebase/messaging-compat":"fire-fcm-compat","@firebase/performance":"fire-perf","@firebase/performance-compat":"fire-perf-compat","@firebase/remote-config":"fire-rc","@firebase/remote-config-compat":"fire-rc-compat","@firebase/storage":"fire-gcs","@firebase/storage-compat":"fire-gcs-compat","@firebase/firestore":"fire-fst","@firebase/firestore-compat":"fire-fst-compat","fire-js":"fire-js",firebase:"fire-js-all"},d=new Map,f=new Map;function p(e,t){try{e.container.addComponent(t)}catch(n){l.debug(`Component ${t.name} failed to register with FirebaseApp ${e.name}`,n)}}function _(e){const t=e.name;if(f.has(t))return l.debug(`There were multiple attempts to register component ${t}.`),!1;f.set(t,e);for(const t of d.values())p(t,e);return!0}function m(e,t){return e.container.getProvider(t)}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const v={"no-app":"No Firebase App '{$appName}' has been created - call Firebase App.initializeApp()","bad-app-name":"Illegal App name: '{$appName}","duplicate-app":"Firebase App named '{$appName}' already exists with different options or config","app-deleted":"Firebase App named '{$appName}' already deleted","invalid-app-argument":"firebase.{$appName}() takes either no argument or a Firebase App instance.","invalid-log-argument":"First argument to `onLog` must be null or a function."},g=new o.b("app","Firebase",v);
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class y{constructor(e,t,n){this._isDeleted=!1,this._options=Object.assign({},e),this._config=Object.assign({},t),this._name=t.name,this._automaticDataCollectionEnabled=t.automaticDataCollectionEnabled,this._container=n,this.container.addComponent(new r.a("app",()=>this,"PUBLIC"))}get automaticDataCollectionEnabled(){return this.checkDestroyed(),this._automaticDataCollectionEnabled}set automaticDataCollectionEnabled(e){this.checkDestroyed(),this._automaticDataCollectionEnabled=e}get name(){return this.checkDestroyed(),this._name}get options(){return this.checkDestroyed(),this._options}get config(){return this.checkDestroyed(),this._config}get container(){return this._container}get isDeleted(){return this._isDeleted}set isDeleted(e){this._isDeleted=e}checkDestroyed(){if(this.isDeleted)throw g.create("app-deleted",{appName:this._name})}}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const b="9.6.4";function C(e,t={}){if("object"!=typeof t){t={name:t}}const n=Object.assign({name:u,automaticDataCollectionEnabled:!1},t),i=n.name;if("string"!=typeof i||!i)throw g.create("bad-app-name",{appName:String(i)});const s=d.get(i);if(s){if(Object(o.k)(e,s.options)&&Object(o.k)(n,s.config))return s;throw g.create("duplicate-app",{appName:i})}const a=new r.b(i);for(const e of f.values())a.addComponent(e);const c=new y(e,n,a);return d.set(i,c),c}function w(e=u){const t=d.get(e);if(!t)throw g.create("no-app",{appName:e});return t}function T(e,t,n){var i;let o=null!==(i=h[e])&&void 0!==i?i:e;n&&(o+=`-${n}`);const s=o.match(/\s|\//),a=t.match(/\s|\//);if(s||a){const e=[`Unable to register library "${o}" with version "${t}":`];return s&&e.push(`library name "${o}" contains illegal characters (whitespace or "/")`),s&&a&&e.push("and"),a&&e.push(`version name "${t}" contains illegal characters (whitespace or "/")`),void l.warn(e.join(" "))}_(new r.a(`${o}-version`,()=>({library:o,version:t}),"VERSION"))}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var k;k="",_(new r.a("platform-logger",e=>new s(e),"PRIVATE")),T(a,c,k),T(a,c,"esm2017"),T("fire-js","")},evD5:function(e,t,n){var r=n("77Pl"),i=n("SfB7"),o=n("MmMw"),s=Object.defineProperty;t.f=n("+E39")?Object.defineProperty:function(e,t,n){if(r(e),t=o(t,!0),r(n),i)try{return s(e,t,n)}catch(e){}if("get"in n||"set"in n)throw TypeError("Accessors not supported!");return"value"in n&&(e[t]=n.value),e}},gZMR:function(e,t,n){"use strict";n.d(t,"a",function(){return i}),n.d(t,"b",function(){return l}),n.d(t,"c",function(){return u}),n.d(t,"d",function(){return h});
/**
 * @license
 * Copyright 2017 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const r=[];var i;!function(e){e[e.DEBUG=0]="DEBUG",e[e.VERBOSE=1]="VERBOSE",e[e.INFO=2]="INFO",e[e.WARN=3]="WARN",e[e.ERROR=4]="ERROR",e[e.SILENT=5]="SILENT"}(i||(i={}));const o={debug:i.DEBUG,verbose:i.VERBOSE,info:i.INFO,warn:i.WARN,error:i.ERROR,silent:i.SILENT},s=i.INFO,a={[i.DEBUG]:"log",[i.VERBOSE]:"log",[i.INFO]:"info",[i.WARN]:"warn",[i.ERROR]:"error"},c=(e,t,...n)=>{if(t<e.logLevel)return;const r=(new Date).toISOString(),i=a[t];if(!i)throw new Error(`Attempted to log a message with an invalid logType (value: ${t})`);console[i](`[${r}]  ${e.name}:`,...n)};class l{constructor(e){this.name=e,this._logLevel=s,this._logHandler=c,this._userLogHandler=null,r.push(this)}get logLevel(){return this._logLevel}set logLevel(e){if(!(e in i))throw new TypeError(`Invalid value "${e}" assigned to \`logLevel\``);this._logLevel=e}setLogLevel(e){this._logLevel="string"==typeof e?o[e]:e}get logHandler(){return this._logHandler}set logHandler(e){if("function"!=typeof e)throw new TypeError("Value assigned to `logHandler` must be a function");this._logHandler=e}get userLogHandler(){return this._userLogHandler}set userLogHandler(e){this._userLogHandler=e}debug(...e){this._userLogHandler&&this._userLogHandler(this,i.DEBUG,...e),this._logHandler(this,i.DEBUG,...e)}log(...e){this._userLogHandler&&this._userLogHandler(this,i.VERBOSE,...e),this._logHandler(this,i.VERBOSE,...e)}info(...e){this._userLogHandler&&this._userLogHandler(this,i.INFO,...e),this._logHandler(this,i.INFO,...e)}warn(...e){this._userLogHandler&&this._userLogHandler(this,i.WARN,...e),this._logHandler(this,i.WARN,...e)}error(...e){this._userLogHandler&&this._userLogHandler(this,i.ERROR,...e),this._logHandler(this,i.ERROR,...e)}}function u(e){r.forEach(t=>{t.setLogLevel(e)})}function h(e,t){for(const n of r){let r=null;t&&t.level&&(r=o[t.level]),n.userLogHandler=null===e?null:(t,n,...o)=>{const s=o.map(e=>{if(null==e)return null;if("string"==typeof e)return e;if("number"==typeof e||"boolean"==typeof e)return e.toString();if(e instanceof Error)return e.message;try{return JSON.stringify(e)}catch(e){return null}}).filter(e=>e).join(" ");n>=(null!==r&&void 0!==r?r:t.logLevel)&&e({level:i[n].toLowerCase(),message:s,args:o,type:t.name})}}}},hJx8:function(e,t,n){var r=n("evD5"),i=n("X8DO");e.exports=n("+E39")?function(e,t,n){return r.f(e,t,i(1,n))}:function(e,t,n){return e[t]=n,e}},hZls:function(e,t,n){"use strict";var r=n("RUaj");n.d(t,"a",function(){return r.a}),n.d(t,"b",function(){return r.b}),n.d(t,"c",function(){return r.c})},kM2E:function(e,t,n){var r=n("7KvD"),i=n("FeBl"),o=n("+ZMJ"),s=n("hJx8"),a=n("D2L2"),c=function(e,t,n){var l,u,h,d=e&c.F,f=e&c.G,p=e&c.S,_=e&c.P,m=e&c.B,v=e&c.W,g=f?i:i[t]||(i[t]={}),y=g.prototype,b=f?r:p?r[t]:(r[t]||{}).prototype;for(l in f&&(n=t),n)(u=!d&&b&&void 0!==b[l])&&a(g,l)||(h=u?b[l]:n[l],g[l]=f&&"function"!=typeof b[l]?n[l]:m&&u?o(h,r):v&&b[l]==h?function(e){var t=function(t,n,r){if(this instanceof e){switch(arguments.length){case 0:return new e;case 1:return new e(t);case 2:return new e(t,n)}return new e(t,n,r)}return e.apply(this,arguments)};return t.prototype=e.prototype,t}(h):_&&"function"==typeof h?o(Function.call,h):h,_&&((g.virtual||(g.virtual={}))[l]=h,e&c.R&&y&&!y[l]&&s(y,l,h)))};c.F=1,c.G=2,c.S=4,c.P=8,c.B=16,c.W=32,c.U=64,c.R=128,e.exports=c},lOnJ:function(e,t){e.exports=function(e){if("function"!=typeof e)throw TypeError(e+" is not a function!");return e}},mClu:function(e,t,n){var r=n("kM2E");r(r.S+r.F*!n("+E39"),"Object",{defineProperty:n("evD5").f})},uLgv:function(e,t,n){"use strict";var r=n("a7sP");n.d(t,"a",function(){return r.e});
/**
 * @license
 * Copyright 2020 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object(r.f)("firebase","9.6.4","app")},wxAW:function(e,t,n){"use strict";t.__esModule=!0;var r,i=n("C4MV"),o=(r=i)&&r.__esModule?r:{default:r};t.default=function(){function e(e,t){for(var n=0;n<t.length;n++){var r=t[n];r.enumerable=r.enumerable||!1,r.configurable=!0,"value"in r&&(r.writable=!0),(0,o.default)(e,r.key,r)}}return function(t,n,r){return n&&e(t.prototype,n),r&&e(t,r),t}}()},xq9A:function(e,t,n){"use strict";n.d(t,"a",function(){return i}),n.d(t,"b",function(){return a});var r=n("28tl");class i{constructor(e,t,n){this.name=e,this.instanceFactory=t,this.type=n,this.multipleInstances=!1,this.serviceProps={},this.instantiationMode="LAZY",this.onInstanceCreated=null}setInstantiationMode(e){return this.instantiationMode=e,this}setMultipleInstances(e){return this.multipleInstances=e,this}setServiceProps(e){return this.serviceProps=e,this}setInstanceCreatedCallback(e){return this.onInstanceCreated=e,this}}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */const o="[DEFAULT]";
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */class s{constructor(e,t){this.name=e,this.container=t,this.component=null,this.instances=new Map,this.instancesDeferred=new Map,this.instancesOptions=new Map,this.onInitCallbacks=new Map}get(e){const t=this.normalizeInstanceIdentifier(e);if(!this.instancesDeferred.has(t)){const e=new r.a;if(this.instancesDeferred.set(t,e),this.isInitialized(t)||this.shouldAutoInitialize())try{const n=this.getOrInitializeService({instanceIdentifier:t});n&&e.resolve(n)}catch(e){}}return this.instancesDeferred.get(t).promise}getImmediate(e){var t;const n=this.normalizeInstanceIdentifier(null===e||void 0===e?void 0:e.identifier),r=null!==(t=null===e||void 0===e?void 0:e.optional)&&void 0!==t&&t;if(!this.isInitialized(n)&&!this.shouldAutoInitialize()){if(r)return null;throw Error(`Service ${this.name} is not available`)}try{return this.getOrInitializeService({instanceIdentifier:n})}catch(e){if(r)return null;throw e}}getComponent(){return this.component}setComponent(e){if(e.name!==this.name)throw Error(`Mismatching Component ${e.name} for Provider ${this.name}.`);if(this.component)throw Error(`Component for ${this.name} has already been provided`);if(this.component=e,this.shouldAutoInitialize()){if(function(e){return"EAGER"===e.instantiationMode}
/**
 * @license
 * Copyright 2019 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */(e))try{this.getOrInitializeService({instanceIdentifier:o})}catch(e){}for(const[e,t]of this.instancesDeferred.entries()){const n=this.normalizeInstanceIdentifier(e);try{const e=this.getOrInitializeService({instanceIdentifier:n});t.resolve(e)}catch(e){}}}}clearInstance(e=o){this.instancesDeferred.delete(e),this.instancesOptions.delete(e),this.instances.delete(e)}async delete(){const e=Array.from(this.instances.values());await Promise.all([...e.filter(e=>"INTERNAL"in e).map(e=>e.INTERNAL.delete()),...e.filter(e=>"_delete"in e).map(e=>e._delete())])}isComponentSet(){return null!=this.component}isInitialized(e=o){return this.instances.has(e)}getOptions(e=o){return this.instancesOptions.get(e)||{}}initialize(e={}){const{options:t={}}=e,n=this.normalizeInstanceIdentifier(e.instanceIdentifier);if(this.isInitialized(n))throw Error(`${this.name}(${n}) has already been initialized`);if(!this.isComponentSet())throw Error(`Component ${this.name} has not been registered yet`);const r=this.getOrInitializeService({instanceIdentifier:n,options:t});for(const[e,t]of this.instancesDeferred.entries()){n===this.normalizeInstanceIdentifier(e)&&t.resolve(r)}return r}onInit(e,t){var n;const r=this.normalizeInstanceIdentifier(t),i=null!==(n=this.onInitCallbacks.get(r))&&void 0!==n?n:new Set;i.add(e),this.onInitCallbacks.set(r,i);const o=this.instances.get(r);return o&&e(o,r),()=>{i.delete(e)}}invokeOnInitCallbacks(e,t){const n=this.onInitCallbacks.get(t);if(n)for(const r of n)try{r(e,t)}catch(e){}}getOrInitializeService({instanceIdentifier:e,options:t={}}){let n=this.instances.get(e);if(!n&&this.component&&(n=this.component.instanceFactory(this.container,{instanceIdentifier:(r=e,r===o?void 0:r),options:t}),this.instances.set(e,n),this.instancesOptions.set(e,t),this.invokeOnInitCallbacks(n,e),this.component.onInstanceCreated))try{this.component.onInstanceCreated(this.container,e,n)}catch(e){}var r;return n||null}normalizeInstanceIdentifier(e=o){return this.component?this.component.multipleInstances?e:o:e}shouldAutoInitialize(){return!!this.component&&"EXPLICIT"!==this.component.instantiationMode}}class a{constructor(e){this.name=e,this.providers=new Map}addComponent(e){const t=this.getProvider(e.name);if(t.isComponentSet())throw new Error(`Component ${e.name} has already been registered with ${this.name}`);t.setComponent(e)}addOrOverwriteComponent(e){this.getProvider(e.name).isComponentSet()&&this.providers.delete(e.name),this.addComponent(e)}getProvider(e){if(this.providers.has(e))return this.providers.get(e);const t=new s(e,this);return this.providers.set(e,t),t}getProviders(){return Array.from(this.providers.values())}}}});
//# sourceMappingURL=vendor.c49fe3c450b9e4c45f19.js.map